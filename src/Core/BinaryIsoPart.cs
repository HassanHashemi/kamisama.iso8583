﻿using KamiSama.Extensions;
using System;

namespace KamiSama.Iso8583
{
	/// <summary>
	/// Binary Iso Part, contains binary data from the iso 8583 message
	/// </summary>
	public class BinaryIsoPart : IsoPart
	{
		/// <summary>
		/// inner binary data
		/// </summary>
		public ReadOnlyMemory<byte> Data { get; set; }
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="data">input data</param>
		public BinaryIsoPart(ReadOnlyMemory<byte> data) => Data = data;
		/// <summary>
		/// constructor
		/// </summary>
		public BinaryIsoPart() => Data = ReadOnlyMemory<byte>.Empty;
		/// <summary>
		/// returns hex value of <see cref="Data"/>
		/// </summary>
		/// <returns></returns>
		public override string ToString() => this.Data.ToArray().ToHex();
		/// <inheritdoc />
		public override ReadOnlyMemory<byte> ToBytes() => Data;
	}
}
