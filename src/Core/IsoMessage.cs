﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace KamiSama.Iso8583
{
	/// <summary>
	/// ISO 8583 message
	/// </summary>
	public class IsoMessage
	{
		private int id;
		/// <summary>
		/// Iso Message Type Indicator (MTI)
		/// </summary>
		public int Id
		{
			get => id;
			set
			{
				this.id = value;
				Parts["mti"] = value.ToString("0000");
			}
		}
		/// <summary>
		/// Primary Bitmap
		/// </summary>
		public BitmapIsoPart PrimaryBitmap { get; private set; } = new BitmapIsoPart();
		/// <summary>
		/// Secondary bitmap of the message
		/// </summary>
		public BitmapIsoPart SecondaryBitmap { get; private set; }
		/// <summary>
		/// message parts
		/// </summary>
		public IsoPartsCollection Parts { get; } = new IsoPartsCollection();
		/// <summary>
		/// manages fields and parts by name, fields are present as p03 and s120 pattern
		/// </summary>
		/// <param name="partName"></param>
		/// <returns></returns>
		public IsoPart this[string partName]
		{
			get => (IsoPart) Parts.GetValueOrDefault(partName);
			set => Parts[partName] = value;
		}
		/// <summary>
		/// managges fields by field no
		/// </summary>
		/// <param name="fieldNo"></param>
		/// <returns></returns>
		public IsoPart this[int fieldNo]
		{
			get => (fieldNo < 65) ? this[$"p{fieldNo:00}"] : this[$"s{fieldNo:000}"];
			set
			{
				if (fieldNo == 0)
				{
					if (value is not BitmapIsoPart bmp)
						throw new ArgumentException($"primary bitmap field should only be assigned to (not null) bitmap.");

					PrimaryBitmap = bmp;
					this["p00"] = value;
					return;
				}

				if (fieldNo < 65)
				{
					if (fieldNo == 1)
					{
						if (value is not BitmapIsoPart bmp)
							throw new ArgumentException($"secondary bitmap field should only be assigned to bitmap.");

						SecondaryBitmap = bmp;
					}

					PrimaryBitmap[fieldNo] = true;
					this[$"p{fieldNo:00}"] = value;
				} else
				{
					if (SecondaryBitmap == null)
						this[1] = new BitmapIsoPart();

					SecondaryBitmap[fieldNo] = true;
					this[$"s{fieldNo:000}"] = value;
				}
			}
		}
		/// <summary>
		/// default constructor
		/// </summary>
		public IsoMessage()
		{
			this.Parts["p00"] = PrimaryBitmap;
		}
		/// <summary>
		/// constrcutor
		/// </summary>
		/// <param name="id"></param>
		public IsoMessage(int id)
		{
			this.Id = id;
			this.Parts["p00"] = PrimaryBitmap;
		}
		/// <summary>
		/// returns a debug string for iso message, hiding sensitive fields
		/// </summary>
		/// <param name="sensitiveFieldNos"></param>
		/// <returns></returns>
		public string ToDebugStringByFields(params int[] sensitiveFieldNos)
		{
			var writer = new StringWriter();

			writer.WriteLine($"Iso Message, MTI: {Id:0000}");
			for (int i = 0; i < 129; i++)
			{
				if ((i > 1) && !this.Has(i))
					continue;

				string fieldName = (i < 65) ? $"p{i:00}" : $"s{i:000}";
				string fieldValue = sensitiveFieldNos.Contains(i) ? "........" : (this[fieldName] + "");
				writer.WriteLine($"{fieldName}\t\t:\t[{fieldValue}]");
			}

			return writer.ToString();
		}
		/// <summary>
		/// MAC field based on whether secondary bitmap exists or not
		/// </summary>
		/// <returns></returns>
		public BinaryIsoPart GetMac()
		{
			if (SecondaryBitmap != null)
				return Parts.GetValueOrDefault("s128", default) as BinaryIsoPart;
			else
				return Parts.GetValueOrDefault("p64", default) as BinaryIsoPart;
		}

		/// <summary>
		/// returns a debug string for iso message, hiding sensitive fields
		/// </summary>
		/// <param name="sensitiveParts"></param>
		/// <returns></returns>
		public string ToDebugStringByParts(params string[] sensitiveParts)
		{
			var writer = new StringWriter();

			writer.WriteLine($"Iso Message, MTI: {Id:0000}");
			foreach (var key in Parts.Keys.OrderBy(x => x))
			{
				var partValue = sensitiveParts.Contains(key) ? "........" : (string) this[key];

				writer.WriteLine($"{key}\t\t:\t[{partValue}]");
			}

			return writer.ToString();
		}
		/// <summary>
		/// returns <see cref="ToDebugStringByParts(string[])" /> hiding pinblock (P52) by default
		/// </summary>
		/// <returns></returns>
		public override string ToString() => ToDebugStringByParts("p52", "pin-block");//hiding pin block by default
		/// <summary>
		/// checks bitmaps whether the field exists or not
		/// </summary>
		/// <param name="fieldNo"></param>
		/// <returns></returns>
		public bool Has(int fieldNo) => (fieldNo <= 64) ? PrimaryBitmap[fieldNo] : SecondaryBitmap?[fieldNo] ?? false;
		/// <summary>
		/// sets mac flag for the iso message based existing fields
		/// </summary>
		public void SetMacFlag()
		{
			if (Has(64) || Has(128))
				return;

			if (SecondaryBitmap != null)
				for (int i = 65; i <= 128; i++)
					if (SecondaryBitmap[i])
					{
						SecondaryBitmap[128] = true;
						return;
					}

			PrimaryBitmap[64] = true;
		}
	}
}