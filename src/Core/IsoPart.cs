﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583
{
	/// <summary>
	/// abstract iso part
	/// </summary>
	public abstract class IsoPart : IConvertible
	{
		/// <summary>
		/// implicit operator to cast string data to <see cref="TextIsoPart" />
		/// </summary>
		/// <param name="data"></param>

		public static implicit operator IsoPart(string data) => new TextIsoPart(data);
		/// <summary>
		/// implicit operator to cast binary data to <see cref="BinaryIsoPart" />
		/// </summary>
		/// <param name="data"></param>

		public static implicit operator IsoPart(ReadOnlyMemory<byte> data) => new BinaryIsoPart(data);
		/// <summary>
		/// implicit operator to cast binary data to <see cref="BinaryIsoPart" />
		/// </summary>
		/// <param name="data"></param>

		public static implicit operator IsoPart(byte[] data) => new BinaryIsoPart(data);
		/// <summary>
		/// implicit operator to cast <see cref="IsoPart" /> to string data 
		/// </summary>
		/// <param name="part"></param>
		public static implicit operator string(IsoPart part) => part?.ToString();
		/// <summary>
		/// implicit operator to cast <see cref="IsoPart" /> to <see cref="ReadOnlyMemory{T}" /> of byte
		/// </summary>
		/// <param name="part"></param>
		public static implicit operator ReadOnlyMemory<byte>(IsoPart part) => part?.ToBytes() ?? ReadOnlyMemory<byte>.Empty;
		/// <summary>
		/// implicit operator to cast <see cref="IsoPart" /> to byte array
		/// </summary>
		/// <param name="part"></param>
		public static implicit operator byte[](IsoPart part) => part?.ToBytes().ToArray();
		/// <summary>
		/// returns default bytes equivalant to the specified IsoPart
		/// </summary>
		/// <returns></returns>
		public abstract ReadOnlyMemory<byte> ToBytes();
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public TypeCode GetTypeCode() => TypeCode.Object;
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public bool ToBoolean(IFormatProvider provider) => bool.Parse(this.ToString());
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public byte ToByte(IFormatProvider provider) => byte.Parse(this.ToString());
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public char ToChar(IFormatProvider provider) => char.Parse(this.ToString());
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public DateTime ToDateTime(IFormatProvider provider) => DateTime.Parse(this.ToString());
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public decimal ToDecimal(IFormatProvider provider) => decimal.Parse(this.ToString());
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public double ToDouble(IFormatProvider provider) => double.Parse(this.ToString());
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public short ToInt16(IFormatProvider provider) => Int16.Parse(this.ToString());
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public int ToInt32(IFormatProvider provider) => Int32.Parse(this.ToString());
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public long ToInt64(IFormatProvider provider) => Int64.Parse(this.ToString());
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public sbyte ToSByte(IFormatProvider provider) => sbyte.Parse(this.ToString());
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public float ToSingle(IFormatProvider provider) => float.Parse(this.ToString());
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public string ToString(IFormatProvider provider) => this.ToString();
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public object ToType(Type conversionType, IFormatProvider provider)
			=> (conversionType == typeof(object)) ? this : throw new InvalidCastException($"Cannot cast to {conversionType}");
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public ushort ToUInt16(IFormatProvider provider) => UInt16.Parse(this.ToString());
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public uint ToUInt32(IFormatProvider provider) => UInt32.Parse(this.ToString());
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public ulong ToUInt64(IFormatProvider provider) => UInt64.Parse(this.ToString());
	}
}
