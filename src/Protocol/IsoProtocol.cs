﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Helpers;
using KamiSama.Iso8583.Protocol.Mac;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace KamiSama.Iso8583.Protocol
{
	/// <summary>
	/// Default implementation of <see cref="IIsoProtocol"/>
	/// </summary>
	[XmlRoot("iso-protocol")]
	public class IsoProtocol : IIsoProtocol, IEquatable<IsoProtocol>
	{
		/// <summary>
		/// postfix for protocol definition file in directories and archive files
		/// </summary>
		public const string protocolDefinitionFile = "definition.protocol.xml";
		/// <summary>
		/// postfix for template definition file in directories and archive files
		/// </summary>
		public const string templateFilePostfix = ".template.xml";
		/// <summary>
		/// postfix for message definition file in directories and archive files
		/// </summary>
		public const string messageFilePostfix = ".message.xml";

		/// <summary>
		/// Protocol Name (informational)
		/// </summary>
		[XmlAttribute("name")]
		public string Name { get; set; }
		/// <summary>
		/// Protocol header, by default it just includes length and MTI
		/// </summary>
		[XmlElement("header")]
		public IsoHeaderDefinition Header { get; set; } = new IsoHeaderDefinition();
		/// <summary>
		/// Message Template Definitions; template definitions will not be chosen to parse a message and can only be reference message definition for <see cref="MessageDefinitions"/>
		/// </summary>
		[XmlElement("template")]
		public IsoMessageTemplateDefinition[] MessageTemplateDefnitions { get; set; }
		/// <summary>
		/// Message Definitions
		/// </summary>
		[XmlElement("message")]
		public IsoMessageDefinition[] MessageDefinitions { get; set; }
		#region LoadFrom X methods
		/// <summary>
		/// Loads a protocol from a single XML file
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="optimize">indicates that the protocol must be optimized after loading</param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public static IsoProtocol LoadFromXmlFile(string filePath, bool optimize = true)
		{
			var protocol = File.ReadAllText(filePath).DeserializeFromXml<IsoProtocol>();

			if (optimize)
				protocol.Optimize(forceOptimize: true);

			return protocol;
		}
		/// <summary>
		/// Loads a protocol from a directory; message are sought as *.message-definition and templates are  as *.message-template
		/// </summary>
		/// <param name="directoryPath"></param>
		/// <param name="includeSubDirectories"></param>
		/// <param name="ignoreInvalidFiles"></param>
		/// <param name="optimize"></param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public static IsoProtocol LoadFromDirectory(string directoryPath, bool optimize = true, bool includeSubDirectories = true, bool ignoreInvalidFiles = false)
		{
			var dir = new DirectoryInfo(directoryPath);

			var protocolFile = dir.GetFiles(protocolDefinitionFile).FirstOrDefault();
			IsoProtocol protocol;

			if (protocolFile?.Exists == true)
				protocol = LoadFromXmlFile(protocolFile.FullName, false);
			else
				protocol = new IsoProtocol { Name = dir.Name };

			var templatesList = (protocol.MessageTemplateDefnitions ?? new IsoMessageTemplateDefinition[0]).ToList();

			var searchOption = includeSubDirectories ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;

			foreach (var file in dir.GetFiles("*" + templateFilePostfix, searchOption))
				try
				{
					var templateDefinition = File.ReadAllText(file.FullName).DeserializeFromXml<IsoMessageTemplateDefinition>();
					templatesList.Add(templateDefinition);
				}
				catch (Exception exp)
				{
					if (!ignoreInvalidFiles)
						throw new IsoProtocolParsingException($"Failed to parse template file: {file.Name}", exp);
				}

			protocol.MessageTemplateDefnitions = templatesList.ToArray();
			var messagesList = (protocol.MessageDefinitions ?? new IsoMessageDefinition[0]).ToList();

			foreach (var file in dir.GetFiles("*" + messageFilePostfix, searchOption))
				try
				{
					var messageDefinition = File.ReadAllText(file.FullName).DeserializeFromXml<IsoMessageDefinition>();
					messagesList.Add(messageDefinition);
				}
				catch (Exception exp)
				{
					if (!ignoreInvalidFiles)
						throw new IsoProtocolParsingException($"Failed to parse message file: {file.Name}", exp);
				}

			protocol.MessageDefinitions = messagesList.ToArray();

			if (optimize)
				protocol.Optimize(forceOptimize: false);

			return protocol;
		}
		/// <summary>
		/// Loads protocol from a ZIP file; message are sought as *.message-definition and templates are sought as *.message-template
		/// </summary>
		/// <param name="zipFile"></param>
		/// <param name="ignoreInvalidFiles"></param>
		/// <param name="optimize"></param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public static IsoProtocol LoadFromZipFile(ZipArchive zipFile, bool optimize = true, bool ignoreInvalidFiles = false)
		{
			var protocolEntry = zipFile.GetEntry(protocolDefinitionFile);

			IsoProtocol protocol;

			if (protocolEntry == null)
				protocol = new IsoProtocol { Name = Guid.NewGuid().ToString("N") };
			else
			{
				using var reader = new StreamReader(protocolEntry.Open());
				protocol = reader.ReadToEnd().DeserializeFromXml<IsoProtocol>();
			}

			var templatesList = (protocol.MessageTemplateDefnitions ?? new IsoMessageTemplateDefinition[0]).ToList();
			var messagesList = (protocol.MessageDefinitions ?? new IsoMessageDefinition[0]).ToList();

			foreach (var entry in zipFile.Entries)
				try
				{
					if (entry.FullName.ToLower().EndsWith(templateFilePostfix))
					{
						using var reader = new StreamReader(entry.Open());

						var templateDefinition = reader.ReadToEnd().DeserializeFromXml<IsoMessageTemplateDefinition>();

						templatesList.Add(templateDefinition);
					}
					else if (entry.FullName.ToLower().EndsWith(messageFilePostfix))
					{
						using var reader = new StreamReader(entry.Open());

						var messageDefinition = reader.ReadToEnd().DeserializeFromXml<IsoMessageDefinition>();

						messagesList.Add(messageDefinition);
					}
				}
				catch
				{
					if (!ignoreInvalidFiles)
						throw;
				}

			protocol.MessageTemplateDefnitions = templatesList.ToArray();
			protocol.MessageDefinitions = messagesList.ToArray();

			if (optimize)
				protocol.Optimize(forceOptimize: true);

			return protocol;
		}
		/// <summary>
		/// Loads protocol from a ZIP file; message are sought as *.message-definition and templates are sought as *.message-template
		/// </summary>
		/// <param name="zipFilePath"></param>
		/// <param name="optimize"></param>
		/// <param name="ignoreInvalidFiles"></param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public static IsoProtocol LoadFromZipFile(string zipFilePath, bool optimize = true, bool ignoreInvalidFiles = false)
		{
			using var archive = ZipFile.OpenRead(zipFilePath);
			return LoadFromZipFile(archive, optimize, ignoreInvalidFiles);
		}
		#endregion
		/// <summary>
		/// Optimizes the protocol
		/// </summary>
		/// <param name="forceOptimize"></param>
		public virtual void Optimize(bool forceOptimize = false)
		{
			if (MessageTemplateDefnitions == null)
				MessageTemplateDefnitions = new IsoMessageTemplateDefinition[0];

			foreach (var template in MessageTemplateDefnitions)
				template.Optimize(this, forceOptimize);

			if (MessageDefinitions == null)
				MessageDefinitions = new IsoMessageDefinition[0];

			foreach (var template in MessageDefinitions)
				template.Optimize(this, forceOptimize);
		}
		/// <summary>
		/// Picks message bytes from stream in the fastest way possible
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="cancellationToken"></param>
		/// <param name="timeout"></param>
		/// <returns></returns>
		/// <exception cref="CannotPickMessageBytesFromStreamException" >if cannot read message length or body from stream</exception>
		public virtual Task<ReadOnlyMemory<byte>> PickMessageBytesFromStreamAsync(Stream stream, CancellationToken cancellationToken, TimeSpan timeout) 
			=> Header.PickMessageBytesFromStreamAsync(stream, cancellationToken, timeout);
		/// <summary>
		/// Parses the message bytes and creates a IsoParsingContext
		/// </summary>
		/// <param name="messageBytes"></param>
		/// <param name="macGenerator">if not null, the MAC field of the message will be verified by this function</param>
		/// <returns></returns>
		public virtual async Task<IsoParsingContext> ParseAsync(ReadOnlyMemory<byte> messageBytes, IMacGenerator macGenerator)
		{
			var context = new IsoParsingContext(messageBytes);
			var mti = ReadMessageTypeIndicator(context);

			context.Message = new IsoMessage(mti);

			foreach (var definition in MessageDefinitions)
				try
				{
					if (definition.Matches(context))
					{
						await definition.ParseAsync(context, macGenerator);
						break;
					}
				}
				catch (Exception exp) when (exp is not MacVerificationFailedException)
				{
					throw new IsoMessageReadException($"Failed to parse iso message by definition: {definition.Name}", exp);
				}

			return context;
		}
		/// <summary>
		/// Reads the MTI header
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		protected internal virtual int ReadMessageTypeIndicator(IsoParsingContext context)
		{
			var mtiBytes = context.Read(4);
			var id = MemoryBytesHelper.ConvertAsciiBytesToInt(mtiBytes);

			if (id == null)
				throw new IsoMessageReadException($"Invalid message type indicator (MTI), hex: {mtiBytes.ToArray().ToHex()}");
			else
				return id.Value;
		}
		/// <summary>
		/// Writes message MTI header
		/// </summary>
		/// <param name="context"></param>
		protected internal virtual void WriteMessageTypeIndicators(IsoComposingContext context)
		{
			var mtiBytes = MemoryBytesHelper.ConvertIntToAsciiBytes(context.Message.Id, 4);

			context.Write(mtiBytes);
		}
		/// <summary>
		/// Composes message using message definitions
		/// </summary>
		/// <param name="context"></param>
		/// <param name="macGenerator"></param>
		/// <returns></returns>
		public async virtual Task<ReadOnlyMemory<byte>> ComposeAsync(IsoComposingContext context, IMacGenerator macGenerator)
		{
			WriteMessageTypeIndicators(context);

			context.Message.SetMacFlag();

			foreach (var definition in MessageDefinitions)
				try
				{
					if (definition.Matches(context))
					{
						await definition.ComposeAsync(context, macGenerator);
						break;
					}
				}
				catch (Exception exp) when (exp is not MacGenerationFailedException)
				{
					throw new IsoMessageWriteException($"Failed to compose iso message by definition: {definition.Name}", exp);
				}

			return context.MessageBodyBytes;
		}
		/// <summary>
		/// Puts message on the stream
		/// </summary>
		/// <param name="stream">the destination stream</param>
		/// <param name="messageBytes">the message bytes to be written</param>
		/// <param name="cancellationToken">cancellation token</param>
		/// <returns></returns>
		/// <exception cref="CannotPutMessageBytesOnStreamException">if cannot write message bytes or length to the stream</exception>
		public virtual Task PutMessageBytesToStreamAsync(Stream stream, ReadOnlyMemory<byte> messageBytes, CancellationToken cancellationToken)
			=> Header.PutMessageBytesToStreamAsync(stream, messageBytes, cancellationToken);
		/// <summary>
		/// compares iso protocol to another one
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public bool Equals(IsoProtocol other)
		{
			if ((other == null) ||
				(Name != other.Name) ||
				(MessageTemplateDefnitions?.Length != other.MessageTemplateDefnitions?.Length) ||
				(MessageDefinitions?.Length != other.MessageDefinitions?.Length))
				return false;

			if (MessageTemplateDefnitions != null)
				for (int i = 0; i < MessageTemplateDefnitions.Length; i++)
					if (!MessageTemplateDefnitions[i].Equals(other.MessageTemplateDefnitions[i]))
						return false;

			if (MessageDefinitions != null)
				for (int i = 0; i < MessageDefinitions.Length; i++)
					if (!MessageDefinitions[i].Equals(other.MessageDefinitions[i]))
						return false;

			return true;
		}
		/// <inheritdoc/>
		[ExcludeFromCodeCoverage]
		public override bool Equals(object obj) => Equals(obj as IsoProtocol);
		/// <inheritdoc/>
		[ExcludeFromCodeCoverage]
		public override int GetHashCode() => base.GetHashCode();
	}
}
