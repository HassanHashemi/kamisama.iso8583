﻿using Expressive;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Helpers;
using KamiSama.Iso8583.Protocol.Parts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Serialization;

namespace KamiSama.Iso8583.Protocol
{
	/// <summary>
	/// Abstract Message Defninition, the possible inheritences are <see cref="IsoMessageDefinition"/> and <see cref="IsoMessageTemplateDefinition"/>
	/// </summary>
	public abstract class IsoMessageDefinitionBase
	{
		/// <summary>
		/// name of the definition (also can be referenced by other definitions)
		/// </summary>
		[XmlAttribute("name")]
		public string Name { get; set; } = Guid.NewGuid().ToString("N");
		/// <summary>
		/// Base Template <see cref="IsoMessageDefinitionBase"/>; can be null
		/// </summary>
		[XmlAttribute("base")]
		public string BaseTemplateName { get; set; }
		/// <summary>
		/// IsoFeild definitions
		/// </summary>
		[XmlElement("field")]
		public IsoFieldDefinition[] Fields { get; set; }
		/// <summary>
		/// The criteria to match
		/// </summary>
		/// <remarks>the base template's criteria is automatically included and checked before execution</remarks>
		[XmlAttribute("matches")]
		public string MatchCriteriaText { get; set; }
		/// <summary>
		/// match criteria expression created in optimization
		/// </summary>
		internal Expression matchCriteriaExpression;
		/// <summary>
		/// base template constructed
		/// </summary>
		protected IsoMessageTemplateDefinition BaseTemplate { get; set; }
		/// <summary>
		/// whether or not the definition is optimized
		/// </summary>
		internal bool optimized = false;
		/// <summary>
		/// Matches the context by <see cref="matchCriteriaExpression"/>
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public virtual bool Matches(IIsoProtocolContext context) => matchCriteriaExpression.Evaluate<bool>(context.Message.Parts);
		/// <summary>
		/// Optimizes the message definition; the process includes merging the base templates into the definition and also optimizing the fields
		/// </summary>
		/// <param name="protocol"></param>
		/// <param name="forceOptimize"></param>
		public virtual void Optimize(IsoProtocol protocol, bool forceOptimize = false)
		{
			if (optimized && !forceOptimize)
				return;

			optimized = false;
			if (!string.IsNullOrWhiteSpace(BaseTemplateName))
			{
				BaseTemplate = protocol.MessageTemplateDefnitions.FirstOrDefault(x => x.Name.ToLower() == BaseTemplateName.ToLower());
				
				if (BaseTemplate == null)
					throw new IsoProtocolParsingException($"Cannot find base template : {BaseTemplateName} for definition : {Name}");
			}

			var criterias = new List<string>();

			if (BaseTemplate != null)
			{
				BaseTemplate.Optimize(protocol, forceOptimize);

				if (BaseTemplate.MatchCriteriaText != "true")
					criterias.Add(BaseTemplate.MatchCriteriaText);
			}

			criterias.Add(MatchCriteriaText);

			criterias = criterias.Where(x => !string.IsNullOrWhiteSpace(x)).ToList();

			if (!criterias.Any())
				this.MatchCriteriaText = "true";
			else if (criterias.Count == 1)
				this.MatchCriteriaText = criterias[0];
			else
				this.MatchCriteriaText = string.Join(" and ", criterias.Select(x => $"({x})"));
			
			matchCriteriaExpression = new Expression(this.MatchCriteriaText, ExpressiveOptions.IgnoreCaseAll);

			var fieldComparer = EqualityComparerEx<IsoFieldDefinition>.Create((x, y) => x.FieldNo == y.FieldNo, x => x.FieldNo.GetHashCode());

			Fields = Fields
				.GroupBy(x => x.FieldNo)
				.Select(x => x.Last())
				.ToArray();

			if (BaseTemplate != null)
				Fields = Fields
					.Union(BaseTemplate.Fields.Except(Fields, fieldComparer))
					.ToArray();
			
			Fields = Fields
					.OrderBy(x => x.FieldNo)
					.ToArray();

			foreach (var field in Fields)
				field.Optimize(forceOptimize);

			optimized = true;
		}
	}
}