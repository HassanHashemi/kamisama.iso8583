﻿namespace KamiSama.Iso8583.Protocol.Generic
{
	/// <summary>
	/// Variable Size Iso Field Length Type
	/// </summary>
	public enum VariableLengthType : int
    {
		/// <summary>
		/// one byte or nibble is used for length
		/// </summary>
		LVar = 1,
		/// <summary>
		/// two bytes or nibble is used for length
		/// </summary>
		LLVar = 2,
		/// <summary>
		/// three bytes or nibble is used for length
		/// </summary>
		LLLVar = 3,
		/// <summary>
		/// four bytes or nibble is used for length
		/// </summary>
		LLLLVar = 4,
    }
}
