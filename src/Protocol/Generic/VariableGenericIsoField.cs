﻿using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Parts;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;

namespace KamiSama.Iso8583.Protocol.Generic
{
	/// <summary>
	/// Variable size iso field
	/// </summary>
	public class VariableGenericIsoField : GenericIsoProtocolField, IEquatable<VariableGenericIsoField>
	{
		/// <summary>
		/// Length Data Type
		/// </summary>
		[XmlAttribute("length-datatype")]
		public string LengthDataType { get; set; }
		/// <summary>
		/// Length Type (how many bytes/nibbles is needed fo length)
		/// </summary>
		[XmlAttribute("length-type")]
		public VariableLengthType LengthType { get; set; }
		/// <inheritdoc />
		public override IsoFieldDefinition ToIsoFieldDefinition()
		{
			var bytesType = LengthDataType switch
			{
				"text" or "txt" => "ascii-",
				"bin" or "binary" => "bin-",
				"bcd" => "bcd-",
				_ => throw new IsoProtocolParsingException($"Length type {LengthDataType} is not supported."),
			};

			bytesType += LengthType.ToString().ToLower();

			var dataType = DataType switch
			{
				"text" or "txt" => ("text " + Encoding).TrimEnd(),
				_ => DataType
			};

			return new IsoFieldDefinition
			{
				FieldNo = this.FieldNo,
				Type = $"{bytesType} {dataType}"
			};
		}
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public bool Equals(VariableGenericIsoField other)
			=> (other != null) &&
				(this.DataType == other.DataType) &&
				(this.Encoding == other.Encoding) &&
				(this.FieldNo == other.FieldNo) &&
				(this.LengthDataType == other.LengthDataType) &&
				(this.LengthType == other.LengthType);
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override bool Equals(object obj) => Equals(obj as VariableGenericIsoField);
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override int GetHashCode() => base.GetHashCode();
	}
}