﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Mac;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace KamiSama.Iso8583.Protocol.Generic
{
	/// <summary>
	/// Generic Iso Protocol; this type uses generic iso field definition to read/write iso messages
	/// </summary>
	[Serializable]
	[XmlRoot("iso-protocol")]
	public class GenericIsoProtocol : IIsoProtocol, IEquatable<GenericIsoProtocol>
	{
		/// <summary>
		/// iso fields
		/// </summary>
		[XmlElement("fixed", typeof(FixedGenericIsoField))]
		[XmlElement("variable", typeof(VariableGenericIsoField))]
		public GenericIsoProtocolField[] Fields { get; set; }
		/// <summary>
		/// internal protocol for compatibility
		/// </summary>
		[XmlIgnore]
		public IsoProtocol InternalProtocol { get; internal set; }
		/// <summary>
		/// Creates internal protocol and optmizes it
		/// </summary>
		/// <param name="forceOptimize"></param>
		public virtual void Optmize(bool forceOptimize= false)
		{
			if ((InternalProtocol != null) && !forceOptimize)
				return;

			InternalProtocol = new IsoProtocol
			{
				Name = Guid.NewGuid().ToString("N"),
				MessageDefinitions = new[]
				{
					new IsoMessageDefinition
					{
						Fields = Fields.Select(x => x.ToIsoFieldDefinition()).ToArray(),
						Name = Guid.NewGuid().ToString("N"),
					}
				}
			};

			InternalProtocol.Optimize(forceOptimize);
		}
		/// <summary>
		/// Loads <see cref="GenericIsoProtocol"/> from xml file
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static GenericIsoProtocol LoadFromXmlFile(string path)
		{
			var protocol = File.ReadAllText(path).DeserializeFromXml<GenericIsoProtocol>();

			protocol.Optmize();

			return protocol;
		}
		/// <inheritdoc/>
		public virtual Task<ReadOnlyMemory<byte>> ComposeAsync(IsoComposingContext context, IMacGenerator macGenerator) =>
			InternalProtocol.ComposeAsync(context, macGenerator);
		/// <inheritdoc/>
		public virtual Task<IsoParsingContext> ParseAsync(ReadOnlyMemory<byte> messageBytes, IMacGenerator macGenerator) =>
			InternalProtocol.ParseAsync(messageBytes, macGenerator);
		/// <inheritdoc />
		public virtual Task<ReadOnlyMemory<byte>> PickMessageBytesFromStreamAsync(Stream stream, CancellationToken cancellationToken, TimeSpan timeout)
			=> InternalProtocol.PickMessageBytesFromStreamAsync(stream, cancellationToken, timeout);
		/// <inheritdoc />
		public virtual Task PutMessageBytesToStreamAsync(Stream stream, ReadOnlyMemory<byte> messageBytes, CancellationToken cancellationToken)
			=> InternalProtocol.PutMessageBytesToStreamAsync(stream, messageBytes, cancellationToken);
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public bool Equals(GenericIsoProtocol other)
		{
			if (other == null)
				return false;

			if ((this.Fields == null) && (other.Fields == null))
				return true;

			if (Fields.Length != other.Fields.Length)
				return false;

			for (int i = 0; i < Fields.Length; i++)
				if (!Fields[i].Equals(other.Fields[i]))
					return false;

			return true;
		}
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override bool Equals(object obj) => Equals(obj as GenericIsoProtocol);
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override int GetHashCode() => base.GetHashCode();
		
	}
}
