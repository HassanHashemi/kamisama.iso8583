﻿using KamiSama.Iso8583.Protocol.Parts;
using System;
using System.Xml.Serialization;

namespace KamiSama.Iso8583.Protocol.Generic
{
	/// <summary>
	/// GenericIsoProtcolField; abstract representation of IsoField; has Fixed and Variable deriviation types
	/// </summary>
	[Serializable]
	public abstract class GenericIsoProtocolField
	{
		/// <summary>
		/// Iso Message Field Number
		/// </summary>
		[XmlAttribute("no")]
		public int FieldNo { get; set; }
		/// <summary>
		/// Iso Message Field Data Type
		/// </summary>
		[XmlAttribute("data-type")]
		public string DataType { get; set; }
		/// <summary>
		/// Iso Message Field Encoding (used for string types); default value is "utf-8"
		/// </summary>
		[XmlAttribute("encoding")]
		public string Encoding { get; set; } = "utf-8";
		/// <summary>
		/// converts a <see cref="GenericIsoProtocolField"/> to <see cref="IsoFieldDefinition"/>
		/// </summary>
		/// <returns></returns>
		public abstract IsoFieldDefinition ToIsoFieldDefinition();
	}
}