﻿using System;
using System.Collections.Generic;

namespace KamiSama.Iso8583.Protocol.Helpers
{
	/// <summary>
	/// Extended Equality Comparer which includes methods to do so
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class EqualityComparerEx<T> : IEqualityComparer<T>
	{
		/// <summary>
		/// compare function
		/// </summary>
		private readonly Func<T, T, bool> compareFunc;
		/// <summary>
		/// hash function
		/// </summary>
		private readonly Func<T, int> hashFunc;
		/// <summary>
		/// cnostrcutor
		/// </summary>
		/// <param name="compareFunc"></param>
		/// <param name="hashFunc">if null, default <typeparamref name="T"/>.GetHashCode /></param>
		public EqualityComparerEx(Func<T, T, bool> compareFunc, Func<T, int> hashFunc)
		{
			this.compareFunc = compareFunc;
			if (hashFunc == null)
				hashFunc = x => x?.GetHashCode() ?? 0;

			this.hashFunc = hashFunc;
		}
		/// <summary>
		/// uses <see cref="compareFunc"/> for object comparision
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		public bool Equals(T x, T y) => compareFunc(x, y);
		/// <summary>
		/// uses <see cref="hashFunc"/> for get hashcode 
		/// </summary>
		/// <param name="x"></param>
		/// <returns></returns>
		public int GetHashCode(T x) => hashFunc(x);
		/// <summary>
		/// Creates <see cref="EqualityComparerEx{T}"/>
		/// </summary>
		/// <param name="compareFunc"></param>
		/// <param name="hashFunc"></param>
		/// <returns></returns>
		public static EqualityComparerEx<T> Create(Func<T, T, bool> compareFunc, Func<T, int> hashFunc = null) => new(compareFunc, hashFunc);
	}
}
