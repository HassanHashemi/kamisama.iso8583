﻿using KamiSama.Extensions;
using System;

namespace KamiSama.Iso8583.Protocol.Helpers
{
	/// <summary>
	/// Memory Helper class
	/// </summary>
	public static class MemoryBytesHelper
	{
		/// <summary>
		/// Converts Ascii data (bytes) to integer
		/// </summary>
		/// <param name="memory"></param>
		/// <returns></returns>
		public static int? ConvertAsciiBytesToInt(ReadOnlyMemory<byte> memory)
		{
			int result = 0;

			for (int i = 0; i < memory.Length; i++)
			{
				var c = memory.Span[i];

				if (c < '0' || c > '9')
					return null;

				result = result * 10 + c - '0';
			}

			return result;
		}
		/// <summary>
		/// Converts BCD data (bytes) to integer
		/// </summary>
		/// <param name="memory"></param>
		/// <param name="bcdLength"></param>
		/// <returns></returns>
		public static int? ConvertBcdBytesToInt(ReadOnlyMemory<byte> memory, int bcdLength = -1)
		{
			if (bcdLength < 0)
				bcdLength = memory.Length << 1;

			int result = 0;

			for (int i = 0; i < memory.Length; i++)
			{
				var b = memory.Span[i];

				var b1 = b / 16;
				var b2 = b % 16;

				if ((i == 0) && (bcdLength % 2 == 1))
					b1 = 0;

				if ((b1 > 9) || (b2 > 9))
					return null;

				result = result * 100 + b1 * 10 + b2;
			}

			return result;
		}
		/// <summary>
		/// converts bytes to hexadecimal equivalant
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static byte[] ConvertBytesToHexBytes(ReadOnlyMemory<byte> data)
		{
			var result = new byte[data.Length * 2];
			var span = data.Span;
			var index = 0;

			for (int i = 0; i < data.Length; i++)
			{
				var b1 = (span[i] & 0xf0) >> 4;
				var b2 = span[i] & 0x0f;

				b1 = (b1 < 10) ? (0x30 + b1) : (0x41 + b1 - 10);
				b2 = (b2 < 10) ? (0x30 + b2) : (0x41 + b2 - 10);

				result[index++] = (byte)b1;
				result[index++] = (byte)b2;
			}

			return result;
		}

		/// <summary>
		/// Converts Ascii String to bytes
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static byte[] ConvertAsciiSringToBytes(string str)
		{
			var result = new byte[str.Length];
			var span = str.AsSpan();

			for (int i = 0; i < result.Length; i++)
				result[i] = (byte)span[i];

			return result;
		}
		/// <summary>
		/// Convert String to BCD bytes
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static ReadOnlyMemory<byte> ConvertNumberStringToBcdBytes(string str)
		{
			if (string.IsNullOrWhiteSpace(str))
				return ReadOnlyMemory<byte>.Empty;

			var result = new byte[(str.Length + 1) / 2];
			var span = str.AsSpan();

			if (span.Length % 2 == 0)
			{
				for (int i = 0; i < result.Length; i++)
				{
					var index = i << 1;

					var b1 = span[index] - '0';
					var b2 = span[index + 1] - '0';

					if ((b1 < 0) || (b1 > 9) || (b2 < 0) || (b2 > 9))
						throw new ArgumentException($"Invalid bcd string: {str}.");

					result[i] = (byte)((b1 << 4) | b2);
				}
			}
			else
			{
				var b = span[0] - '0';

				if ((b < 0) || (b > 9))
					throw new ArgumentException($"Invalid bcd string: {str}.");

				result[0] = (byte) b;
				for (int i = 1; i < result.Length; i++)
				{
					var index = (i << 1) - 1;
					var b1 = span[index] - '0';
					var b2 = span[index + 1] - '0';

					if ((b1 < 0) || (b1 > 9) || (b2 < 0) || (b2 > 9))
						throw new ArgumentException($"Invalid bcd string: {str}.");

					result[i] = (byte)((b1 << 4) | b2);
				}
			}

			return result;
		}
		/// <summary>
		/// Converts BCD bytes to string
		/// </summary>
		/// <param name="memory"></param>
		/// <returns></returns>
		public static string ConvertBcdBytesToString(ReadOnlyMemory<byte> memory)
		{
			char[] arr = new char[memory.Length * 2];
			var span = memory.Span;

			for (int i = 0; i < arr.Length; i += 2)
			{
				var b = span[i / 2];

				var b1 = b / 16;
				var b2 = b % 16;

				if ((b1 > 9) || (b2 > 9))
					throw new ArgumentException($"Invalid bytes to convert to bcd. hex: {memory.ToArray().ToHex()}.");

				arr[i] = (char)('0' + b1);
				arr[i + 1] = (char)('0' + b2);
			}

			return new string(arr);
		}
		/// <summary>
		/// Converts integer to int
		/// </summary>
		/// <param name="x"></param>
		/// <param name="bytesLength"></param>
		/// <returns></returns>
		public static byte[] ConvertIntToBytes(int x, int bytesLength = -1)
		{
			if (bytesLength < 0)
				bytesLength = sizeof(int);

			byte[] result = new byte[bytesLength];

			for (int i = result.Length - 1; i >= 0; i--)
			{
				result[i] = (byte)(x & 0xFF);
				x >>= 8;
			}

			return result;
		}

		/// <summary>
		/// Convert bytes to integer number
		/// </summary>
		/// <param name="lengthBytes"></param>
		/// <returns></returns>
		public static int ConvertBytesToInt(ReadOnlyMemory<byte> lengthBytes)
		{
			var result = 0;
			var span = lengthBytes.Span;

			for (int i = 0; i < span.Length; i++)
			{
				result <<= 8;
				result |= span[i];
			}

			return result;
		}
		/// <summary>
		/// Converts hexadecimal data (bytes) to bytes
		/// </summary>
		/// <param name="memory"></param>
		/// <returns></returns>
		public static byte[] ConvertHexBytesToBytes(ReadOnlyMemory<byte> memory)
		{
			var offset = memory.Length & 0x1;
			var arr = new byte[(memory.Length + offset) / 2];
			var span = memory.Span;

			for (int i = 0; i < span.Length; i++)
			{
				int b = span[i];

				if ((b >= '0') && (b <= '9'))
					b -= '0';
				else if ((b >= 'A') && (b <= 'F'))
					b -= 'A' - 10;
				else if ((b >= 'a') && (b <= 'f'))
					b -= 'a' - 10;
				else
					throw new ArgumentException($"The given data is not hexadecimal. hex bytes: {memory.ToArray().ToHex()}");

				if (i % 2 == offset)
					arr[(i + offset) >> 1] |= (byte)(b << 4);
				else
					arr[(i + offset) >> 1] |= (byte)b;
			}

			return arr;
		}
		/// <summary>
		/// Converts integer to ascii data (bytes)
		/// </summary>
		/// <param name="x"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static byte[] ConvertIntToAsciiBytes(int x, int length = sizeof(int))
		{
			var result = new byte[length];

			for (int i = length - 1; i >= 0; i--)
			{
				result[i] = (byte)('0' + x % 10);

				x /= 10;
			}

			return result;
		}
		/// <summary>
		/// Converts integer to bcd bytes
		/// </summary>
		/// <param name="x"></param>
		/// <param name="bytesLength"></param>
		/// <returns></returns>
		public static byte[] ConvertIntToBcdBytes(int x, int bytesLength)
		{
			var result = new byte[bytesLength];

			for (int i = result.Length - 1; i >= 0; i--)
			{
				var b1 = x % 10;
				var b2 = (x / 10) % 10;
				x /= 100;

				result[i] = (byte)((b2 << 8) + b1);
			}

			return result;
		}
	}
}
