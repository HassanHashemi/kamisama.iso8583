﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Protocol.Helpers
{
	/// <summary>
	/// Stream helper class
	/// </summary>
	public static class IsoStreamHelper
	{
		/// <summary>
		/// Reads a bytes corresponding to <paramref name="memory"/>'s length
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="memory"></param>
		/// <param name="cancellationToken"></param>
		/// <param name="timeOut"></param>
		/// <returns>false if request is timed out.</returns>
		public static async Task<bool> ReadAsync(Stream stream, Memory<byte> memory, CancellationToken cancellationToken, TimeSpan timeOut)
		{
			int totalRead = 0;

			bool checkForTimeOut = timeOut != Timeout.InfiniteTimeSpan;
			DateTime? startTime = checkForTimeOut ? DateTime.UtcNow : (DateTime?)null;

			do
			{
				var bytesRead = await stream.ReadAsync(memory, cancellationToken);

				if (bytesRead > 0)
					totalRead += bytesRead;
				else
					await Task.Delay(1);

				if ((totalRead < memory.Length) && checkForTimeOut && ((DateTime.UtcNow - startTime.Value) > timeOut))
					return false;
			}
			while (totalRead < memory.Length);

			return true;
		}
	}
}
