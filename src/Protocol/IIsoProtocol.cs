﻿using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Mac;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Protocol
{
	/// <summary>
	/// Iso Protocol interface
	/// </summary>
	public interface IIsoProtocol
	{
		/// <summary>
		/// Parses message bytes into context, also verifies mac
		/// </summary>
		/// <param name="messageBytes">message body including MTI and fields</param>
		/// <param name="macGenerator">MAC generation method, if null mac verification will be ignored.</param>
		/// <returns>parsed iso context including parts and fields</returns>
		Task<IsoParsingContext> ParseAsync(ReadOnlyMemory<byte> messageBytes, IMacGenerator macGenerator);
		/// <summary>
		/// Composes context into message bytes
		/// </summary>
		/// <param name="context">contet including parts and fields</param>
		/// <param name="macGenerator">mac generation function</param>
		/// <returns></returns>
		Task<ReadOnlyMemory<byte>> ComposeAsync(IsoComposingContext context, IMacGenerator macGenerator);
		/// <summary>
		/// Picks message bytes from stream in the fastest way possible
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="cancellationToken"></param>
		/// <param name="timeout"></param>
		/// <returns></returns>
		/// <exception cref="CannotPickMessageBytesFromStreamException" >if cannot read message length or body from stream</exception>
		Task<ReadOnlyMemory<byte>> PickMessageBytesFromStreamAsync(Stream stream, CancellationToken cancellationToken, TimeSpan timeout);
		/// <summary>
		/// Puts message on the stream
		/// </summary>
		/// <param name="stream">the destination stream</param>
		/// <param name="messageBytes">the message bytes to be written</param>
		/// <param name="cancellationToken">cancelation token</param>
		/// <returns></returns>
		/// <exception cref="CannotPutMessageBytesOnStreamException">if cannot write message bytes or length to the stream</exception>
		Task PutMessageBytesToStreamAsync(Stream stream, ReadOnlyMemory<byte> messageBytes, CancellationToken cancellationToken);		
	}
}
