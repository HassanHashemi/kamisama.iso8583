﻿using System;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Protocol.Mac
{
	/// <summary>
	/// Function based Mac Generator
	/// </summary>
	public class FunctionMacGenerator : IMacGenerator
	{
		/// <summary>
		/// function to generate mac
		/// </summary>
		public Func<IIsoProtocolContext, MacGenerationType, Task<byte[]>> Func { get; }
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="func"></param>
		public FunctionMacGenerator(Func<IIsoProtocolContext, MacGenerationType, Task<byte[]>> func) => this.Func = func;
		/// <inheritdoc />
		public Task<byte[]> GenerateMacAsync(IIsoProtocolContext context, MacGenerationType macGenerationType) => Func(context, macGenerationType);
	}
}
