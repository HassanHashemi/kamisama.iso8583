﻿using KamiSama.Extensions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Protocol.Mac
{
	/// <summary>
	/// Generates ISO MAC based on the proposed algorithm
	/// </summary>
	public interface IMacGenerator
	{
		/// <summary>
		/// Generates ISO MAC based on the proposed algorithm
		/// </summary>
		/// <param name="context"></param>
		/// <param name="macGenerationType"></param>
		/// <returns></returns>
		Task<byte[]> GenerateMacAsync(IIsoProtocolContext context, MacGenerationType macGenerationType);
		/// <summary>
		/// creates mac generator from function
		/// </summary>
		/// <param name="func"></param>
		/// <returns></returns>
		public static IMacGenerator FromFunction(Func<IIsoProtocolContext, MacGenerationType, Task<byte[]>> func) => new FunctionMacGenerator(func);
		/// <summary>
		/// creates mac generator from synchronous function
		/// </summary>
		/// <param name="func"></param>
		/// <returns></returns>
		public static IMacGenerator FromFunction(Func<IIsoProtocolContext, MacGenerationType, byte[]> func) => new FunctionMacGenerator((ctx, macType) => Task.FromResult(func(ctx, macType)));
		/// <summary>
		/// a mac generator that only causes exception for test purposes
		/// </summary>
		/// <param name="exp"></param>
		/// <returns></returns>
		public static IMacGenerator FromException(Exception exp) => new FunctionMacGenerator((ctx, macType) => throw exp);
		/// <summary>
		/// a mac generator from a key (hex)
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public static IMacGenerator FromKey(byte[] key) => new KeyBasedMacGenerator(key);
		/// <summary>
		/// a mac generator from a key (hex)
		/// </summary>
		/// <param name="keyHex"></param>
		/// <returns></returns>
		public static IMacGenerator FromKey(string keyHex) => new KeyBasedMacGenerator(keyHex.FromHex());
	}
}
