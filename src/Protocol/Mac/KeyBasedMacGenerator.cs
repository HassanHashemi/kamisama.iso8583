﻿using KamiSama.Extensions;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Protocol.Mac
{
	/// <summary>
	/// Generates mac based on a key
	/// </summary>
	public class KeyBasedMacGenerator : IMacGenerator
	{
		/// <summary>
		/// key for signing the message
		/// </summary>
		private readonly byte[] key;
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="key"></param>
		public KeyBasedMacGenerator(byte[] key)
			=> this.key = key;
		/// <summary>
		/// Generates mac based on algorithm
		/// </summary>
		/// <param name="context"></param>
		/// <param name="macGenerationType"></param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public Task<byte[]> GenerateMacAsync(IIsoProtocolContext context, MacGenerationType macGenerationType)
		{
			var data = context.ForMacBytes.ToArray();

			switch (macGenerationType)
			{
				case MacGenerationType.Skip: //not gonna happen
				default:
					throw new NotSupportedException($"Mac generation type: [{macGenerationType}] is not supported by {typeof(KeyBasedMacGenerator)}");

				case MacGenerationType.AnsiX9_19:
					var keyPart1 = key.AsSpan(0, 8).ToArray();
					var keyPart2 = key.AsSpan(8, 8).ToArray();

					return Task.FromResult(data
						.DesSign(keyPart1, padding: PaddingMode.Zeros)
						.DesDecrypt(keyPart2, cipherMode: CipherMode.CBC, padding: PaddingMode.None)
						.DesEncrypt(keyPart1, cipherMode: CipherMode.CBC, padding: PaddingMode.None));

				case MacGenerationType.DesMac:
					return Task.FromResult(data
						.DesSign(key, padding: PaddingMode.Zeros));

				case MacGenerationType.TripleDesMac:
					return Task.FromResult(data.TripleDesSign(key, padding: PaddingMode.Zeros));

				case MacGenerationType.AesMac:
					return Task.FromResult(data.AesSign(key, padding: PaddingMode.Zeros));
			}
		}
	}
}
