﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Helpers;
using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Handlers.BytesHandlers
{
	/// <summary>
	/// Read/Writes bcd-length-value bytes from/to context
	/// </summary>
	/// <remarks>as the bcd numbers are half bytes, the odd numbers as length are considered to upper value</remarks>
	public class BcdLengthValueIsoPartBytesHandler : IIsoPartBytesHandler
	{
		/// <summary>
		/// lengths to be read in the measure of nibbles (half bytes)
		/// </summary>
		public int LengthLength { get; }
		/// <summary>
		/// maximum supportable length
		/// </summary>
		private readonly int max;
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="lengthLength"></param>
		public BcdLengthValueIsoPartBytesHandler(int lengthLength)
		{
			LengthLength = lengthLength;
			max = (int)Math.Pow(10, lengthLength);
		}
		/// <inheritdoc />
		public static bool Matches(string type) => type.StartsWith("bcd-l") && type.EndsWith("var");
		/// <summary>
		/// Creates a <see cref="BcdLengthValueIsoPartBytesHandler" /> out of string
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public static BcdLengthValueIsoPartBytesHandler Create(string type)
		{
			var lengthLength = type switch
			{
				"bcd-lvar" => 1,
				"bcd-llvar" => 2,
				"bcd-lllvar" => 3,
				"bcd-llllvar" => 4,
				"bcd-lllllvar" => 5,
				_ => throw new IsoProtocolParsingException($"Invalid length value type. type: {type}.")
			};

			return new BcdLengthValueIsoPartBytesHandler(lengthLength);
		}
		/// <summary>
		/// reads bcd-length and then bytes from the context
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public ReadOnlyMemory<byte> ReadBytes(IsoParsingContext context)
		{
			var lengthBytes = context.Read((LengthLength + 1) / 2);
			var length = MemoryBytesHelper.ConvertBcdBytesToInt(lengthBytes, LengthLength);
			length %= max;

			if (length == null)
				throw new IsoMessageReadException($"invalid length for L({LengthLength})Var. length bytes hex: {lengthBytes.ToArray().ToHex()}");

			return context.Read(length.Value);
		}
		/// <summary>
		/// Writes bcd-length and the bytes to the context
		/// </summary>
		/// <param name="context"></param>
		/// <param name="data"></param>
		public void WrtieBytes(IsoComposingContext context, ReadOnlyMemory<byte> data)
		{
			var length = data.Length;

			if (length >= max)
				throw new IsoMessageWriteException($"Invalid data length for bcd length. lenght: {length}, maximum:{max}");

			var lengthBytes = MemoryBytesHelper.ConvertIntToBcdBytes(data.Length, (LengthLength + 1) / 2);

			context.Write(lengthBytes);
			context.Write(data);
		}
	}
}
