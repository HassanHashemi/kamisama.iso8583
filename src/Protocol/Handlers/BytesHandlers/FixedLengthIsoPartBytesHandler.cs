﻿using KamiSama.Iso8583.Protocol.Exceptions;
using System;

namespace KamiSama.Iso8583.Protocol.Handlers.BytesHandlers
{
	/// <summary>
	/// Fixed Length Iso Part Bytes Handler
	/// </summary>
	public class FixedLengthIsoPartBytesHandler : IIsoPartBytesHandler
	{
		/// <summary>
		/// the length to be read as bytes
		/// </summary>
		public int Length { get; }
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="length"></param>
		public FixedLengthIsoPartBytesHandler(int length) => Length = length;
		/// <summary>
		/// Matches the type as fixed(n)
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static bool Matches(string type) => type.StartsWith("fixed(") && type.EndsWith(")");
		/// <summary>
		/// Creates a <see cref="FixedLengthIsoPartBytesHandler"/> out of type
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static FixedLengthIsoPartBytesHandler Create(string type)
		{
			if ((type == null) || !type.StartsWith("fixed(") || !type.EndsWith(")"))
				throw new IsoProtocolParsingException($"Invalid type to create fixed length iso part bytes handler, type: {type}.");

			var str = type.Substring("fixed(".Length, type.Length - "fixed()".Length);

			if (!int.TryParse(str, out int length))
				throw new IsoProtocolParsingException($"Invalid type to create fixed length iso part bytes handler, type: {type}.");

			return new FixedLengthIsoPartBytesHandler(length);
		}
		/// <summary>
		/// Reads <see cref="Length" /> bytes from the context
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public ReadOnlyMemory<byte> ReadBytes(IsoParsingContext context)
			=> context.Read(Length);
		/// <summary>
		/// Writes <see cref="Length"/> bytes to the context
		/// </summary>
		/// <param name="context"></param>
		/// <param name="data"></param>
		public void WrtieBytes(IsoComposingContext context, ReadOnlyMemory<byte> data)
		{
			if (data.Length != Length)
				throw new IsoMessageWriteException($"Invalid data length for fixed part. length: {data.Length}, expected: {Length}");

			context.Write(data);
		}
	}
}
