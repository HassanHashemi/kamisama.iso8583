﻿using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Parts;
using System;
using System.Collections.Generic;

namespace KamiSama.Iso8583.Protocol.Handlers.BytesHandlers
{
	/// <summary>
	/// IsoPartBytesHandler, used in <see cref="IsoFieldDefinition" />
	/// </summary>
	public interface IIsoPartBytesHandler
	{
		/// <summary>
		/// Handler Factories, you can add your own factory here
		/// </summary>
		public static List<IIsoPartBytesHandlerFactory> HandlerFactories { get; } = new List<IIsoPartBytesHandlerFactory>
		{
			new DefaultIsoPartBytesHandlerFactory<FixedLengthIsoPartBytesHandler>(),
			new DefaultIsoPartBytesHandlerFactory<AsciiLengthValueIsoPartBytesHandler>(),
			new DefaultIsoPartBytesHandlerFactory<BcdLengthValueIsoPartBytesHandler>(),
			new DefaultIsoPartBytesHandlerFactory<BinaryLengthValueIsoPartBytesHandler>(),
			new DefaultIsoPartBytesHandlerFactory<AnyIsoPartBytesHandler>(),
		};
		/// <summary>
		/// Resolves the type based <see cref="HandlerFactories"/>
		/// </summary>
		/// <param name="type">the type string, some acceptable values are fixed(n), lvar, llvar, bcd-lllvar </param>
		/// <returns></returns>
		public static IIsoPartBytesHandler Resolve(string type)
		{
			type = (type ?? "").ToLower();

			foreach (var factory in HandlerFactories)
				if (factory.Matches(type))
					return factory.Create(type);

			throw new IsoProtocolParsingException($"Cannot resolve iso part bytes handler: {type}.");
		}
		/// <summary>
		/// Reads bytes based on handler from context
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		ReadOnlyMemory<byte> ReadBytes(IsoParsingContext context);
		/// <summary>
		/// Writes bytes to the context based on handler 
		/// </summary>
		/// <param name="context"></param>
		/// <param name="data"></param>
		void WrtieBytes(IsoComposingContext context, ReadOnlyMemory<byte> data);
	}
}