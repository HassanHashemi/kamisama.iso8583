﻿using KamiSama.Iso8583.Protocol.Exceptions;
using System;

namespace KamiSama.Iso8583.Protocol.Handlers.BytesHandlers
{
	/// <summary>
	/// Any Iso Bytes does not have length. it writes any bytes given and reads up to the end of the context
	/// </summary>
	public class AnyIsoPartBytesHandler : IIsoPartBytesHandler
	{
		/// <inheritdoc />
		public static bool Matches(string type) => type switch
		{
			"any" or "all" or "*" => true,
			_ => false
		};
		/// <inheritdoc />
		public static AnyIsoPartBytesHandler Create(string type) => type switch
		{
			"any" or "all" or "*" => new AnyIsoPartBytesHandler(),
			_ => throw new IsoProtocolParsingException($"Invalid type for any iso part bytes handler. type: {type}.")
		};
		/// <summary>
		/// Reads to the end of the context bytes
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public ReadOnlyMemory<byte> ReadBytes(IsoParsingContext context) => context.ReadToEnd();
		/// <summary>
		/// Writes raw byte data into the context
		/// </summary>
		/// <param name="context"></param>
		/// <param name="data"></param>
		public void WrtieBytes(IsoComposingContext context, ReadOnlyMemory<byte> data)
			=> context.Write(data);
	}
}
