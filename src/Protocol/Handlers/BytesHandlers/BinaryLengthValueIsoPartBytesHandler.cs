﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Helpers;
using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Handlers.BytesHandlers
{
	/// <summary>
	/// Read/Writes bcd-length-value bytes from/to context
	/// </summary>
	public class BinaryLengthValueIsoPartBytesHandler : IIsoPartBytesHandler
	{
		/// <summary>
		/// lengths to be read in the measure of nibbles (half bytes)
		/// </summary>
		public int LengthLength { get; }
		/// <summary>
		/// maximum supportable length
		/// </summary>
		private readonly int max;
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="lengthLength"></param>
		public BinaryLengthValueIsoPartBytesHandler(int lengthLength)
		{
			LengthLength = lengthLength;
			max = (int)(1L << (4 * lengthLength)); //length in nibbles
		}
		/// <inheritdoc />
		public static bool Matches(string type) => (type.StartsWith("bin-l") || type.StartsWith("binary-l")) && type.EndsWith("var");
		/// <summary>
		/// Creates a <see cref="BinaryLengthValueIsoPartBytesHandler" /> out of string
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		/// <exception cref="IsoProtocolParsingException" >if type length is invalid</exception>
		[ExcludeFromCodeCoverage]
		public static BinaryLengthValueIsoPartBytesHandler Create(string type)
		{
			var lengthLength = type switch
			{
				"bin-lvar" => 1,
				"bin-llvar" => 2,
				"bin-lllvar" => 3,
				"bin-llllvar" => 4,
				"bin-lllllvar" => 5,
				"binary-lvar" => 1,
				"binary-llvar" => 2,
				"binary-lllvar" => 3,
				"binary-llllvar" => 4,
				"binary-lllllvar" => 5,
				_ => throw new IsoProtocolParsingException($"Invalid length value type. type: {type}.")
			};

			return new BinaryLengthValueIsoPartBytesHandler(lengthLength);
		}
		/// <summary>
		/// reads bcd-length and then bytes from the context
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public ReadOnlyMemory<byte> ReadBytes(IsoParsingContext context)
		{
			var lengthBytes = context.Read((LengthLength + 1) / 2);
			var length = MemoryBytesHelper.ConvertBytesToInt(lengthBytes);

			length %= max;

			return context.Read(length);
		}
		/// <summary>
		/// Writes bcd-length and the bytes to the context
		/// </summary>
		/// <param name="context"></param>
		/// <param name="data"></param>
		public void WrtieBytes(IsoComposingContext context, ReadOnlyMemory<byte> data)
		{
			var length = data.Length;

			if (length > max)
				throw new IsoMessageWriteException($"Invalid data length for bcd length. lenght: {length}, maximum:{max}");

			var lengthBytes = MemoryBytesHelper.ConvertIntToBytes(data.Length, (LengthLength + 1) / 2);

			context.Write(lengthBytes);
			context.Write(data);
		}
	}
}
