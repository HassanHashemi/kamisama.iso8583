﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Helpers;
using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Handlers.BytesHandlers
{
	/// <summary>
	/// L(n)Var Iso Part Bytes Handler
	/// </summary>
	public class AsciiLengthValueIsoPartBytesHandler : IIsoPartBytesHandler
	{
		/// <summary>
		/// Length of length part
		/// </summary>
		public int LengthLength { get; }
		/// <summary>
		/// maximum supported value for length
		/// </summary>
		public long max;
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="lengthLength"></param>
		public AsciiLengthValueIsoPartBytesHandler(int lengthLength)
		{
			LengthLength = lengthLength;
			max = (int) Math.Pow(10, lengthLength);
		}
		/// <summary>
		/// Matches the <paramref name="type"/> to the lvar, llvar, ..., lllllvar
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		/// <exception cref="IsoProtocolParsingException">for invalid type</exception>
		public static bool Matches(string type) => (type.StartsWith("ascii-l") || type.StartsWith("l")) && type.EndsWith("var");
		/// <summary>
		/// Creates a <see cref="AsciiLengthValueIsoPartBytesHandler"/> out of <paramref name="type"/>
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		/// <exception cref="IsoProtocolParsingException" />
		[ExcludeFromCodeCoverage]
		public static AsciiLengthValueIsoPartBytesHandler Create(string type)
		{
			var lengthLength = type switch
			{
				"lvar" => 1,
				"llvar" => 2,
				"lllvar" => 3,
				"llllvar" => 4,
				"lllllvar" => 5,
				"ascii-lvar" => 1,
				"ascii-llvar" => 2,
				"ascii-lllvar" => 3,
				"ascii-llllvar" => 4,
				"ascii-lllllvar" => 5,
				_ => throw new IsoProtocolParsingException($"Invalid length value type. type: {type}.")
			};

			return new AsciiLengthValueIsoPartBytesHandler(lengthLength);
		}
		/// <summary>
		/// Reads <see cref="LengthLength" /> bytes and the data part from context
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		/// <exception cref="IsoMessageReadException">if length is invalid</exception>
		public ReadOnlyMemory<byte> ReadBytes(IsoParsingContext context)
		{
			var lengthBytes = context.Read(LengthLength);
			var length = MemoryBytesHelper.ConvertAsciiBytesToInt(lengthBytes);

			if (length == null)
				throw new IsoMessageReadException($"invalid length for L({LengthLength})Var. length bytes hex: {lengthBytes.ToArray().ToHex()}");

			return context.Read(length.Value);
		}
		/// <summary>
		/// Writes <see cref="LengthLength" /> bytes and the data part to the context
		/// </summary>
		/// <param name="context"></param>
		/// <param name="data"></param>
		public void WrtieBytes(IsoComposingContext context, ReadOnlyMemory<byte> data)
		{
			var length = data.Length;

			if (length >= max)
				throw new IsoMessageWriteException($"Invalid data length: {data.Length}, maximum: {max}");

			var lengthBytes = MemoryBytesHelper.ConvertIntToAsciiBytes(length, LengthLength);

			context.Write(lengthBytes);
			context.Write(data);
		}
	}
}