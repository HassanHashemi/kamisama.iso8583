﻿namespace KamiSama.Iso8583.Protocol.Handlers.TypeHandlers
{
	/// <summary>
	/// <see cref="BcdIsoPartTypeHandler"/> factory
	/// </summary>
	public interface IIsoPartTypeHandlerFactory
	{
		/// <summary>
		/// returns true if the <paramref name="type"/> matches the corresponding handler
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		bool Matches(string type);
		/// <summary>
		/// Creates a new <see cref="IIsoPartTypeHandler"/>
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		IIsoPartTypeHandler Create(string type);
	}
}