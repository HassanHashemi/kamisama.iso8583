﻿using KamiSama.Iso8583.Protocol.Helpers;
using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Handlers.TypeHandlers
{
	/// <summary>
	/// MAC ISO Part Type hanler
	/// </summary>
	public class HexMacIsoPartTypeHandler : MacIsoPartTypeHandlerBase
	{
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="macGenerationType"></param>
		public HexMacIsoPartTypeHandler(MacGenerationType macGenerationType)
			: base(macGenerationType)
		{
		}
		/// <summary>
		/// Matches type to mac
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static bool Matches(string type) => type.StartsWith("hex-mac");
		/// <summary>
		/// Creates new <see cref="MacIsoPartTypeHandler"/>
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public static HexMacIsoPartTypeHandler Create(string type)
		{
			var splitted = type.Split(new char[] { ' ', '\t' }, 2);

			var macType = splitted.Length > 1 ? splitted[1] : "iso-19.09";

			return macType.ToLower() switch
			{
				"des" or "des-mac" or "desmac" => new HexMacIsoPartTypeHandler(MacGenerationType.DesMac),
				"3des" or "3des-mac" or "tripledes-mac" or "tripledes" => new HexMacIsoPartTypeHandler(MacGenerationType.TripleDesMac),
				"aes" or "aes-mac" => new HexMacIsoPartTypeHandler(MacGenerationType.AesMac),
				"skip" => new HexMacIsoPartTypeHandler(MacGenerationType.Skip),
				_ or "x9.19" or "ansi9.19" or "ansix9.19" => new HexMacIsoPartTypeHandler(MacGenerationType.AnsiX9_19),
			};
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="part"></param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public override ReadOnlyMemory<byte> ConvertToBytes(IsoPart part)
			=> part switch
			{
				BinaryIsoPart binaryIsoPart => MemoryBytesHelper.ConvertBytesToHexBytes(binaryIsoPart.Data),
				null => throw new NullReferenceException("part cannot be null."),
				_ => throw new NotSupportedException($"{part.GetType()} is not supported by {nameof(MacIsoPartTypeHandler)}"),
			};
		/// <summary>
		/// creates <see cref="BinaryIsoPart"/>
		/// </summary>
		/// <param name="bytes"></param>
		/// <returns></returns>
		public override IsoPart ConvertToIsoPart(ReadOnlyMemory<byte> bytes) => new BinaryIsoPart(MemoryBytesHelper.ConvertHexBytesToBytes(bytes));
	}
}
