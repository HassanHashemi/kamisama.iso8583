﻿using KamiSama.Iso8583.Protocol.Helpers;
using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Handlers.TypeHandlers
{
	/// <summary>
	/// Converts <see cref="BitmapIsoPart"/> into (hex) bytes and vice versa
	/// </summary>
	public class HexBitmapIsoPartTypeHandler : IIsoPartTypeHandler
	{
		/// <summary>
		/// Matches type to hex-bmp, hex-bitmap
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public static bool Matches(string type)
			=> type switch
			{
				"hex-bmp" or "hex-bitmap" or "bmp-hex" or "bitmap-hex" => true,
				_ => false,
			};
		/// <summary>
		/// Creates new <see cref="HexBitmapIsoPartTypeHandler"/>
		/// </summary>
		/// <param name="type"></param>
		[SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
		public static HexBitmapIsoPartTypeHandler Create(string type) => new();
		/// <summary>
		/// Converts (hex) bytes into <see cref="BitmapIsoPart"/>
		/// </summary>
		/// <param name="part"></param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public ReadOnlyMemory<byte> ConvertToBytes(IsoPart part)
			=> part switch
			{
				BitmapIsoPart bitmapIsoPart => MemoryBytesHelper.ConvertAsciiSringToBytes(bitmapIsoPart.GetBitmapHex()),
				null => throw new NullReferenceException($"part cannot be null."),
				_ => throw new NotSupportedException($"{part.GetType()} is not supported by {nameof(BcdIsoPartTypeHandler)}"),
			};
		/// <summary>
		/// Converts <see cref="BitmapIsoPart"/> into (hex) bytes
		/// </summary>
		/// <exception cref="NotSupportedException">for any other IsoPart type</exception>
		/// <exception cref="NullReferenceException">if part is null</exception>
		/// <param name="bytes"></param>
		/// <returns></returns>
		public IsoPart ConvertToIsoPart(ReadOnlyMemory<byte> bytes)
		{
			bytes = MemoryBytesHelper.ConvertHexBytesToBytes(bytes);
			return BitmapIsoPart.FromBytes(bytes);
		}
	}
}