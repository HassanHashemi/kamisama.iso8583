﻿using KamiSama.Iso8583.Protocol.Exceptions;
using System;
using System.Collections.Generic;

namespace KamiSama.Iso8583.Protocol.Handlers.TypeHandlers
{
	/// <summary>
	/// Converts IsoPart to bytes and vice versa
	/// </summary>
	public interface IIsoPartTypeHandler
	{
		/// <summary>
		/// Handler Factories, you can add your own factory here
		/// </summary>
		public static List<IIsoPartTypeHandlerFactory> HandlerFactories { get; } = new List<IIsoPartTypeHandlerFactory>
		{
			new DefaultIsoPartTypeHandlerFactory<BinaryIsoPartTypeHandler>(),
			new DefaultIsoPartTypeHandlerFactory<BitmapIsoPartTypeHandler>(),
			new DefaultIsoPartTypeHandlerFactory<HexBitmapIsoPartTypeHandler>(),
			new DefaultIsoPartTypeHandlerFactory<TextIsoPartTypeHandler>(),
			new DefaultIsoPartTypeHandlerFactory<BcdIsoPartTypeHandler>(),
			new DefaultIsoPartTypeHandlerFactory<MacIsoPartTypeHandler>(),
			new DefaultIsoPartTypeHandlerFactory<HexMacIsoPartTypeHandler>(),
		};
		/// <summary>
		/// Resovles the type based <see cref="HandlerFactories"/>
		/// </summary>
		/// <param name="type">the type string, some acceptable values are bmp, bitmap, hex-bitmap, bin, binary, text utf-8</param>
		/// <returns></returns>
		public static IIsoPartTypeHandler Resolve(string type)
		{
			type = (type ?? "").ToLower();

			foreach (var factory in HandlerFactories)
				if (factory.Matches(type))
					return factory.Create(type);

			throw new IsoProtocolParsingException($"Cannot resolve iso part type handler: {type}.");
		}
		/// <summary>
		/// Converts bytes to <see cref="IsoPart"/>
		/// </summary>
		/// <param name="bytes"></param>
		/// <returns></returns>
		IsoPart ConvertToIsoPart(ReadOnlyMemory<byte> bytes);
		/// <summary>
		/// Converts <see cref="IsoPart"/> into bytes
		/// </summary>
		/// <param name="part"></param>
		/// <returns></returns>
		ReadOnlyMemory<byte> ConvertToBytes(IsoPart part);
	}
}