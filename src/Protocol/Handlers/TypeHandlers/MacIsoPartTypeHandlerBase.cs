﻿using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Mac;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Protocol.Handlers.TypeHandlers
{
	/// <summary>
	/// MAC Iso PartType handler base class
	/// </summary>
	public abstract class MacIsoPartTypeHandlerBase : IIsoPartTypeHandler
	{
		/// <summary>
		/// Mac Generation type
		/// </summary>
		public MacGenerationType MacGenerationType { get; }

		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="macGenerationType"></param>
		public MacIsoPartTypeHandlerBase(MacGenerationType macGenerationType)
		{
			this.MacGenerationType = macGenerationType;
		}
		/// <summary>
		/// generates mac based on the <see cref="MacGenerationType"/> and compare it to the message mac field
		/// </summary>
		/// <param name="context"></param>
		/// <param name="macGenerator"></param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public async Task VerifyMacAsync(IsoParsingContext context, IMacGenerator macGenerator)
		{
			if (MacGenerationType == MacGenerationType.Skip)
				return;

			var messageMac = context.Message.GetMac()?.Data.ToArray() ?? throw new MacVerificationFailedException($"message mac field does not exist.");

			byte[] generatedMac;

			try
			{
				generatedMac = await macGenerator.GenerateMacAsync(context, MacGenerationType);
			}
			catch (Exception exp)
			{
				throw new MacVerificationFailedException($"The mac generator has thrown an exception while generating mac.", exp);
			}

			if (generatedMac == null)
				throw new MacVerificationFailedException($"Generated MAC is invalid (null).");

			if (generatedMac.Length != messageMac.Length)
				throw new MacVerificationFailedException($"Generated MAC is not equal to message mac; invalid length.");

			for (int i = 0; i < messageMac.Length; i++)
				if (generatedMac[i] != messageMac[i])
					throw new MacVerificationFailedException($"Incorrected MAC");
		}
		/// <summary>
		/// Generates mac based on <see cref="MacGenerationType"/>
		/// </summary>
		/// <param name="context"></param>
		/// <param name="macGenerator"></param>
		/// <returns></returns>
		public virtual async Task<ReadOnlyMemory<byte>> GenerateMacAsync(IsoComposingContext context, IMacGenerator macGenerator)
		{
			if (MacGenerationType == MacGenerationType.Skip)
			{
				var mac = context.Message.GetMac()?.Data;
				return mac ?? throw new MacGenerationFailedException($"mac generation type is set to skip but mac field in iso message is not set.");
			}
			else try
				{
					return await macGenerator.GenerateMacAsync(context, MacGenerationType);
				}
				catch (Exception exp)
				{
					throw new MacGenerationFailedException($"Unexpected error has occured while generating mac.", exp);
				}
		}
		/// <inheritdoc/>
		public abstract IsoPart ConvertToIsoPart(ReadOnlyMemory<byte> bytes);
		/// <inheritdoc/>
		public abstract ReadOnlyMemory<byte> ConvertToBytes(IsoPart part);
	}
}
