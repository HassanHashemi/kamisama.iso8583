﻿using KamiSama.IranSystem;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace KamiSama.Iso8583.Protocol.Handlers.TypeHandlers
{
	/// <summary>
	/// Converts <see cref="TextIsoPart"/> into bytes and vice versa
	/// </summary>
	public class TextIsoPartTypeHandler : IIsoPartTypeHandler
	{
		/// <summary>
		/// text's encoding
		/// </summary>
		public Encoding Encoding { get; private set; }
		/// <summary>
		/// constrcutor
		/// </summary>
		/// <param name="encodingName">the name of the encoding</param>
		[ExcludeFromCodeCoverage]
		public TextIsoPartTypeHandler(string encodingName)
		{
			Encoding = (encodingName ?? "").ToLower() switch
			{
				null or "" or "utf8" => Encoding.UTF8,
				"iransystem" or "iran-system" or "f" or "farsi" => new IranSystemEncoding(),
				_ => Encoding.GetEncoding(encodingName),
			};
		}
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="encoding">the text's encoding, default is <see cref="Encoding.UTF8"/></param>
		public TextIsoPartTypeHandler(Encoding encoding = null) => Encoding = encoding ?? Encoding.UTF8;
		/// <summary>
		/// 
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public static bool Matches(string type)
			=> (type ?? "").Split(' ').FirstOrDefault() switch
			{
				"text" or "txt" or "t" or "s" or "str" => true,
				_ => false
			};
		/// <summary>
		/// Creates new <see cref="TextIsoPartTypeHandler"/>
		/// </summary>
		/// <param name="type"></param>
		public static IIsoPartTypeHandler Create(string type)
		{
			var arr = type.Split(new char[] { ' ', '\t' }, 2);
			var encoding = arr.Length > 1 ? arr[1] : "utf8";
			return new TextIsoPartTypeHandler(encoding);
		}
		/// <summary>
		/// Converts bytes into <see cref="TextIsoPart"/>
		/// </summary>
		/// <param name="bytes"></param>
		/// <returns></returns>
		public IsoPart ConvertToIsoPart(ReadOnlyMemory<byte> bytes)
			=> new TextIsoPart(Encoding.GetString(bytes.Span));
		/// <summary>
		/// Converts <see cref="TextIsoPart"/> into bytes
		/// </summary>
		/// <exception cref="NotSupportedException">for any other IsoPart type</exception>
		/// <exception cref="NullReferenceException">if part is null</exception>
		/// <param name="part"></param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public ReadOnlyMemory<byte> ConvertToBytes(IsoPart part)
			=> part switch
			{
				TextIsoPart TextIsoPart => Encoding.GetBytes(TextIsoPart.Data),
				null => throw new NullReferenceException($"part cannot be null."),
				_ => throw new NotSupportedException($"{part?.GetType()} is not supported by {nameof(TextIsoPartTypeHandler)}"),
			};
	}
}