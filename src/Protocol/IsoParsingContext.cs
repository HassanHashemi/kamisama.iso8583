﻿using KamiSama.Iso8583.Protocol.Exceptions;
using System;

namespace KamiSama.Iso8583.Protocol
{
	/// <summary>
	/// Iso message parsing context
	/// </summary>
	public class IsoParsingContext : IIsoProtocolContext
	{
		/// <summary>
		/// original message bytes (before parsing)
		/// </summary>
		public ReadOnlyMemory<byte> OriginalMessageBytes { get; }
		/// <summary>
		/// current context bytes
		/// </summary>
		public ReadOnlyMemory<byte> ContextBytes { get; }
		/// <summary>
		/// bytes for MAC Generation/Verification
		/// </summary>
		public ReadOnlyMemory<byte> ForMacBytes { get; internal protected set; }
		/// <summary>
		/// Current position in message bytes
		/// </summary>
		public int CurrentPosition { get; private set; }
		/// <inheritdoc />
		public IsoMessage Message { get; set; }
		/// <summary>
		/// parent Parsing context
		/// </summary>
		public IsoParsingContext ParentContext { get; }
		#region constructors
		/// <summary>
		/// constrcutor
		/// </summary>
		/// <param name="messageBytes"></param>
		public IsoParsingContext(ReadOnlyMemory<byte> messageBytes)
		{
			this.ParentContext = null;
			this.OriginalMessageBytes = messageBytes;
			this.ContextBytes = messageBytes;
			this.ForMacBytes = ForMacBytes;
		}
		/// <summary>
		/// constrcutor
		/// </summary>
		/// <param name="parentContext"></param>
		/// <param name="contextBytes"></param>
		public IsoParsingContext(IsoParsingContext parentContext, ReadOnlyMemory<byte> contextBytes)
		{
			this.ParentContext = parentContext;
			this.OriginalMessageBytes = parentContext.OriginalMessageBytes;
			this.ContextBytes = contextBytes;
			this.ForMacBytes = parentContext.ForMacBytes;
			this.Message = parentContext.Message;
		}
		#endregion
		/// <summary>
		/// Reads <paramref name="length"/> bytes from the message
		/// </summary>
		/// <param name="length"></param>
		/// <returns></returns>
		/// <exception cref="ArgumentException">if length is less than zero</exception>
		/// <exception cref="IsoMessageReadException">if the context does not have enough data to be read</exception>
		public virtual ReadOnlyMemory<byte> Read(int length)
		{
			if (length < 0)
				throw new ArgumentException($"length cannot be negative number, length: {length}.");

			if (CurrentPosition + length > ContextBytes.Length)
				throw new IsoMessageReadException($"Cannot read {length} byte(s). message length: {ContextBytes.Length}, pos: {CurrentPosition}.");

			var result = ContextBytes.Slice(CurrentPosition, length);
			CurrentPosition += length;

			return result;
		}
		/// <summary>
		/// Reads to the last bytes of the current context
		/// </summary>
		/// <returns></returns>
		public ReadOnlyMemory<byte> ReadToEnd()
		{
			var length = ContextBytes.Length - CurrentPosition;

			if (length >  0)
				return Read(length);
			else
				return ReadOnlyMemory<byte>.Empty;
		}
	}
}