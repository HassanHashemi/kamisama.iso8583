﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Exceptions
{
	/// <summary>
	/// IsoMessageWriteException; this exception will be thrown in different iso message composing stages
	/// </summary>
	[Serializable]
	[ExcludeFromCodeCoverage]
	public class IsoMessageWriteException : Exception
	{
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public IsoMessageWriteException(string message, Exception innerException = null)
			: base(message, innerException)
		{
		}
	}
}
