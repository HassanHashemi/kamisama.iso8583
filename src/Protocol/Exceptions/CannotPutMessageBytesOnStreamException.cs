﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Exceptions
{
	/// <summary>
	/// Cannot put message bytes on stream, see message
	/// </summary>
	[ExcludeFromCodeCoverage]
	[Serializable]
	public class CannotPutMessageBytesOnStreamException : IsoMessageWriteException
	{
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public CannotPutMessageBytesOnStreamException(string message = "Cannot put message byes on stream.", Exception innerException = null) : base(message, innerException)
		{
		}
	}
}
