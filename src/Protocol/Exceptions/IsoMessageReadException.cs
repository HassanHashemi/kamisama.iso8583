﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Exceptions
{
	/// <summary>
	/// IsoMessageReadException; this exception will be thrown in different iso message parsing stages
	/// </summary>
	[Serializable]
	[ExcludeFromCodeCoverage]
    public class IsoMessageReadException : Exception
	{
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public IsoMessageReadException(string message, Exception innerException = null)
			: base(message, innerException)
		{
		}
    }
}
