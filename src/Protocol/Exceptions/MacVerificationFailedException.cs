﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Exceptions
{
	/// <summary>
	/// MacVerificationFailedException; this exception will be thrown if mac verification is failed in reading message
	/// </summary>
	[Serializable]
	[ExcludeFromCodeCoverage]
	public class MacVerificationFailedException : IsoMessageReadException
	{
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="messageMacHex"></param>
		/// <param name="generatedMacHex"></param>
		public MacVerificationFailedException(string messageMacHex, string generatedMacHex)
			: this($"Mac verification fiald, Message MAC: {messageMacHex}, Generated MAC: {generatedMacHex}")
		{
		}
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public MacVerificationFailedException(string message, Exception innerException = null)
			: base(message, innerException)
		{
		}
	}
}