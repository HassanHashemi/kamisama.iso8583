﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Exceptions
{
	/// <summary>
	/// Cannot Pick message bytes from stream, see the message
	/// </summary>
	[ExcludeFromCodeCoverage]
	[Serializable]
	public class CannotPickMessageBytesFromStreamException : IsoMessageReadException
	{
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public CannotPickMessageBytesFromStreamException(string message = "Cannot pick message bytes from stream." ,Exception innerException = null) 
			: base(message, innerException)
		{
		}
	}
}
