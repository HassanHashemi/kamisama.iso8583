﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Exceptions
{
	/// <summary>
	/// Thrown when mac generation failed see the message
	/// </summary>
	[ExcludeFromCodeCoverage]
	[Serializable]
	public class MacGenerationFailedException : IsoMessageWriteException
	{
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public MacGenerationFailedException(string message = "MAC generation failed.", Exception innerException = null) : base(message, innerException)
		{
		}
	}
}
