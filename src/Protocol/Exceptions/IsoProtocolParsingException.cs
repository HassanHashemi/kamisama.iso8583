﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Exceptions
{
	/// <summary>
	/// Thrown when something is wrong in parsing the protocol file
	/// </summary>
	[ExcludeFromCodeCoverage]
	[Serializable]
	public class IsoProtocolParsingException : Exception
	{
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public IsoProtocolParsingException(string message, Exception innerException = null)
			: base(message, innerException)
		{
		}
	}
}
