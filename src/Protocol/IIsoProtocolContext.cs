﻿using System;
using System.Collections.Generic;

namespace KamiSama.Iso8583.Protocol
{
	/// <summary>
	/// IIsoProtocolContext abstraction
	/// </summary>
	public interface IIsoProtocolContext
	{
		/// <summary>
		/// Part of message to be used for mac generation or verification
		/// </summary>
		ReadOnlyMemory<byte> ForMacBytes { get; }
		/// <summary>
		/// Iso Message
		/// </summary>
		IsoMessage Message { get; set; }
	}
}
