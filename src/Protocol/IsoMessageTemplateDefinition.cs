﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;

namespace KamiSama.Iso8583.Protocol
{
	/// <summary>
	/// Template message definitions. used as base templates for messaging
	/// </summary>
	[XmlRoot("template")]
	public class IsoMessageTemplateDefinition : IsoMessageDefinitionBase, IEquatable<IsoMessageTemplateDefinition>
	{
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public bool Equals(IsoMessageTemplateDefinition other)
		{
			if ((other == null) ||
				(this.Name != other.Name) ||
				(this.BaseTemplateName != other.BaseTemplateName) ||
				(this.Fields?.Length != other.Fields?.Length))
				return false;

			for (int i = 0; i < (Fields?.Length ?? 0); i++)
				if (!Fields[i].Equals(other.Fields[i]))
					return false;

			return true;
		}
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override bool Equals(object obj) => Equals(obj as IsoMessageTemplateDefinition);
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override int GetHashCode() => base.GetHashCode();
	}
}
