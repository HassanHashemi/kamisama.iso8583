﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Helpers;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Protocol
{
	/// <summary>
	/// Iso 8583 Protocol's header file
	/// </summary>
	public class IsoHeaderDefinition
	{
		/// <summary>
		/// Picks message bytes from stream in the fastest way possible
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="cancellationToken"></param>
		/// <param name="timeout"></param>
		/// <returns></returns>
		/// <exception cref="CannotPickMessageBytesFromStreamException" >if cannot read message length or body from stream</exception>
		public virtual async Task<ReadOnlyMemory<byte>> PickMessageBytesFromStreamAsync(Stream stream, CancellationToken cancellationToken, TimeSpan timeout)
		{
			var lengthBytes = new Memory<byte>(new byte[4]);

			if (!await IsoStreamHelper.ReadAsync(stream, lengthBytes, cancellationToken, timeout))
				throw new CannotPickMessageBytesFromStreamException($"failed to read message length");

			var length = MemoryBytesHelper.ConvertAsciiBytesToInt(lengthBytes);

			if ((length == null) || (length <= 0))
				throw new CannotPickMessageBytesFromStreamException($"message length bytes are not ASCII encoded number: {lengthBytes.ToArray().ToHex()}");

			var messageBytes = new Memory<byte>(new byte[length.Value]);

			if (!await IsoStreamHelper.ReadAsync(stream, messageBytes, cancellationToken, timeout))
				throw new CannotPickMessageBytesFromStreamException($"Cannot read message bytes.");

			return messageBytes;
		}
		/// <summary>
		/// Puts message on the stream
		/// </summary>
		/// <param name="stream">the destination stream</param>
		/// <param name="messageBytes">the message bytes to be written</param>
		/// <param name="cancellationToken">cancelation token</param>
		/// <returns></returns>
		/// <exception cref="CannotPutMessageBytesOnStreamException">if cannot write message bytes or length to the stream</exception>
		public virtual async Task PutMessageBytesToStreamAsync(Stream stream, ReadOnlyMemory<byte> messageBytes, CancellationToken cancellationToken)
		{
			try
			{
				var lengthBytes = MemoryBytesHelper.ConvertIntToAsciiBytes(messageBytes.Length, 4);
				await stream.WriteAsync(lengthBytes, cancellationToken);
				await stream.WriteAsync(messageBytes, cancellationToken);
			}
			catch (Exception exp)
			{
				throw new CannotPutMessageBytesOnStreamException($"cannot write message into the stream.", exp);
			}
		}
	}
}
