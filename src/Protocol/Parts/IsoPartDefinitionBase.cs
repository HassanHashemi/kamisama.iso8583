﻿using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Handlers.BytesHandlers;
using KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;
using System;
using System.Linq;
using System.Xml.Serialization;

namespace KamiSama.Iso8583.Protocol.Parts
{
	/// <summary>
	/// abstract class for <see cref="IsoPartDefinition"/> and <see cref="IsoFieldDefinition"/>
	/// </summary>
	public abstract class IsoPartDefinitionBase
	{
		/// <summary>
		/// whether it is optimized or not
		/// </summary>
		internal bool optimized = false;
		/// <summary>
		/// type of the string includes bytes and iso part type eg. "llvar str utf8"
		/// </summary>
		[XmlAttribute("type")]
		public string Type { get; set; }
		/// <summary>
		/// Mapped Iso Part name
		/// </summary>
		[XmlAttribute("map-to")]
		public string MappedName { get; set; }
		/// <summary>
		/// inner Iso Parts
		/// </summary>
		[XmlElement("part")]
		public IsoPartDefinition[] InnerParts { get; set; }
		/// <summary>
		/// bytes handler which reads/write bytes from/to context
		/// </summary>
		protected internal IIsoPartBytesHandler BytesHandler { get; internal set; }
		/// <summary>
		/// type handler which converts Iso Part to bytes and vice versa
		/// </summary>
		protected internal IIsoPartTypeHandler TypeHandler { get; internal set; }		
		/// <summary>
		/// Parses from the context bytes and sets the corresponding <see cref="IsoPart"/>s
		/// </summary>
		/// <param name="context"></param>
		protected IsoPart Parse(IsoParsingContext context)
		{
			var bytes = BytesHandler.ReadBytes(context);
			var isoPart = TypeHandler.ConvertToIsoPart(bytes);

			if (!string.IsNullOrWhiteSpace(MappedName))
				context.Message[MappedName] = isoPart;

			if (InnerParts.Length == 0)
				return isoPart;

			var mappedContext = new IsoParsingContext(context, bytes);

			foreach (var part in InnerParts)
				part.Parse(mappedContext);

			return isoPart;
		}
		/// <summary>
		/// Composes from context and finally writes the bytes into the context
		/// </summary>
		/// <param name="bodyContext"></param>
		/// <param name="initialPart"></param>
		/// <remarks>data is composed in the following order:<list type="number">
		///		<item>from the corresponding iso field</item>
		///		<item>from the mapped iso part</item>
		///		<item>from inner parts</item>
		///	</list>
		///	if the data is composed the next steps will not be executed.
		///	</remarks>
		///	<exception cref="IsoMessageWriteException">if non of the steps to compose data is available.</exception>
		protected virtual void Compose(IsoComposingContext bodyContext, IsoPart initialPart)
		{
			if (initialPart == null && !string.IsNullOrWhiteSpace(MappedName))
				initialPart = bodyContext.Message[MappedName];

			if (initialPart == null)
			{
				if (InnerParts.Length == 0)
					throw new IsoMessageWriteException($"Cannot compose the part. neither iso part nor inner parts are present.");

				var mappedContext = bodyContext.CreateSubContext();

				foreach (IsoPartDefinition p in InnerParts)
					p.Compose(mappedContext, null);

				initialPart = new BinaryIsoPart(mappedContext.ToArray());
			}


			ReadOnlyMemory<byte> partBytes;

			//skipping the customized encoding if presented data is binary itself
			if (initialPart is BinaryIsoPart binaryIsoPart)
				partBytes = binaryIsoPart.Data;
			else
				partBytes = TypeHandler.ConvertToBytes(initialPart);

			BytesHandler.WrtieBytes(bodyContext, partBytes);
		}
		/// <summary>
		/// Optmizes the iso field to be the best at performance
		/// </summary>
		/// <param name="forceOptimize"></param>
		public virtual void Optimize(bool forceOptimize = false)
		{
			if (optimized && !forceOptimize)
				return;

			optimized = false;

			if (string.IsNullOrWhiteSpace(Type))
				throw new IsoProtocolParsingException($"{nameof(Type)} cannot be null.");

			var splitted = Type.Split(new char[] { ' ', '\t' }, 2).ToArray();

			BytesHandler = IIsoPartBytesHandler.Resolve(type: splitted[0]);
			if (splitted.Length > 1)
				TypeHandler = IIsoPartTypeHandler.Resolve(type: splitted[1]);
			else
				TypeHandler = IIsoPartTypeHandler.Resolve("binary");

			if (InnerParts == null)
				InnerParts = new IsoPartDefinition[0];

			foreach (var part in InnerParts)
				part.Optimize(forceOptimize);

			optimized = true;
		}
	}
}