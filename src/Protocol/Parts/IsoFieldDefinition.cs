﻿using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;
using KamiSama.Iso8583.Protocol.Mac;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace KamiSama.Iso8583.Protocol.Parts
{
	/// <summary>
	/// Iso Field Definition
	/// </summary>
	public class IsoFieldDefinition : IsoPartDefinitionBase, IEquatable<IsoFieldDefinition>
	{
		/// <summary>
		/// ISO Field Number
		/// </summary>
		[XmlAttribute("no")]
		public int FieldNo { get; set; }
		/// <summary>
		/// in case of mac iso part type the field is also set.
		/// </summary>
		internal MacIsoPartTypeHandlerBase macIsoPartTypeHandler;
		/// <summary>
		/// optmizes parsing and composnig of iso field
		/// </summary>
		/// <param name="forceOptimize"></param>
		public override void Optimize(bool forceOptimize = false)
		{
			base.Optimize(forceOptimize);

			macIsoPartTypeHandler = base.TypeHandler as MacIsoPartTypeHandlerBase;
		}
		/// <summary>
		/// Parses the iso field and sub parts
		/// </summary>
		/// <param name="context"></param>
		/// <param name="macGenerator"></param>
		/// <returns></returns>
		public virtual async Task ParseAsync(IsoParsingContext context, IMacGenerator macGenerator)
		{
			if (macIsoPartTypeHandler != null)
				context.ForMacBytes = context.OriginalMessageBytes.Slice(0, context.CurrentPosition);

			var isoPart = base.Parse(context);

			context.Message[FieldNo] = isoPart;

			if (macIsoPartTypeHandler != null)
				await macIsoPartTypeHandler.VerifyMacAsync(context, macGenerator);
		}
		/// <summary>
		/// composes the field based on iso field data, mapped field or inner parts
		/// </summary>
		/// <param name="bodyContext"></param>
		/// <param name="macGenerator"></param>
		/// <returns></returns>
		public virtual async Task ComposeAsync(IsoComposingContext bodyContext, IMacGenerator macGenerator)
		{
			if (macIsoPartTypeHandler != null)
			{
				bodyContext.ForMacBytes = bodyContext.ToArray();

				var mac = await macIsoPartTypeHandler.GenerateMacAsync(bodyContext, macGenerator);
				var macPart  = new BinaryIsoPart(mac);
				bodyContext.Message[FieldNo] = macPart;

				var macBytes = macIsoPartTypeHandler.ConvertToBytes(macPart);

				BytesHandler.WrtieBytes(bodyContext, macBytes);
				return;
			}

			var part = bodyContext.Message[FieldNo];

			try
			{
				base.Compose(bodyContext, part);
			}
			catch (Exception exp)
			{
				throw new IsoMessageWriteException($"Cannot compose field: {FieldNo}.", exp);
			}
		
			bodyContext.Message[FieldNo] = part;
		}
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public bool Equals(IsoFieldDefinition other)
		{
			if ((other == null) ||
				(FieldNo != other.FieldNo) ||
				(Type != other.Type) ||
				(MappedName != other.MappedName) ||
				(InnerParts?.Length != other.InnerParts?.Length))
				return false;

			for (int i = 0; i < (InnerParts?.Length ?? 0); i++)
				if (!InnerParts[i].Equals(other.InnerParts[i]))
					return false;

			return true;
		}
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override bool Equals(object obj) => Equals(obj as IsoFieldDefinition);
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override int GetHashCode() => base.GetHashCode();
	}
}