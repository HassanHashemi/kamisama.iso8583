﻿using KamiSama.Iso8583.Client.Exceptions;
using KamiSama.Iso8583.Protocol;
using KamiSama.Iso8583.Protocol.Mac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Client.Tests
{
	[TestClass]
	public class Iso8583ClientTests
	{
		private static IMacGenerator MacGenerator => IMacGenerator.FromKey("0102030405060708");

		[TestMethod]
		public async Task SendAsync_must_send_and_receive_successfuly()
		{
			var client = Substitute.For<ITcpClient>();
			var protocol = Substitute.For<IIsoProtocol>();
			var request = new IsoMessage(1200);
			var expectedResponse = new IsoMessage(1210);

			protocol.When(p => p.PutMessageBytesToStreamAsync(default, default, default)).Do(callInfo => { });
			protocol.PickMessageBytesFromStreamAsync(default, default, default).ReturnsForAnyArgs(new byte[0]);
			protocol.ParseAsync(default, default).ReturnsForAnyArgs(new IsoParsingContext(new byte[0]) { Message = expectedResponse });
			var isoClient = new Iso8583Client(protocol);

			var result = await isoClient.SendAsync(request, client, MacGenerator, CancellationToken.None, Timeout.InfiniteTimeSpan);

			Assert.AreEqual(expectedResponse, result);
		}
		[TestMethod]
		[ExpectedException(typeof(SendingMessageFailedException))]
		public async Task SendAsync_throws_SendingMessageFailedException_composing_is_failed()
		{
			var client = Substitute.For<ITcpClient>();
			var protocol = Substitute.For<IIsoProtocol>();

			protocol.When(p => p.ComposeAsync(Arg.Any<IsoComposingContext>(), Arg.Any<IMacGenerator>())).Throw<Exception>();

			var isoClient = new Iso8583Client(protocol);

			await isoClient.SendAsync(new IsoMessage(200) { }, client, MacGenerator, CancellationToken.None, Timeout.InfiniteTimeSpan);
		}
		[TestMethod]
		[ExpectedException(typeof(SendingMessageFailedException))]
		public async Task SendAsync_throws_SendingMessageFailedException_sending_is_failed()
		{
			var client = Substitute.For<ITcpClient>();
			var protocol = Substitute.For<IIsoProtocol>();

			protocol.ComposeAsync(Arg.Any<IsoComposingContext>(), Arg.Any<IMacGenerator>()).ReturnsForAnyArgs(new byte[0]);
			protocol.When(p => p.PutMessageBytesToStreamAsync(Arg.Any<Stream>(), Arg.Any<ReadOnlyMemory<byte>>(), Arg.Any<CancellationToken>())).Throw<Exception>();

			var isoClient = new Iso8583Client(protocol);

			await isoClient.SendAsync(new IsoMessage(200) { }, client, MacGenerator, CancellationToken.None, Timeout.InfiniteTimeSpan);
		}
		[TestMethod]
		[ExpectedException(typeof(ReceivingMessageFailedException))]
		public async Task SendAsync_throws_ReceivingMessageFailedException_if_receiving_response_failed()
		{
			var client = Substitute.For<ITcpClient>();
			var protocol = Substitute.For<IIsoProtocol>();
			var isoClient = new Iso8583Client(protocol);

			protocol.When(p => p.ParseAsync(default, default)).Throw<Exception>();

			await isoClient.SendAsync(new IsoMessage(200) { }, client, MacGenerator, CancellationToken.None, Timeout.InfiniteTimeSpan);
		}
	}
}
