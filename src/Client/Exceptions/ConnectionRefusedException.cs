﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace KamiSama.Iso8583.Client.Exceptions
{
	/// <summary>
	/// ConnectionRefusedException thrown when IsoClient could not connect to the endpoint
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class ConnectionRefusedException : Exception
	{
		/// <summary>
		/// constructor
		/// </summary>
		public ConnectionRefusedException()
		{
		}
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="message"></param>
		public ConnectionRefusedException(string message) : base(message)
		{
		}
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public ConnectionRefusedException(string message, Exception innerException) : base(message, innerException)
		{
		}
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ConnectionRefusedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
