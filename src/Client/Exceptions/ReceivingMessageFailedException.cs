﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace KamiSama.Iso8583.Client.Exceptions
{
	/// <summary>
	/// ReceivingMessageFailedException thrown when iso message cannot be read from stream
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class ReceivingMessageFailedException : Exception
	{
		/// <summary>
		/// constructor
		/// </summary>
		public ReceivingMessageFailedException()
		{
		}
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="message"></param>
		public ReceivingMessageFailedException(string message) : base(message)
		{
		}
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public ReceivingMessageFailedException(string message, Exception innerException) : base(message, innerException)
		{
		}
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected ReceivingMessageFailedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
