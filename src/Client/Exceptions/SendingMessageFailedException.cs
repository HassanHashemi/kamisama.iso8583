﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace KamiSama.Iso8583.Client.Exceptions
{
	/// <summary>
	/// SendingMessageFailedException thrown when sending message into the stream fails
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class SendingMessageFailedException : Exception
	{
		/// <summary>
		/// constructor
		/// </summary>
		public SendingMessageFailedException()
		{
		}
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="message"></param>
		public SendingMessageFailedException(string message) : base(message)
		{
		}
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public SendingMessageFailedException(string message, Exception innerException) : base(message, innerException)
		{
		}
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected SendingMessageFailedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
