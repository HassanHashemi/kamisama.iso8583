﻿using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Client
{
	/// <summary>
	/// Wrapper for TcpClient (used for testing)
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class TcpClientWrapper : ITcpClient
	{
		/// <summary>
		/// Internal TCP Client
		/// </summary>
		public TcpClient Client { get; }
		/// <summary>
		/// empty constructor
		/// </summary>
		public TcpClientWrapper()
		{
			Client = new TcpClient();
		}
		/// <summary>
		/// constructor for tcp client
		/// </summary>
		/// <param name="_client"></param>
		public TcpClientWrapper(TcpClient _client)
		{
			this.Client = _client;
		}
		/// <summary>
		/// Disposes internal TcpClient
		/// </summary>
		public void Dispose() => Client.Dispose();
		/// <summary>
		/// returns the internal TcpClient Stream
		/// </summary>
		/// <returns></returns>
		public NetworkStream GetStream() => Client.GetStream();
		/// <summary>
		/// Connects to an endpoint
		/// </summary>
		/// <param name="endPoint"></param>
		/// <returns></returns>
		public Task ConnectAsync(IPEndPoint endPoint) => Client.ConnectAsync(endPoint.Address, endPoint.Port);
		/// <summary>
		/// Connects to host:port
		/// </summary>
		/// <param name="host"></param>
		/// <param name="port"></param>
		/// <returns></returns>
		public Task ConnectAsync(string host, int port) => Client.ConnectAsync(host, port);
		/// <summary>
		/// implicitly converts TcpClient to TcpClientWrapper
		/// </summary>
		/// <param name="client"></param>
		public static implicit operator TcpClientWrapper(TcpClient client) => new(client);
	}
}
