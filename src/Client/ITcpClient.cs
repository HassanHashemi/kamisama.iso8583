﻿using System;
using System.Net.Sockets;

namespace KamiSama.Iso8583.Client
{
	/// <summary>
	/// Tcp CLient interface (used for testing)
	/// </summary>
	public interface ITcpClient : IDisposable
	{
		/// <summary>
		/// Returns the NetworkStream
		/// </summary>
		/// <returns></returns>
		NetworkStream GetStream();
	}
}