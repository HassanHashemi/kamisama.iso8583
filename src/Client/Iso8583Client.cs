﻿using KamiSama.Iso8583.Client.Exceptions;
using KamiSama.Iso8583.Protocol;
using KamiSama.Iso8583.Protocol.Mac;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Client
{
	/// <summary>
	/// Iso8583Client
	/// </summary>
	public class Iso8583Client
	{
		/// <summary>
		/// The protocol used to encode/decode iso messages
		/// </summary>
		public IIsoProtocol Protocol { get; }
		/// <summary>
		/// constructor
		/// </summary>
		/// <param name="protocol"></param>
		public Iso8583Client(IIsoProtocol protocol)
		{
			this.Protocol = protocol;
		}
		/// <summary>
		/// Sends an iso message using the client
		/// </summary>
		/// <param name="request"></param>
		/// <param name="client"></param>
		/// <param name="macGenerator"></param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public Task<IsoMessage> SendAsync(IsoMessage request, TcpClient client, IMacGenerator macGenerator) => SendAsync(request, new TcpClientWrapper(client), macGenerator, CancellationToken.None, Timeout.InfiniteTimeSpan);
		/// <summary>
		/// Sends an iso message using the client
		/// </summary>
		/// <param name="request"></param>
		/// <param name="client"></param>
		/// <param name="macGenerator"></param>
		/// <param name="cancellationToken">task cancellation token</param>
		/// <param name="timeout">receive time out</param>
		/// <returns></returns>
		public async Task<IsoMessage> SendAsync(IsoMessage request, ITcpClient client, IMacGenerator macGenerator, CancellationToken cancellationToken, TimeSpan timeout)
		{
			NetworkStream stream;
			try
			{
				stream = client.GetStream();
				var context = new IsoComposingContext(request);
				var messageBytes = await Protocol.ComposeAsync(context, macGenerator);
				await Protocol.PutMessageBytesToStreamAsync(stream, messageBytes, cancellationToken);
			}
			catch (Exception ex)
			{
				throw new SendingMessageFailedException($"Cannot send message.", ex);
			}
			try
			{
				var messageBytes = await Protocol.PickMessageBytesFromStreamAsync(stream, cancellationToken, timeout);
				var context = await Protocol.ParseAsync(messageBytes, macGenerator);
				
				return context.Message;
			}
			catch (Exception ex)
			{
				throw new ReceivingMessageFailedException($"Reading response message failed.", ex);
			}
		}
		/// <summary>
		/// Sends an iso message to the endpoint
		/// </summary>
		/// <param name="request"></param>
		/// <param name="endPoint"></param>
		/// <param name="macGenerator"></param>
		/// <param name="timeout">send/receive timeout</param>
		/// <param name="cancellationToken">Task Cancellation token</param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public Task<IsoMessage> SendAsync(IsoMessage request, IPEndPoint endPoint, IMacGenerator macGenerator, CancellationToken cancellationToken, TimeSpan timeout)
		{
			using var client = new TcpClientWrapper();
			
			try
			{
				client.Client.SendTimeout = (int)timeout.TotalMilliseconds;
				client.Client.ReceiveTimeout = (int)timeout.Milliseconds;
				client.ConnectAsync(endPoint);
			}
			catch (Exception ex)
			{
				throw new ConnectionRefusedException($"Cannot connect to {endPoint}.", ex);
			}

			return SendAsync(request, client, macGenerator, cancellationToken, timeout);
		}
		/// <summary>
		/// Sends an iso message to host:port
		/// </summary>
		/// <param name="request"></param>
		/// <param name="host"></param>
		/// <param name="port"></param>
		/// <param name="macGenerator"></param>
		/// <param name="timeout"></param>
		/// <param name="cancellationToken">cancellation token</param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public async Task<IsoMessage> SendAsync(IsoMessage request, string host, int port, IMacGenerator macGenerator, CancellationToken cancellationToken, TimeSpan timeout)
		{
			using var client = new TcpClientWrapper();
			
			try
			{
				client.Client.SendTimeout = (int) timeout.TotalMilliseconds;
				client.Client.ReceiveTimeout = (int) timeout.Milliseconds;

				await client.ConnectAsync(host, port);
			}
			catch (Exception ex)
			{
				throw new ConnectionRefusedException($"Cannot connect to {host}:{port}.", ex);
			}

			return await SendAsync(request, client, macGenerator, cancellationToken, timeout);
		}
		/// <summary>
		/// Sends an iso message to ipAddress:port
		/// </summary>
		/// <param name="request"></param>
		/// <param name="ipAddress"></param>
		/// <param name="port"></param>
		/// <param name="macGenerator"></param>
		/// <param name="cancellationToken">task cancellation token</param>
		/// <param name="timeout">send/receive timeout</param>
		/// <returns></returns>
		[ExcludeFromCodeCoverage]
		public Task<IsoMessage> SendAsync(IsoMessage request, IPAddress ipAddress, int port, IMacGenerator macGenerator, CancellationToken cancellationToken, TimeSpan timeout)
			=> SendAsync(request, new IPEndPoint(ipAddress, port), macGenerator, cancellationToken, timeout);
	}
}
