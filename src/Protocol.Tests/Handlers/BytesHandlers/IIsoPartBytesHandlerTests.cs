﻿using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Handlers.BytesHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace KamiSama.Iso8583.Protocol.Tests.Handlers.BytesHandlers
{
	[TestClass]
	public class IIsoPartBytesHandlerTests
	{
		[TestMethod]
		[DataRow("fixed(10)", typeof(FixedLengthIsoPartBytesHandler), "Length", 10)]
		[DataRow("fixed(6)", typeof(FixedLengthIsoPartBytesHandler), "Length", 6)]
		[DataRow("lvar", typeof(AsciiLengthValueIsoPartBytesHandler), "LengthLength", 1)]
		[DataRow("llvar", typeof(AsciiLengthValueIsoPartBytesHandler), "LengthLength", 2)]
		[DataRow("lllvar", typeof(AsciiLengthValueIsoPartBytesHandler), "LengthLength", 3)]
		[DataRow("llllvar", typeof(AsciiLengthValueIsoPartBytesHandler), "LengthLength", 4)]
		[DataRow("lllllvar", typeof(AsciiLengthValueIsoPartBytesHandler), "LengthLength", 5)]
		[DataRow("bcd-lvar", typeof(BcdLengthValueIsoPartBytesHandler), "LengthLength", 1)]
		[DataRow("bcd-llvar", typeof(BcdLengthValueIsoPartBytesHandler), "LengthLength", 2)]
		[DataRow("bcd-lllvar", typeof(BcdLengthValueIsoPartBytesHandler), "LengthLength", 3)]
		[DataRow("bcd-llllvar", typeof(BcdLengthValueIsoPartBytesHandler), "LengthLength", 4)]
		[DataRow("bcd-lllllvar", typeof(BcdLengthValueIsoPartBytesHandler), "LengthLength", 5)]
		public void Resolve_returns_corresponding_IsoPartBytesHandler(string type, Type expectedHandlerType, string propertyName, int expectedPropertyValue)
		{
			var handler = IIsoPartBytesHandler.Resolve(type);

			Assert.IsInstanceOfType(handler, expectedHandlerType);

			var property = expectedHandlerType.GetProperty(propertyName);

			Assert.AreEqual(expectedPropertyValue, property.GetValue(handler));
		}
		[TestMethod]
		[ExpectedException(typeof(IsoProtocolParsingException))]
		[DataRow("fixed(10")]
		[DataRow("llva")]
		[DataRow("bcdllvar-")]
		[DataRow(null)]
		[DataRow("")]
		public void Resolve_throws_IsoParsingProtocolException_if_type_cannot_be_resolved(string type)
		{
			IIsoPartBytesHandler.Resolve(type);
		}
	}
}
