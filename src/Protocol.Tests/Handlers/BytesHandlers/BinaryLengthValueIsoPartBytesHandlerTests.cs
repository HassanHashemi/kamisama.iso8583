﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Handlers.BytesHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace KamiSama.Iso8583.Protocol.Tests.Handlers.BytesHandlers
{
	[TestClass]
	public class BinaryLengthValueIsoPartBytesHandlerTests
	{
		[TestMethod]
		[DataRow("bin-lvar", true)]
		[DataRow("bin-llvar", true)]
		[DataRow("bin-lllvar", true)]
		[DataRow("bin-llllvar", true)]
		[DataRow("bin-lllllvar", true)]
		[DataRow("binary-lvar", true)]
		[DataRow("binary-llvar", true)]
		[DataRow("binary-lllvar", true)]
		[DataRow("binary-llllvar", true)]
		[DataRow("binary-lllllvar", true)]
		[DataRow("invalid-type", false)]
		[DataRow("bina-l", false)]
		[DataRow("l", false)]
		[DataRow("var", false)]
		public void Matches_matches_type_string(string type, bool expectedResult)
			=> Assert.AreEqual(expectedResult, BinaryLengthValueIsoPartBytesHandler.Matches(type));
		[TestMethod]
		[DataRow("bin-lvar", 1)]
		[DataRow("bin-llvar", 2)]
		[DataRow("bin-lllvar", 3)]
		[DataRow("bin-llllvar", 4)]
		[DataRow("bin-lllllvar", 5)]
		[DataRow("binary-lvar", 1)]
		[DataRow("binary-llvar", 2)]
		[DataRow("binary-lllvar", 3)]
		[DataRow("binary-llllvar", 4)]
		[DataRow("binary-lllllvar", 5)]
		public void Create_creates_BinaryLengthValueIsoPartBytesHandler_with_correct_length(string type, int expectedLengthLength)
		{
			var handler = BinaryLengthValueIsoPartBytesHandler.Create(type);

			Assert.IsNotNull(handler);
			Assert.AreEqual(expectedLengthLength, handler.LengthLength);
		}
		[TestMethod]
		[DataRow("invalid-type")]
		[DataRow("l")]
		[DataRow("var")]
		[DataRow(null)]
		[ExpectedException(typeof(IsoProtocolParsingException))]
		public void Create_throws_IsoProtocolParsingException_if_type_is_invalid(string type)
			=> BinaryLengthValueIsoPartBytesHandler.Create(type);

		[TestMethod]
		[DataRow("bin-lvar", "0F0102030405060708090A0B0C0D0E0F", "0102030405060708090A0B0C0D0E0F")]
		[DataRow("bin-llvar", "0F0102030405060708090A0B0C0D0E0F", "0102030405060708090A0B0C0D0E0F")]
		[DataRow("bin-lllvar", "000F0102030405060708090A0B0C0D0E0F", "0102030405060708090A0B0C0D0E0F")]
		[DataRow("bin-llllvar", "000F0102030405060708090A0B0C0D0E0F", "0102030405060708090A0B0C0D0E0F")]
		[DataRow("bin-lllllvar", "00000F0102030405060708090A0B0C0D0E0F", "0102030405060708090A0B0C0D0E0F")]
		public void ReadBytes_reads_length_then_data_from_context_correctly(string type, string messageBytesHex, string expectedDataHex)
		{
			var handler = BinaryLengthValueIsoPartBytesHandler.Create(type);

			var context = new IsoParsingContext(messageBytesHex.FromHex());

			var data = handler.ReadBytes(context);

			Assert.AreEqual(expectedDataHex, data.ToArray().ToHex());
		}
		[TestMethod]
		[ExpectedException(typeof(IsoMessageReadException))]
		public void ReadBytes_throws_IsoMessageReadException_when_length_is_invalid()
		{
			var messageBytesHex = "430102";
			var handler = BinaryLengthValueIsoPartBytesHandler.Create("bin-lvar");

			var context = new IsoParsingContext(messageBytesHex.FromHex());

			handler.ReadBytes(context);
		}
		[TestMethod]
		[DataRow("bin-lvar", "0102030405060708090A0B0C0D0E0F", "0F0102030405060708090A0B0C0D0E0F")]
		[DataRow("bin-llvar", "0102030405060708090A0B0C0D0E0F", "0F0102030405060708090A0B0C0D0E0F")]
		[DataRow("bin-lllvar", "0102030405060708090A0B0C0D0E0F", "000F0102030405060708090A0B0C0D0E0F")]
		[DataRow("bin-llllvar", "0102030405060708090A0B0C0D0E0F", "000F0102030405060708090A0B0C0D0E0F")]
		[DataRow("bin-lllllvar", "0102030405060708090A0B0C0D0E0F", "00000F0102030405060708090A0B0C0D0E0F")]
		public void WriteBytes_writes_length_then_data_to_the_context_correctly(string type, string dataHex, string expectedMessageBytesHex)
		{
			var handler = BinaryLengthValueIsoPartBytesHandler.Create(type);

			var context = new IsoComposingContext(new IsoMessage(0200));

			handler.WrtieBytes(context, dataHex.FromHex());

			Assert.AreEqual(expectedMessageBytesHex, context.ToArray().ToHex());
		}
		[TestMethod]
		[ExpectedException(typeof(IsoMessageWriteException))]
		public void WriteBytes_throws_IsoMessageWriteException_when_data_exceeds_maximum_length_acceptable()
		{
			var dataHex = "";
			for (int i = 0; i <= 256; i++)
				dataHex += i.ToString("X2");

			var handler = BinaryLengthValueIsoPartBytesHandler.Create("bin-lvar"); //maximum length is 255

			var context = new IsoComposingContext(new IsoMessage(0200));

			handler.WrtieBytes(context, dataHex.FromHex());
		}
	}
}
