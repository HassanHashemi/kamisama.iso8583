﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;

namespace KamiSama.Iso8583.Protocol.Tests.Handlers.TypeHandlers
{
	[TestClass]
	public class TextIsoPartTypeHandlerTests
	{
		[TestMethod]
		[DataRow("text", true)]
		[DataRow("txt", true)]
		[DataRow("str", true)]
		[DataRow("s", true)]
		[DataRow("t", true)]
		[DataRow("", false)]
		[DataRow(null, false)]
		public void Matches_matches_type_with_correct_values(string type, bool expectedResult)
		{
			var result = TextIsoPartTypeHandler.Matches(type);

			Assert.AreEqual(expectedResult, result);
		}
		[TestMethod]
		public void Constrcutor_default_encoding_is_UTF8()
		{
			var handler1 = new TextIsoPartTypeHandler((Encoding)null);

			Assert.AreEqual(Encoding.UTF8, handler1.Encoding);

			var handler2 = new TextIsoPartTypeHandler((string)null);

			Assert.AreEqual(Encoding.UTF8, handler2.Encoding);
		}
		[TestMethod]
		public void Create_returns_new_TextIsoPartTypeHandler()
		{
			var handler = TextIsoPartTypeHandler.Create("text");

			Assert.IsNotNull(handler);
		}
		[TestMethod]
		public void ConvertToIsoPart_returns_TextIsoPart_for_data()
		{
			var expectedStr = "salaam";
			var arr = expectedStr.ToBytes();
			var handler = TextIsoPartTypeHandler.Create("text");

			var part = handler.ConvertToIsoPart(bytes: arr);

			var TextIsoPart = part as TextIsoPart;

			Assert.IsNotNull(TextIsoPart);
			Assert.AreEqual(expectedStr, TextIsoPart.Data);
		}
		[TestMethod]
		[ExpectedException(typeof(NotSupportedException))]
		public void ConvertToBytes_throws_NotSupportedException_if_part_is_not_TextIsoPart()
		{
			var handler = TextIsoPartTypeHandler.Create("text");
			var part = new BinaryIsoPart(new byte[2]);

			handler.ConvertToBytes(part);
		}
		[TestMethod]
		[ExpectedException(typeof(NullReferenceException))]
		public void ConvertToBytes_throws_NullReferenceException_if_part_is_null()
		{
			var handler = TextIsoPartTypeHandler.Create("text");
			handler.ConvertToBytes(null);
		}
		[TestMethod]
		public void CovertToBytes_returns_corresponding_TextIsoPart_Data()
		{
			var expectedStr = "salaam";
			var handler = TextIsoPartTypeHandler.Create("text");
			var part = new TextIsoPart(expectedStr);

			var bytes = handler.ConvertToBytes(part);

			CollectionAssert.AreEqual(expectedStr.ToBytes(), bytes.ToArray());
		}
	}
}
