﻿using KamiSama.IranSystem;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;

namespace KamiSama.Iso8583.Protocol.Tests.Handlers.ByteHandlers
{
	[TestClass]
	public class IsoPartTypeHandlerTests
	{
		[TestMethod]
		[DataRow("bin", typeof(BinaryIsoPartTypeHandler), null)]
		[DataRow("b", typeof(BinaryIsoPartTypeHandler), null)]
		[DataRow("binary", typeof(BinaryIsoPartTypeHandler), null)]
		[DataRow("bmp", typeof(BitmapIsoPartTypeHandler), null)]
		[DataRow("bitmap", typeof(BitmapIsoPartTypeHandler), null)]
		[DataRow("binary-bitmap", typeof(BitmapIsoPartTypeHandler), null)]
		[DataRow("hex-bmp", typeof(HexBitmapIsoPartTypeHandler), null)]
		[DataRow("hex-bitmap", typeof(HexBitmapIsoPartTypeHandler), null)]
		[DataRow("text iransystem", typeof(TextIsoPartTypeHandler), typeof(IranSystemEncoding))]
		[DataRow("text", typeof(TextIsoPartTypeHandler), typeof(UTF8Encoding))]
		[DataRow("text utf-8", typeof(TextIsoPartTypeHandler), typeof(UTF8Encoding))]
		[DataRow("text utf8", typeof(TextIsoPartTypeHandler), typeof(UTF8Encoding))]
		public void Resolve_returns_corresponding_IsoPartBytesHandler(string type, Type expectedHandlerType, Type expectedEncoding)
		{
			var handler = IIsoPartTypeHandler.Resolve(type);

			Assert.IsInstanceOfType(handler, expectedHandlerType);

			if ((expectedEncoding != null) && (handler is TextIsoPartTypeHandler textIsoPartTypeHandler))
				Assert.IsInstanceOfType(textIsoPartTypeHandler.Encoding, expectedEncoding);
		}
		[TestMethod]
		[ExpectedException(typeof(IsoProtocolParsingException))]
		[DataRow("bitmapp")]
		[DataRow("textt")]
		[DataRow("")]
		[DataRow(null)]
		public void Resolve_throws_IsoParsingProtocolException_if_type_cannot_be_resolved(string type) => IIsoPartTypeHandler.Resolve(type);
	}
}
