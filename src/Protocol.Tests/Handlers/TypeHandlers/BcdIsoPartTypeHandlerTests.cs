﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace KamiSama.Iso8583.Protocol.Tests.Handlers.TypeHandlers
{
	[TestClass]
	public class BcdIsoPartTypeHandlerTests
	{
		[TestMethod]
		[DataRow("bcd", true)]
		[DataRow("", false)]
		public void Matches_matches_type_with_correct_values(string type, bool expectedResult)
		{
			var result = BcdIsoPartTypeHandler.Matches(type);

			Assert.AreEqual(expectedResult, result);
		}
		[TestMethod]
		public void Create_returns_new_BcdIsoPartTypeHandler()
		{
			var handler = BcdIsoPartTypeHandler.Create("bcd");

			Assert.IsNotNull(handler);
		}
		[TestMethod]
		[DataRow("123456", "123456")]
		[DataRow("23456", "023456")]
		public void ConvertToIsoPart_returns_TextIsoPart_for_bcd_value(string hexData, string expectedString)
		{
			var arr = hexData.FromHex();
			var handler = BcdIsoPartTypeHandler.Create("bcd");

			var part = handler.ConvertToIsoPart(arr);

			var TextIsoPart = part as TextIsoPart;

			Assert.IsNotNull(TextIsoPart);
			Assert.AreEqual(expectedString, TextIsoPart.Data);
		}
		[TestMethod]
		[ExpectedException(typeof(NotSupportedException))]
		public void ConvertToBytes_throws_NotSupportedException_if_part_is_not_TextIsoPart()
		{
			var handler = BcdIsoPartTypeHandler.Create("bcd");
			var part = new BinaryIsoPart(new byte[2]);

			handler.ConvertToBytes(part);
		}
		[TestMethod]
		[ExpectedException(typeof(IsoMessageWriteException))]
		public void ConvertToBytes_throws_IsoMessageWriteException_if_part_is_a_valid_bcd_string()
		{
			var handler = BcdIsoPartTypeHandler.Create("bcd");
			var part = new TextIsoPart("some invalid text");

			handler.ConvertToBytes(part);
		}
		[TestMethod]
		[ExpectedException(typeof(NullReferenceException))]
		public void ConvertToBytes_throws_NullReferenceException_if_part_is_null()
		{
			var handler = BcdIsoPartTypeHandler.Create("bcd");
			handler.ConvertToBytes(null);
		}
		[TestMethod]
		[DataRow("123456", "123456")]
		[DataRow("23456", "023456")]
		public void CovertToBytes_returns_corresponding_TextIsoPart_Data(string data, string expectedHex)
		{
			var handler = BcdIsoPartTypeHandler.Create("bcd");
			var part = new TextIsoPart(data);

			var bytes = handler.ConvertToBytes(part);

			Assert.AreEqual(expectedHex, bytes.ToArray().ToHex());
		}
	}
}
