﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;
using KamiSama.Iso8583.Protocol.Mac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Protocol.Tests.Handlers.TypeHandlers
{
	[TestClass]
	public class MacIsoPartTypeHandlerTests
	{
		[TestMethod]
		[DataRow("mac", true)]
		public void Matches_matches_type_with_correct_values(string type, bool expectedResult)
		{
			var result = MacIsoPartTypeHandler.Matches(type);

			Assert.AreEqual(expectedResult, result);
		}
		[TestMethod]
		[DataRow("mac", MacGenerationType.AnsiX9_19)]
		[DataRow("mac skip", MacGenerationType.Skip)]
		[DataRow("mac ansi9.19", MacGenerationType.AnsiX9_19)]
		[DataRow("mac ansix9.19", MacGenerationType.AnsiX9_19)]
		[DataRow("mac x9.19", MacGenerationType.AnsiX9_19)]
		[DataRow("mac des", MacGenerationType.DesMac)]
		[DataRow("mac des-mac", MacGenerationType.DesMac)]
		[DataRow("mac desmac", MacGenerationType.DesMac)]
		public void Create_returns_new_MacIsoPartTypeHandler(string type, MacGenerationType expectedMacType)
		{
			var handler = MacIsoPartTypeHandler.Create(type);

			Assert.IsNotNull(handler);
			Assert.AreEqual(expectedMacType, handler.MacGenerationType);
		}
		[TestMethod]
		[DataRow("123456")]
		[DataRow("23456")]
		public void ConvertToIsoPart_returns_BinaryIsoPart_for_data(string hexData)
		{
			var arr = hexData.FromHex();
			var handler = MacIsoPartTypeHandler.Create("mac");

			var part = handler.ConvertToIsoPart(arr);

			var MacIsoPart = part as BinaryIsoPart;

			Assert.IsNotNull(MacIsoPart);
			Assert.AreEqual(arr.ToHex(), MacIsoPart.Data.ToArray().ToHex());
		}
		[TestMethod]
		[ExpectedException(typeof(NotSupportedException))]
		public void ConvertToBytes_throws_NotSupportedException_if_part_is_not_BinaryIsoPart()
		{
			var handler = MacIsoPartTypeHandler.Create("mac");
			var part = new TextIsoPart("1234");

			handler.ConvertToBytes(part);
		}
		[TestMethod]
		[ExpectedException(typeof(NullReferenceException))]
		public void ConvertToBytes_throws_NullReferenceException_if_part_is_null()
		{
			var handler = MacIsoPartTypeHandler.Create("mac");
			handler.ConvertToBytes(null);
		}
		[TestMethod]
		[DataRow("123456")]
		[DataRow("23456")]
		public void CovertToBytes_returns_corresponding_TextIsoPart_Data(string data)
		{
			var arr = data.FromHex();
			var handler = MacIsoPartTypeHandler.Create("mac");
			var part = new BinaryIsoPart(arr);

			var bytes = handler.ConvertToBytes(part);

			Assert.AreEqual(arr.ToHex(), bytes.ToArray().ToHex());
		}
		[TestMethod]
		public async Task VerifyMacAsync_does_nothing_if_type_is_skip()
		{
			bool funcIsCalled = false;
			var handler = new MacIsoPartTypeHandler(MacGenerationType.Skip);

			var macGenerator = IMacGenerator.FromFunction((ctx, macGenerationType) =>
			{
				funcIsCalled = true;
				return Task.FromResult(new byte[0]);
			});

			var context = new IsoParsingContext(new byte[0]);

			await handler.VerifyMacAsync(context, macGenerator);

			Assert.IsFalse(funcIsCalled);
		}
		[TestMethod]
		[ExpectedException(typeof(MacVerificationFailedException))]
		[DataRow("0102030405060709")]
		[DataRow("01020304050607")]
		[DataRow(null)]
		public async Task VerifyMacAsync_throws_MacVerificationFailedException_if_generatedMac_is_different_from_expectedMac(string generatedMacHex)
		{
			var handler = new MacIsoPartTypeHandler(MacGenerationType.AnsiX9_19);

			var macGenerator = IMacGenerator.FromFunction((ctx, macGenerationType) => generatedMacHex?.FromHex());
			var context = new IsoParsingContext(new byte[0])
			{
				Message = new IsoMessage(1200)
			};
			context.Message[64] = "0102030405060708".FromHex();

			await handler.VerifyMacAsync(context, macGenerator);
		}
		[TestMethod]
		[ExpectedException(typeof(MacVerificationFailedException))]
		public async Task VerifyMacAsync_throws_MacVerificationFailedException_if_macGenerationType_is_set_to_other_than_skip_and_message_mac_is_not_present()
		{
			var handler = new MacIsoPartTypeHandler(MacGenerationType.AnsiX9_19);

			var context = new IsoParsingContext(new byte[0])
			{
				Message = new IsoMessage(1200)
			};

			await handler.VerifyMacAsync(context, default);
		}
		[TestMethod]
		public async Task VerifyMacAsync_throws_nothing_if_mac_verification_is_successful()
		{
			bool funcIsCalled = false;
			var expectedMac = "010203405060708".FromHex();
			var handler = new MacIsoPartTypeHandler(MacGenerationType.AnsiX9_19);

			var macGenerator = IMacGenerator.FromFunction((ctx, macGenerationType) =>
			{
				funcIsCalled = true;
				return expectedMac;
			});

			var context = new IsoParsingContext(new byte[0])
			{
				Message = new IsoMessage(1200)
			};
			context.Message[64] = expectedMac;

			await handler.VerifyMacAsync(context, macGenerator);

			Assert.IsTrue(funcIsCalled);
		}
		[TestMethod]
		[ExpectedException(typeof(MacGenerationFailedException))]
		public async Task GenerateMacAsync_throws_MacGenerationFailedException_if_MacGenerationType_is_skip_and_no_mac_is_present_in_message()
		{
			var context = new IsoComposingContext(new IsoMessage(1200));
			var handler = new MacIsoPartTypeHandler(MacGenerationType.Skip);

			await handler.GenerateMacAsync(context, default);
		}
		[TestMethod]
		[ExpectedException(typeof(MacGenerationFailedException))]
		public async Task GenerateMacAsync_throws_MacGenerationFailedException_if_something_goes_wrong_in_macGenerationFunc()
		{
			var context = new IsoComposingContext(new IsoMessage(1200));
			var handler = new MacIsoPartTypeHandler(MacGenerationType.AnsiX9_19);
			var macGenerator = IMacGenerator.FromException(new Exception());

			await handler.GenerateMacAsync(context, macGenerator);
		}
		[TestMethod]
		public async Task GenerateMacAsync_returns_message_mac_if_macGenerationType_is_set_to_skip()
		{
			var expectedMac = "0102030405060708".FromHex();

			var message = new IsoMessage(1200);
			message[64] = expectedMac;

			var context = new IsoComposingContext(message);
			var handler = new MacIsoPartTypeHandler(MacGenerationType.Skip);

			var result = await handler.GenerateMacAsync(context, default);

			CollectionAssert.AreEqual(expectedMac, result.ToArray());
		}
		[TestMethod]
		public async Task GenerateMacAsync_returns_generated_mac_for_macGenerationType_other_than_skip()
		{
			var expectedMac = "0102030405060708".FromHex();

			var message = new IsoMessage(1200);
			var context = new IsoComposingContext(message);
			var macGenerator = IMacGenerator.FromFunction((ctx, macGenerationType) => expectedMac);
			
			var handler = new MacIsoPartTypeHandler(MacGenerationType.AnsiX9_19);

			var result = await handler.GenerateMacAsync(context, macGenerator);

			CollectionAssert.AreEqual(expectedMac, result.ToArray());
		}
	}
}
