﻿using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Tests.Handlers.TypeHandlers
{
	[TestClass]
	public class DefaultIsoPartTypeHandlerFactoryTests
	{
		public abstract class SampleIsoPartTypeHandlerWithStaticMethods : IIsoPartTypeHandler
		{
			public abstract IsoPart ConvertToIsoPart(ReadOnlyMemory<byte> bytes);
			public abstract ReadOnlyMemory<byte> ConvertToBytes(IsoPart part);
			public abstract ReadOnlyMemory<byte> ReadBytes(IsoParsingContext context);
			public abstract void WrtieBytes(IsoComposingContext context, ReadOnlyMemory<byte> data);

			public static bool Matches(string type) => type == "example-type";
			[SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
			public static SampleIsoPartTypeHandlerWithStaticMethods Create(string type) => Substitute.For<SampleIsoPartTypeHandlerWithStaticMethods>();
		}
		public abstract class SampleIsoPartTypeHandlerWithoutStaticMethods : IIsoPartTypeHandler
		{
			public abstract IsoPart ConvertToIsoPart(ReadOnlyMemory<byte> bytes);
			public abstract ReadOnlyMemory<byte> ConvertToBytes(IsoPart part);
		}
		[TestMethod]
		[DataRow("example-type", true)]
		[DataRow("another-type", false)]
		public void Matches_calls_static_match_method(string type, bool expectedResult)
			=> Assert.AreEqual(expectedResult, new DefaultIsoPartTypeHandlerFactory<SampleIsoPartTypeHandlerWithStaticMethods>().Matches(type));
		[TestMethod]
		[ExpectedException(typeof(IsoProtocolParsingException))]
		public void Matches_throw_IsoProtocolParsingException_if_no_static_match_method_is_found()
			=> new DefaultIsoPartTypeHandlerFactory<SampleIsoPartTypeHandlerWithoutStaticMethods>().Matches("example-type");
		[TestMethod]
		public void Create_calls_static_create_method()
			=> Assert.IsInstanceOfType(new DefaultIsoPartTypeHandlerFactory<SampleIsoPartTypeHandlerWithStaticMethods>().Create("example-type"), typeof(SampleIsoPartTypeHandlerWithStaticMethods));
		[TestMethod]
		[ExpectedException(typeof(IsoProtocolParsingException))]
		public void Create_throw_IsoProtocolParsingException_if_no_static_match_method_is_found()
			=> new DefaultIsoPartTypeHandlerFactory<SampleIsoPartTypeHandlerWithoutStaticMethods>().Create("example-type");
	}
}
