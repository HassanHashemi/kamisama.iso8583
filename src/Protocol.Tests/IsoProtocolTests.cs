﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Parts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.IO;
using System.IO.Compression;
using System.Threading;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Protocol.Tests
{
	[TestClass]
	public class IsoProtocolTests
	{
		#region Expected Protocol in different test
		private static readonly IsoProtocol expectedProtocol = new()
		{
			Name = "test-protocol",
			MessageTemplateDefnitions = new[]
			{
				new IsoMessageTemplateDefinition
				{
					Name = "financial",
					MatchCriteriaText = @"[mti] == ""0200""",
					Fields = new []
					{
						new IsoFieldDefinition { FieldNo = 0, Type= "fixed(16) hex-bmp" },
						new IsoFieldDefinition { FieldNo = 1, Type = "fixed(16) hex-bmp" },
						new IsoFieldDefinition { FieldNo = 2, Type = "llvar text", MappedName = "pan" },
						new IsoFieldDefinition { FieldNo = 3, Type = "fixed(6) text", MappedName= "process-code" }
					}
				}
			},
			MessageDefinitions = new[]
			{
				new IsoMessageDefinition
				{
					Name = "purchase",
					BaseTemplateName = "financial",
					MatchCriteriaText = @"[process-code] == ""000000""",
					Fields = new []
					{
						new IsoFieldDefinition
						{
							FieldNo = 48,
							Type = "lllvar binary",
							MappedName = "additional-data",
							InnerParts = new []
							{
								new IsoPartDefinition
								{
									Type = "any",
									Condition = @"[p24] == ""205""",
									MappedName = "custom-data"
								}
							}
						}
					}
				}
			}
		};
		#endregion
		#region Load from X methods' tests
		[TestMethod]
		public void LoadFromXml_must_load_the_protocol_properly()
		{
			var protocol = IsoProtocol.LoadFromXmlFile("./protocols/single.protocol.xml", optimize: false);

			Assert.AreEqual(expectedProtocol, protocol);
		}
		[TestMethod]
		public void LoadFromDirectory_must_load_the_protocol_properly()
		{
			var protocol = IsoProtocol.LoadFromDirectory("./protocols/dir-protocol", optimize: false);

			Assert.AreEqual(expectedProtocol, protocol);
		}
		[TestMethod]
		public void LoadFromDirectory_must_load_the_protocol_without_definition()
		{
			var protocol = IsoProtocol.LoadFromDirectory("./protocols/dir-protocol-without-def", optimize: false);

			Assert.AreEqual("dir-protocol-without-def", protocol.Name);
			protocol.Name = "test-protocol";

			Assert.AreEqual(expectedProtocol, protocol);
		}
		[TestMethod]
		public void LoadFromZip_must_load_the_protocol_properly()
		{
			var protocol = IsoProtocol.LoadFromZipFile("./protocols/zip-protocol.zip", optimize: false);

			Assert.AreEqual(expectedProtocol, protocol);
		}
		[TestMethod]
		public void LoadFromZip_must_load_the_protocol_without_definition_file()
		{
			var protocol = IsoProtocol.LoadFromZipFile("./protocols/zip-protocol-without-def.zip", optimize: false);
			Assert.IsNotNull(protocol.Name);

			protocol.Name = "test-protocol";

			Assert.AreEqual(expectedProtocol, protocol);
		} 
		#endregion
		[TestMethod]
		public void ReadMessageTypeIndicator_reads_messageTypeIndicator_from_message()
		{
			var context = new IsoParsingContext("1200".ToBytes());

			var protocol = new IsoProtocol();

			var mti = protocol.ReadMessageTypeIndicator(context);

			Assert.AreEqual(1200, mti);
		}
		[TestMethod]
		[ExpectedException(typeof(IsoMessageReadException))]
		public void ReadMessageTypeIndicator_throws_IsoMessageReadException_if_mti_is_invalid()
		{
			var context = new IsoParsingContext("aaaa".ToBytes());

			var protocol = new IsoProtocol();

			protocol.ReadMessageTypeIndicator(context);
		}
		[TestMethod]
		public void WriteMessageTypeIndicator_writes_messageTypeIndicator_into_context()
		{
			var context = new IsoComposingContext(new IsoMessage { Id = 1200 });
			var protocol = new IsoProtocol();

			protocol.WriteMessageTypeIndicators(context);

			CollectionAssert.AreEqual("1200".ToBytes(), context.ToArray());
		}
		[TestMethod]
		[DataRow(true)]
		[DataRow(false)]
		public void Optmize_calls_message_and_template_definition_Optmize(bool forceOptmizeFlag)
		{
			var protocol = new IsoProtocol
			{
				MessageDefinitions = new[]
				{
					Substitute.For<IsoMessageDefinition>(),
				},
				MessageTemplateDefnitions = new[]
				{
					Substitute.For<IsoMessageTemplateDefinition>()
				}
			};

			protocol.Optimize(forceOptmizeFlag);

			protocol.MessageTemplateDefnitions[0].Received().Optimize(protocol, forceOptmizeFlag);
			protocol.MessageDefinitions[0].Received().Optimize(protocol, forceOptmizeFlag);
		}
		[TestMethod]
		public void Optimize_initializes_message_and_template_arrays_if_they_are_null()
		{
			var protocol = new IsoProtocol();

			protocol.Optimize();

			Assert.IsNotNull(protocol.MessageDefinitions);
			Assert.IsNotNull(protocol.MessageTemplateDefnitions);
		}
		[TestMethod]
		public async Task ParseAsync_calls_corresponding_IsoDefinition_ParseAsync()
		{
			var messageDefinition1 = Substitute.ForPartsOf<IsoMessageDefinition>();
			var messageDefinition2 = Substitute.ForPartsOf<IsoMessageDefinition>();

			var protocol = new IsoProtocol
			{
				MessageDefinitions = new[]
				{
					messageDefinition1,
					messageDefinition2
				}
			};

			messageDefinition1.WhenForAnyArgs(x => x.Matches(default)).DoNotCallBase();
			messageDefinition1.Matches(default).ReturnsForAnyArgs(false);
			messageDefinition1.WhenForAnyArgs(x => x.ParseAsync(default, default)).DoNotCallBase();
			messageDefinition1.Fields = new IsoFieldDefinition[0];

			messageDefinition2.WhenForAnyArgs(x => x.Matches(default)).DoNotCallBase();
			messageDefinition2.Matches(default).ReturnsForAnyArgs(true);
			messageDefinition2.WhenForAnyArgs(x => x.ParseAsync(default, default)).DoNotCallBase();
			messageDefinition2.Fields = new IsoFieldDefinition[0];

			var bytes = "12000000000000000000".ToBytes();

			protocol.Optimize();

			await protocol.ParseAsync(bytes, default);

			await protocol.MessageDefinitions[0].DidNotReceiveWithAnyArgs().ParseAsync(default, default);
			await protocol.MessageDefinitions[1].ReceivedWithAnyArgs().ParseAsync(default, default);
		}
		[TestMethod]
		public async Task ComposeAsync_calls_corresponding_IsoDefinition_ComposeAsync()
		{
			var messageDefinition1 = Substitute.ForPartsOf<IsoMessageDefinition>();
			var messageDefinition2 = Substitute.ForPartsOf<IsoMessageDefinition>();

			var protocol = new IsoProtocol
			{
				MessageDefinitions = new[]
				{
					messageDefinition1,
					messageDefinition2
				}
			};

			messageDefinition1.WhenForAnyArgs(x => x.Matches(default)).DoNotCallBase();
			messageDefinition1.Matches(default).ReturnsForAnyArgs(false);
			messageDefinition1.WhenForAnyArgs(x => x.ComposeAsync(default, default)).DoNotCallBase();
			messageDefinition1.Fields = new IsoFieldDefinition[0];

			messageDefinition2.WhenForAnyArgs(x => x.Matches(default)).DoNotCallBase();
			messageDefinition2.Matches(default).ReturnsForAnyArgs(true);
			messageDefinition2.WhenForAnyArgs(x => x.ComposeAsync(default, default)).DoNotCallBase();
			messageDefinition2.Fields = new IsoFieldDefinition[0];

			protocol.Optimize();

			var context = new IsoComposingContext(new IsoMessage(1200));

			await protocol.ComposeAsync(context, default);

			await protocol.MessageDefinitions[0].DidNotReceiveWithAnyArgs().ComposeAsync(default, default);
			await protocol.MessageDefinitions[1].ReceivedWithAnyArgs().ComposeAsync(default, default);
		}
		[TestMethod]
		public async Task PickMessageBytesFromStream_calls_Header_PickMessageBytesFromStream_method()
		{
			var stream = new MemoryStream();
			
			var protocol = new IsoProtocol
			{
				Header = Substitute.For<IsoHeaderDefinition>()
			};

			await protocol.PickMessageBytesFromStreamAsync(stream, CancellationToken.None, Timeout.InfiniteTimeSpan);

			await protocol.Header.Received().PickMessageBytesFromStreamAsync(stream, CancellationToken.None, Timeout.InfiniteTimeSpan);
		}
		[TestMethod]
		public async Task PutMessageBytesToStreamAsync_calls_Header_PutMessageBytesToStreamAsync_method()
		{
			var stream = new MemoryStream();
			var messageBytes = new byte[0];

			var protocol = new IsoProtocol
			{
				Header = Substitute.For<IsoHeaderDefinition>()
			};

			await protocol.PutMessageBytesToStreamAsync(stream, messageBytes, CancellationToken.None);

			await protocol.Header.Received().PutMessageBytesToStreamAsync(stream, messageBytes, CancellationToken.None);
		}
	}
}
