﻿using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Mac;
using KamiSama.Iso8583.Protocol.Parts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Protocol.Tests
{
	[TestClass]
	public class IsoMessageDefinitionTests
	{
		[TestMethod]
		public void Optimize_calls_base_Optmize_and_fills_optmizedFields()
		{
			var protocol = new IsoProtocol();
			var field4 = Substitute.For<IsoFieldDefinition>();
			var field2 = Substitute.For<IsoFieldDefinition>();
			var field3 = Substitute.For<IsoFieldDefinition>();

			field2.FieldNo = 2;
			field3.FieldNo = 3;
			field4.FieldNo = 4;

			var definition = new IsoMessageDefinition
			{
				Fields = new[] { field2, field3, field4 }
			};

			definition.Optimize(protocol, forceOptimize: true);

			Assert.IsTrue(definition.optimized);
			Assert.IsNotNull(definition.optimizedFields);
			Assert.AreSame(field2, definition.optimizedFields[2]);
			Assert.AreSame(field3, definition.optimizedFields[3]);
			Assert.AreSame(field4, definition.optimizedFields[4]);

			definition.optimizedFields = new IsoFieldDefinition[0];

			definition.Optimize(protocol, forceOptimize: false);
			Assert.AreEqual(0, definition.optimizedFields.Length);
		}
		[TestMethod]
		public async Task ParseAsync_calls_ParseAsync_for_fields_if_present()
		{
			var protocol = new IsoProtocol();
			var func = IMacGenerator.FromFunction((ctx, macType) => new byte[0]);
			var field0 = Substitute.For<IsoFieldDefinition>();
			var field4 = Substitute.For<IsoFieldDefinition>();
			var field2 = Substitute.For<IsoFieldDefinition>();
			var field3 = Substitute.For<IsoFieldDefinition>();

			field0.FieldNo = 0;
			field2.FieldNo = 2;
			field3.FieldNo = 3;
			field4.FieldNo = 4;

			var definition = new IsoMessageDefinition
			{
				Fields = new[] { field0, field2, field3, field4 }
			};

			definition.Optimize(protocol, forceOptimize: false);

			var context = new IsoParsingContext(new byte[0])
			{
				Message = new IsoMessage(1200)
			};

			context.Message.PrimaryBitmap[2] = true;
			context.Message.PrimaryBitmap[3] = true;
			context.Message.PrimaryBitmap[4] = false;

			await definition.ParseAsync(context, func);

			await field0.Received().ParseAsync(context, func);
			await field2.Received().ParseAsync(context, func);
			await field3.Received().ParseAsync(context, func);
			await field4.DidNotReceive().ParseAsync(context, func);
		}
		[TestMethod]
		[ExpectedException(typeof(IsoMessageReadException))]
		public async Task ParseAsync_throws_IsoMessageReadException_if_field_definition_is_not_found()
		{
			var protocol = new IsoProtocol();
			var context = new IsoParsingContext(new byte[0])
			{
				Message = new IsoMessage(1200)
			};
			context.Message.PrimaryBitmap[2] = true;
			var field0 = Substitute.For<IsoFieldDefinition>();

			var definition = new IsoMessageDefinition
			{
				Fields = new[] { field0 }
			};

			definition.Optimize(protocol, forceOptimize: false);

			await definition.ParseAsync(context, default);
		}
		[TestMethod]
		[ExpectedException(typeof(IsoMessageWriteException))]
		public async Task ComposeAsync_throws_IsoMessageWriteException_if_field_definition_is_not_found()
		{
			var protocol = new IsoProtocol();
			var context = new IsoComposingContext(new IsoMessage(0200))
			{
				Message = new IsoMessage(1200)
			};
			context.Message.PrimaryBitmap[2] = true;
			var field0 = Substitute.For<IsoFieldDefinition>();

			var definition = new IsoMessageDefinition
			{
				Fields = new[] { field0 }
			};

			definition.Optimize(protocol, forceOptimize: false);

			await definition.ComposeAsync(context, default);
		}
		[TestMethod]
		public async Task ComposeAsync_calls_ComposeAsync_for_fields_if_present()
		{
			var protocol = new IsoProtocol();
			var macGenerator = IMacGenerator.FromFunction((ctx, macType) => new byte[0]);
			var field0 = Substitute.For<IsoFieldDefinition>();
			var field4 = Substitute.For<IsoFieldDefinition>();
			var field2 = Substitute.For<IsoFieldDefinition>();
			var field3 = Substitute.For<IsoFieldDefinition>();

			field0.FieldNo = 0;
			field2.FieldNo = 2;
			field3.FieldNo = 3;
			field4.FieldNo = 4;

			var definition = new IsoMessageDefinition
			{
				Fields = new[] { field0, field2, field3, field4 }
			};

			definition.Optimize(protocol, forceOptimize: false);

			var message = new IsoMessage(1200);
			var context = new IsoComposingContext(message);

			context.Message.PrimaryBitmap[2] = true;
			context.Message.PrimaryBitmap[3] = true;
			context.Message.PrimaryBitmap[4] = false;

			await definition.ComposeAsync(context, macGenerator);

			await field0.Received().ComposeAsync(context, macGenerator);
			await field2.Received().ComposeAsync(context, macGenerator);
			await field3.Received().ComposeAsync(context, macGenerator);
			await field4.DidNotReceive().ComposeAsync(context, macGenerator);
		}
	}
}
