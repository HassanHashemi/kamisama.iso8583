﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol;
using KamiSama.Iso8583.Protocol.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.header.Tests
{
	[TestClass]
	public class IsoHeaderDefinitionTests
	{
		#region PickMessageBytesFromStreamAsync
		[TestMethod]
		public async Task PickMessageBytesFromStreamAsync_picks_length_then_message_from_stream()
		{
			var stream = new MemoryStream("000812001234".ToBytes());
			var header = new IsoHeaderDefinition();

			var message = await header.PickMessageBytesFromStreamAsync(stream, CancellationToken.None, Timeout.InfiniteTimeSpan);

			Assert.AreEqual("12001234", message.ToArray().GetString());
		}
		[TestMethod]
		[DataRow("00", "not enough bytes for length")]
		[DataRow("0000", "zero length")]
		[DataRow("000a", "invalid length value")]
		[DataRow("00081200123", "invalid message size")]
		public async Task PickMessageBytesFromStreamAsync_throws_CannotPickMessageFromStream_if_input_stream_data_is_invalid(string data, string errorMessage)
		{
			var stream = new MemoryStream(data.ToBytes());
			var header = new IsoHeaderDefinition();

			await Assert.ThrowsExceptionAsync<CannotPickMessageBytesFromStreamException>(async () => await header.PickMessageBytesFromStreamAsync(stream, CancellationToken.None, TimeSpan.FromMilliseconds(100)), $"test failed: {errorMessage}");
		}
		#endregion
		#region PutMessageBytesToStreamAsync
		[TestMethod]
		public async Task PutMessageBytesToStreamAsync_puts_correct_message_on_stream()
		{
			var stream = new MemoryStream();
			var messageBytes = "12001234".ToBytes();
			var header = new IsoHeaderDefinition();

			await header.PutMessageBytesToStreamAsync(stream, messageBytes, CancellationToken.None);

			Assert.AreEqual("000812001234", stream.ToArray().GetString());
		}
		[TestMethod]
		[ExpectedException(typeof(CannotPutMessageBytesOnStreamException))]
		public async Task PutMessageBytesToStreamAsync_throws_CannotPutMessageBytesOnStreamException_if_writing_to_stream_fails()
		{
			var stream = Substitute.ForPartsOf<MemoryStream>();
			var messageBytes = "12001234".ToBytes();

			stream.WriteAsync(default, default).Throws<Exception>();

			var header = new IsoHeaderDefinition();

			await header.PutMessageBytesToStreamAsync(stream, messageBytes, CancellationToken.None);
		}
		#endregion
	}
}
