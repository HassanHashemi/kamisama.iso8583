﻿using KamiSama.Iso8583.Protocol.Helpers;
using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace KamiSama.Iso8583.Protocol.Tests.Helpers
{
	[TestClass]
	public class EqualityComparerExTests
	{
		[TestMethod]
		public void Equals_calls_compareFunc()
		{
			bool functionCalled = false;
			var comparer = new EqualityComparerEx<int>((x, y) =>
			{
				functionCalled = true;

				return x == y;
			}, null);

			Assert.IsFalse(functionCalled);
			comparer.Equals(2, 3);

			Assert.IsTrue(functionCalled);
		}
		[TestMethod]
		public void GetHashCode_calls_hashFunc()
		{
			bool functionCalled = false;
			var comparer = new EqualityComparerEx<int>((x, y) => x == y, x =>
			{
				functionCalled = true;

				return x.GetHashCode();
			});

			Assert.IsFalse(functionCalled);
			comparer.GetHashCode(2);

			Assert.IsTrue(functionCalled);
		}
		[TestMethod]
		public void GetHashCode_returns_object_GetHashCode_if_hashFunc_is_null()
		{
			Func<int?, int> hashFunc = null;
			var comparer = new EqualityComparerEx<int?>((x, y) => x == y, hashFunc);
			var a = 2;

			var result = comparer.GetHashCode(a);
			var expectedResult = a.GetHashCode();

			Assert.AreEqual(expectedResult, result);
		}
		[TestMethod]
		public void GetHashCode_returns_zero_if_hashFunc_and_input_are_null()
		{
			Func<int?, int> hashFunc = null;
			var comparer = new EqualityComparerEx<int?>((x, y) => x == y, hashFunc);

			Assert.AreEqual(0, comparer.GetHashCode(null));
		}
		[TestMethod]
		public void Create_creates_new_EqualityComparerEx()
		{
			var comparer = EqualityComparerEx<int?>.Create((x, y) => x == y);

			Assert.IsTrue(comparer.Equals(2, 2));
			Assert.IsFalse(comparer.Equals(2, 3));
			Assert.AreEqual(2.GetHashCode(), comparer.GetHashCode(2));
			Assert.AreEqual(0, comparer.GetHashCode(null));
		}
	}
}
