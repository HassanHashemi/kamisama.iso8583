﻿using KamiSama.Iso8583.Protocol.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Protocol.Tests.Helpers
{
	[TestClass]
	public class IsoStreamHelperTests
	{
		[TestMethod]
		public async Task ReadAsync_must_read_from_memory()
		{
			var stream = new MemoryStream(new byte[] { 0x01, 0x02, 0x03, 0x04 });
			var buffer = new Memory<byte>(new byte[3]);

			var result = await IsoStreamHelper.ReadAsync(stream, buffer, CancellationToken.None, Timeout.InfiniteTimeSpan);

			Assert.IsTrue(result);
			CollectionAssert.AreEqual(new byte[] { 0x01, 0x02, 0x03 }, buffer.ToArray());
			Assert.AreEqual(3, stream.Position);
		}
		[TestMethod]
		public async Task ReadAsync_returns_false_if_read_is_timed_out()
		{
			var stream = Substitute.For<Stream>();
			var buffer = new Memory<byte>(new byte[10]);

			stream
				.ReadAsync(default, default)
				.ReturnsForAnyArgs(callInfo =>
					{
						Thread.Sleep(200);

						return 3;
					});
					
			var result = await IsoStreamHelper.ReadAsync(stream, buffer, CancellationToken.None, TimeSpan.FromMilliseconds(100));

			Assert.IsFalse(result);
		}
	}
}
