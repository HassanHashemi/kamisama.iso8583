﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace KamiSama.Iso8583.Protocol.Tests.Helpers
{
	[TestClass]
	public class MemoryBytesHelpertTests
	{
		#region ConvertAsciiBytesToInt
		[TestMethod]
		public void ConvertAsciiBytesToInt_must_convert_ascii_string_bytes_to_int()
		{
			var expectedNumber = 1234;
			var bytes = (expectedNumber + "").ToBytes();

			Assert.AreEqual(expectedNumber, MemoryBytesHelper.ConvertAsciiBytesToInt(bytes));
		}
		[TestMethod]
		[DataRow("31003131")]
		[DataRow("31413131")]
		public void ConvertAsciiBytesToInt_returns_null_if_bytes_are_not_valid_ascii_int(string invalidStr)
			=> Assert.IsNull(MemoryBytesHelper.ConvertAsciiBytesToInt(invalidStr.FromHex()));
		#endregion
		#region ConvertBcdBytesToInt
		[TestMethod]
		public void ConvertBcdBytesToInt_must_convert_bcd_string_bytes_to_int()
		{
			var expectedNumber = 1234;
			var bytes = (expectedNumber + "").FromHex();

			Assert.AreEqual(expectedNumber, MemoryBytesHelper.ConvertBcdBytesToInt(bytes));
		}
		[TestMethod]
		[DataRow("31a03131")]
		[DataRow("310a3131")]
		public void ConvertBcdBytesToInt_returns_null_if_bytes_are_not_valid_bcd_string(string invalidStr)
			=> Assert.IsNull(MemoryBytesHelper.ConvertBcdBytesToInt(invalidStr.FromHex()));
		#endregion
		#region ConvertAsciiSringToBytes
		[TestMethod]
		public void ConvertAsciiSringToBytes_must_convert_ascii_string_to_bytes()
		{
			var testStr = "test string";

			CollectionAssert.AreEqual(testStr.ToBytes(), MemoryBytesHelper.ConvertAsciiSringToBytes(testStr));
		}
		#endregion
		#region ConvertStringToBcdBytes
		[TestMethod]
		[DataRow("")]
		[DataRow(" ")]
		[DataRow(null)]
		public void ConvertNumberStringToBcdBytes_returns_empty_bytes_if_string_is_empty(string str)
		{
			var result = MemoryBytesHelper.ConvertNumberStringToBcdBytes(str);

			Assert.AreEqual(ReadOnlyMemory<byte>.Empty, result);
		}
		[TestMethod]
		[DataRow("A")]
		[DataRow("!")]
		[DataRow("1A")]
		[DataRow("1!")]
		[DataRow("1A2")]
		[DataRow("1!2")]
		[DataRow("12A")]
		[DataRow("12!")]
		[DataRow("123A")]
		[DataRow("123!")]
		[ExpectedException(typeof(ArgumentException))]
		public void ConvertNumberStringToBcdBytes_throw_ArgumentExcption_if_string_is_not_Number_string(string str)
			=> MemoryBytesHelper.ConvertNumberStringToBcdBytes(str);
		[TestMethod]
		[DataRow("123", "0123")]
		[DataRow("1", "01")]
		[DataRow("12", "12")]
		public void ConvertStringToBcdBytesString_converts_bcd_number_to_bytes(string str, string expectedResultHex)
		{
			var result = MemoryBytesHelper.ConvertNumberStringToBcdBytes(str);

			Assert.AreEqual(expectedResultHex, result.ToArray().ToHex());
		}
		#endregion
		#region ConvertBcdBytesToString
		[TestMethod]
		[DataRow("1A")]
		[DataRow("110A")]
		[DataRow("0A")]
		[DataRow("A0")]
		[ExpectedException(typeof(ArgumentException))]
		public void ConvertBcdBytesToString_throws_ArgumentException_if_bytes_is_not_bcd(string str)
			=> MemoryBytesHelper.ConvertBcdBytesToString(str.FromHex());
		[TestMethod]
		[DataRow("01", "01")]
		[DataRow("12", "12")]
		public void ConvertBcdBytesToString_converts_bcd_bytes_to_string(string hexData, string expectedString)
		{
			var result = MemoryBytesHelper.ConvertBcdBytesToString(hexData.FromHex());

			Assert.AreEqual(result, expectedString);
		}
		#endregion
		#region ConvertIntToBytes
		[TestMethod]
		[DataRow(1, 1, "01")]
		[DataRow(1, 2, "0001")]
		[DataRow(1, 3, "000001")]
		[DataRow(1, 4, "00000001")]
		[DataRow(1, 5, "0000000001")]
		[DataRow(1, 6, "000000000001")]
		[DataRow(1, 7, "00000000000001")]
		[DataRow(1, 8, "0000000000000001")]
		public void ConvertIntToBytes_converts_integer_to_bytes(int x, int size, string expectedHex)
		{
			var result = MemoryBytesHelper.ConvertIntToBytes(x, size);

			Assert.AreEqual(result.ToArray().ToHex(), expectedHex);
		}
		[TestMethod]
		public void ConvertIntToBytes_size_default_value_is_size_of_int()
		{
			var x = new Random().Next();

			var result1 = MemoryBytesHelper.ConvertIntToBytes(x);
			var result2 = MemoryBytesHelper.ConvertIntToBytes(x, sizeof(int));

			CollectionAssert.AreEqual(result1, result2);
		}
		#endregion
		#region ConvertBytesToInt
		[TestMethod]
		public void ConvertBytesToInt_converts_bytes_to_integer()
		{
			var expectedResult = 0x01020304;
			var bytes = new byte[] { 0x01, 0x02, 0x03, 0x04 };

			var result = MemoryBytesHelper.ConvertBytesToInt(bytes);

			Assert.AreEqual(expectedResult, result);
		}
		#endregion
		#region ConvertHexBytesToBytes
		[TestMethod]
		public void ConvertHexBytesToBytes_converts_hexBytes_to_bytes()
		{
			var str = "something";
			var bytes = str.ToBytes();

			var hex = bytes.ToHex().ToBytes();
			var hex2 = bytes.ToHex().ToLower().ToBytes();

			var result = MemoryBytesHelper.ConvertHexBytesToBytes(hex);
			var result2 = MemoryBytesHelper.ConvertHexBytesToBytes(hex2);

			CollectionAssert.AreEqual(bytes, result);
			CollectionAssert.AreEqual(bytes, result2);
		}
		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		[DataRow("40")]
		[DataRow("21")]
		[DataRow("76")]
		public void ConvertHexBytesToBytes_throws_ArgumentException_if_bytes_are_not_hex_value(string hexData)
			=> MemoryBytesHelper.ConvertHexBytesToBytes(hexData.FromHex());
		#endregion
		#region ConvertBytesToHexBytes
		[TestMethod]
		[DataRow("31313131")]
		[DataRow("FF")]
		public void ConvertBytesToHexBytes_converts_bytes_to_hex_bytes(string hex)
		{
			var bytes = hex.FromHex();
			
			var result = MemoryBytesHelper.ConvertBytesToHexBytes(bytes);

			Assert.AreEqual(bytes.ToHex().ToBytes().ToHex(), result.ToHex());
		}
		#endregion
		#region ConvertIntToAsciiBytes
		[TestMethod]
		[DataRow(1, 1, "31")]
		[DataRow(1, 2, "3031")]
		[DataRow(1, 3, "303031")]
		[DataRow(1, 4, "30303031")]
		[DataRow(1, 5, "3030303031")]
		[DataRow(1, 6, "303030303031")]
		[DataRow(1, 7, "30303030303031")]
		[DataRow(1, 8, "3030303030303031")]
		public void ConvertIntToAsciiBytes_converts_integer_to_bytes(int x, int size, string expectedHex)
		{
			var result = MemoryBytesHelper.ConvertIntToAsciiBytes(x, size);

			Assert.AreEqual(expectedHex, result.ToArray().ToHex());
		}
		[TestMethod]
		public void ConvertIntToAsciiBytes_size_default_value_is_size_of_int()
		{
			var x = new Random().Next();

			var result1 = MemoryBytesHelper.ConvertIntToAsciiBytes(x);
			var result2 = MemoryBytesHelper.ConvertIntToAsciiBytes(x, sizeof(int));

			CollectionAssert.AreEqual(result1, result2);
		}
		#endregion
		#region ConvertIntToBcdBytes
		[TestMethod]
		[DataRow(1, 1, "01")]
		[DataRow(1, 2, "0001")]
		[DataRow(1, 3, "000001")]
		[DataRow(1, 4, "00000001")]

		public void ConvertIntToBcdBytes_converts_integer_into_bytes(int x, int size, string expectedHex)
		{
			var result = MemoryBytesHelper.ConvertIntToBcdBytes(x, size);

			Assert.AreEqual(result.ToArray().ToHex(), expectedHex);
		}
		#endregion
	}
}
