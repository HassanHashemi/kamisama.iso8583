﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace KamiSama.Iso8583.Protocol.Tests
{
	[TestClass]
	public class IsoParsingContextTests
	{
		[TestMethod]
		public void Constructor_sets_ContextBytes_and_originialMessageBytes_correctly()
		{
			var context = new IsoParsingContext(new byte[] { 0x01, 0x02, 0x03 })
			{
				ForMacBytes = new byte[] { 0x08, 0x08, 0x08 },
				Message = new IsoMessage(1100)
			};

			Assert.AreEqual("010203", context.ContextBytes.ToArray().ToHex());
			Assert.AreEqual("010203", context.OriginalMessageBytes.ToArray().ToHex());
			Assert.AreEqual("080808", context.ForMacBytes.ToArray().ToHex());

			var childContext = new IsoParsingContext(context, new byte[] { 0x04, 0x5, 0x06 });

			Assert.AreEqual("040506", childContext.ContextBytes.ToArray().ToHex());
			Assert.AreEqual("010203", childContext.OriginalMessageBytes.ToArray().ToHex());
			Assert.AreEqual("080808", childContext.ForMacBytes.ToArray().ToHex());
			Assert.AreSame(context, childContext.ParentContext);
			Assert.AreSame(context.Message, childContext.Message);
		}
		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void Read_throws_ArgumentException_if_length_is_less_than_zero()
		{
			var context = new IsoParsingContext(new byte[] { 0x01, 0x02, 0x03 });

			context.Read(-1);
		}
		[TestMethod]
		public void ReadToEnd_reads_to_the_end_of_context()
		{
			var data = new byte[] { 0x01, 0x02, 0x03, 0x04 };
			var context = new IsoParsingContext(data);
			
			var result1 = context.ReadToEnd();
			var result2 = context.ReadToEnd();

			CollectionAssert.AreEqual(data, result1.ToArray());
			CollectionAssert.AreEqual(new byte[0], result2.ToArray());
		}
		[TestMethod]
		[DataRow(0, 5)]
		[DataRow(1, 4)]
		[DataRow(4, 1)]
		[ExpectedException(typeof(IsoMessageReadException))]
		public void Read_throws_IsoMessageReadException_if_length_is_more_than_unread_Bytes(int initialRead, int bytesForException)
		{
			var context = new IsoParsingContext(new byte[] { 0x01, 0x02, 0x03, 0x04 });

			context.Read(initialRead);
			Assert.IsTrue(true); //must not throw exception up to here
			context.Read(bytesForException);
		}
		[TestMethod]
		public void Read_reads_corresponding_bytes_from_message_Bytes()
		{
			var context = new IsoParsingContext(new byte[] { 0x01, 0x02, 0x03, 0x04 });

			var result = context.Read(2);
			Assert.AreEqual(2, context.CurrentPosition);
			Assert.AreEqual("0102", result.ToArray().ToHex());

			result = context.Read(2);
			Assert.AreEqual(4, context.CurrentPosition);
			Assert.AreEqual("0304", result.ToArray().ToHex());
		}
	}
}
