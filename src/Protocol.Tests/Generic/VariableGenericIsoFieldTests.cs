﻿using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace KamiSama.Iso8583.Protocol.Tests.Generic
{
	[TestClass]
	public class VariableGenericIsoFieldTests
	{
		[TestMethod]
		[DataRow("text", VariableLengthType.LVar, "text", "utf-8", "ascii-lvar text utf-8")]
		[DataRow("txt", VariableLengthType.LLVar, "text", "utf-8", "ascii-llvar text utf-8")]
		[DataRow("text", VariableLengthType.LLLVar, "text", "utf-8", "ascii-lllvar text utf-8")]
		[DataRow("binary", VariableLengthType.LVar, "text", null, "bin-lvar text")]
		[DataRow("bcd", VariableLengthType.LLVar, "text", null, "bcd-llvar text")]
		[DataRow("bcd", VariableLengthType.LLVar, "binary", null, "bcd-llvar binary")]
		public void ToIsoFieldDefinition_converts_into_IsoFieldDefinition(string lengthDataType, VariableLengthType lengthType, string dataType, string encoding, string expectedType)
		{
			var field = new VariableGenericIsoField
			{
				FieldNo = 2,
				LengthDataType = lengthDataType,
				DataType = dataType,
				Encoding = encoding,
				LengthType = lengthType
			};

			var result = field.ToIsoFieldDefinition();

			Assert.AreEqual(field.FieldNo, result.FieldNo);
			Assert.AreEqual(expectedType, result.Type);
		}
		[TestMethod]
		[ExpectedException(typeof(IsoProtocolParsingException))]
		public void ToIsoFieldDefinition_throws_IsoProtocolParsingException_for_other_length_types()
		{
			var field = new VariableGenericIsoField
			{
				FieldNo = 2,
				LengthDataType = "something_invalid",
				DataType = "text",
				Encoding = "utf-8",
				LengthType = VariableLengthType.LLVar
			};

			field.ToIsoFieldDefinition();
		}
	}
}
