﻿using KamiSama.Iso8583.Protocol.Generic;
using KamiSama.Iso8583.Protocol.Mac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Protocol.Tests.Generic
{
	[TestClass]
	public class GenericIsoProtocolTests
	{
		private static readonly GenericIsoProtocol expectedProtocol = new()
		{
			Fields = new GenericIsoProtocolField[]
			{
				new FixedGenericIsoField { FieldNo = 0, Length = 16, DataType = "bitmap-hex" },
				new FixedGenericIsoField { FieldNo = 1, Length = 16, DataType = "bitmap-hex" },
				new VariableGenericIsoField { FieldNo = 2, LengthDataType = "text", DataType = "text", LengthType = VariableLengthType.LLVar },
				new FixedGenericIsoField { FieldNo = 3, Length = 6, DataType = "text" },
				new VariableGenericIsoField { FieldNo = 48, LengthDataType = "text", DataType = "text", LengthType = VariableLengthType.LLLVar },
			}
		};
		#region LoadFromXmlFile
		[TestMethod]
		public void LoadFromXmlFile_parses_the_file_correctly()
		{
			var protocol = GenericIsoProtocol.LoadFromXmlFile("./protocols/generic.protocol.xml");

			Assert.AreEqual(expectedProtocol, protocol);
			Assert.IsNotNull(protocol.InternalProtocol);


		}
		[TestMethod]
		public void Optmize_only_optmizes_once_unless_forceOptimize_is_set_to_true()
		{
			var protocol = GenericIsoProtocol.LoadFromXmlFile("./protocols/generic.protocol.xml");
			Assert.IsNotNull(protocol.InternalProtocol);

			protocol.Fields = new GenericIsoProtocolField[0];
			protocol.Optmize();

			Assert.AreEqual(expectedProtocol.Fields.Length, protocol.InternalProtocol.MessageDefinitions[0].Fields.Length);

			protocol.Optmize(true); //force optimize

			Assert.AreEqual(0, protocol.InternalProtocol.MessageDefinitions[0].Fields.Length);
		}
		#endregion
		[TestMethod]
		public async Task ParseAsync_calls_InternalProtocol_ParseAsync_method()
		{
			var messageBytes = new byte[0];
			var macGenerator = default(IMacGenerator);

			var protocol = new GenericIsoProtocol
			{
				InternalProtocol = Substitute.For<IsoProtocol>()
			};

			await protocol.ParseAsync(messageBytes, macGenerator);

			await protocol.InternalProtocol.Received().ParseAsync(messageBytes, macGenerator);
		}
		[TestMethod]
		public async Task ComposeAsync_calls_InternalProtocol_ComposeAsync_method()
		{
			var context = new IsoComposingContext(new IsoMessage(1200));
			var macGenerator = default(IMacGenerator);

			var protocol = new GenericIsoProtocol
			{
				InternalProtocol = Substitute.For<IsoProtocol>()
			};

			await protocol.ComposeAsync(context, macGenerator);

			await protocol.InternalProtocol.Received().ComposeAsync(context, macGenerator);
		}
		[TestMethod]
		public async Task PickMessageBytesFromStreamAsync_calls_InternalProtocol_PickMessageBytesFromStreamAsync_method()
		{
			var stream = new MemoryStream();

			var protocol = new GenericIsoProtocol
			{
				InternalProtocol = Substitute.For<IsoProtocol>()
			};

			await protocol.PickMessageBytesFromStreamAsync(stream, CancellationToken.None, Timeout.InfiniteTimeSpan);

			await protocol.InternalProtocol.Received().PickMessageBytesFromStreamAsync(stream, CancellationToken.None, Timeout.InfiniteTimeSpan);
		}
		[TestMethod]
		public async Task PutMessageBytesToStreamAsync_calls_InternalProtocol_PutMessageBytesToStreamAsync_method()
		{
			var stream = new MemoryStream();
			var messageBytes = new byte[0];

			var protocol = new GenericIsoProtocol
			{
				InternalProtocol = Substitute.For<IsoProtocol>()
			};

			await protocol.PutMessageBytesToStreamAsync(stream, messageBytes, CancellationToken.None);

			await protocol.InternalProtocol.Received().PutMessageBytesToStreamAsync(stream, messageBytes, CancellationToken.None);
		}
	}
}
