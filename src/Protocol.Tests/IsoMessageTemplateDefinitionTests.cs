﻿using Expressive;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Parts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace KamiSama.Iso8583.Protocol.Tests
{
	[TestClass]
	public class IsoMessageTemplateDefinitionTests
	{
		[TestMethod]
		[ExpectedException(typeof(IsoProtocolParsingException))]
		public void Optmize_throws_IsoProtocolParsingException_if_baseTemplateName_is_not_found()
		{
			var template = new IsoMessageTemplateDefinition { BaseTemplateName = "something-not-found" };
			var protocol = new IsoProtocol
			{
				MessageTemplateDefnitions = new[] { template } 
			};

			template.Optimize(protocol, false);
		}
		[TestMethod]
		public void Optmize_optimizes_and_sorts_fields_by_fieldNo()
		{
			var field1 = Substitute.For<IsoFieldDefinition>();
			var field2 = Substitute.For<IsoFieldDefinition>();

			field1.FieldNo = 2;
			field2.FieldNo = 1;

			var template = new IsoMessageTemplateDefinition
			{
				Fields = new[]
				{
					field1,
					field2
				}
			};
			var protocol = new IsoProtocol
			{
				MessageTemplateDefnitions = new[] { template }
			};

			template.Optimize(protocol, forceOptimize: false);

			Assert.AreSame(field2, template.Fields[0]);
			Assert.AreSame(field1, template.Fields[1]);

			field1.Received().Optimize(forceOptimize: false);
			field2.Received().Optimize(forceOptimize: false);

			// for checking the optimize
			template.Fields = null;

			Assert.IsTrue(template.optimized);

			template.Optimize(protocol, forceOptimize: false);

			Assert.IsNull(template.Fields);
		}
		[TestMethod]
		[DataRow(null, null, "true")]
		[DataRow(null, @"[p03] == ""000000""", @"[p03] == ""000000""")]
		[DataRow(@"[mti] == ""1200""", null, @"[mti] == ""1200""")]
		[DataRow(@"[mti] == ""1200""", @"[p03] == ""000000""", @"([mti] == ""1200"") and ([p03] == ""000000"")")]
		public void Optmize_calls_parent_template_optmize_and_merge_fields(string parentCriteria, string templateCriteria, string expectedExpression)
		{
			var parent_field1 = Substitute.For<IsoFieldDefinition>();
			var parent_field2 = Substitute.For<IsoFieldDefinition>();
			var field1 = Substitute.For<IsoFieldDefinition>();
			var field2 = Substitute.For<IsoFieldDefinition>();

			parent_field1.FieldNo = 3;
			parent_field2.FieldNo = 2;
			field1.FieldNo = 2;
			field2.FieldNo = 1;

			var template = new IsoMessageTemplateDefinition
			{
				Name = "sample-template",
				BaseTemplateName = "parent",
				MatchCriteriaText = templateCriteria,
				Fields = new[]
				{
					field1,
					field2
				}
			};
			var parent = Substitute.ForPartsOf<IsoMessageTemplateDefinition>();
			parent.Name = "parent";
			parent.MatchCriteriaText = parentCriteria;
			parent.Fields = new[]
				{
					parent_field1,
					parent_field2
				};

			var protocol = new IsoProtocol
			{
				MessageTemplateDefnitions = new[] { template, parent }
			};

			template.Optimize(protocol, forceOptimize: false);

			parent.Received().Optimize(protocol, forceOptimize: false);

			//ordered by field no and merged by parent
			Assert.AreSame(field2, template.Fields[0]);
			Assert.AreSame(field1, template.Fields[1]);
			Assert.AreSame(parent_field1, template.Fields[2]);

			Assert.AreEqual(expectedExpression, template.MatchCriteriaText);
			Assert.IsNotNull(template.matchCriteriaExpression);
		}
		[TestMethod]
		[DataRow(@"true", true)]
		[DataRow(@"([mti] == ""1200"") and ([p03] == ""000000"")", true)]
		[DataRow(@"([mti] != ""1200"") and ([p03] == ""000000"")", false)]
		[DataRow(@"([mti] == ""1200"") and ([p03] != ""000000"")", false)]
		public void Matches_calls_match_CriteriaExpession(string expression, bool expectedResult)
		{
			var message = new IsoMessage(1200);
			message[3] = "000000";

			var context = Substitute.For<IIsoProtocolContext>();

			context.Message.Returns(message);

			var template = new IsoMessageTemplateDefinition
			{
				matchCriteriaExpression = new Expression(expression, ExpressiveOptions.IgnoreCaseAll)
			};

			Assert.AreEqual(expectedResult, template.Matches(context));
		}
	}
}
