﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Handlers.BytesHandlers;
using KamiSama.Iso8583.Protocol.Handlers.TypeHandlers;
using KamiSama.Iso8583.Protocol.Mac;
using KamiSama.Iso8583.Protocol.Parts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Protocol.Tests.Parts
{
	[TestClass]
	public class IsoFieldDefinitionTests
	{
		#region Optimize
		[TestMethod]
		[DataRow(null)]
		[DataRow("")]
		[DataRow(" ")]
		[ExpectedException(typeof(IsoProtocolParsingException))]
		public void Optimize_throws_IsoProtocolParsingException_if_Type_is_null_or_empty(string type)
		{
			var field = new IsoFieldDefinition { Type = type };

			field.Optimize(forceOptimize: false);
		}
		[TestMethod]
		public void Optimize_is_only_called_once()
		{
			var field = new IsoFieldDefinition
			{
				Type = "fixed(6)"
			};

			Assert.IsFalse(field.optimized);

			field.Optimize(forceOptimize: false);

			Assert.IsTrue(field.optimized);
			Assert.IsNotNull(field.TypeHandler);
			Assert.IsNotNull(field.BytesHandler);

			field.TypeHandler = null;
			field.Optimize(forceOptimize: false);

			Assert.IsNull(field.TypeHandler);

			field.Optimize(forceOptimize: true);

			Assert.IsTrue(field.optimized);
			Assert.IsNotNull(field.TypeHandler);
			Assert.IsNotNull(field.BytesHandler);
		}
		[TestMethod]
		[DataRow("fixed(6) text", typeof(TextIsoPartTypeHandler), typeof(FixedLengthIsoPartBytesHandler))]
		[DataRow("bcd-lvar", typeof(BinaryIsoPartTypeHandler), typeof(BcdLengthValueIsoPartBytesHandler))]
		[DataRow("bin-llvar bin", typeof(BinaryIsoPartTypeHandler), typeof(BinaryLengthValueIsoPartBytesHandler))]
		[DataRow("ascii-lllvar bcd", typeof(BcdIsoPartTypeHandler), typeof(AsciiLengthValueIsoPartBytesHandler))]
		public void Optimize_sets_typeHandler_and_bytesHandler(string type, Type typeHandlerType, Type byteHandlerType)
		{
			var field = new IsoFieldDefinition
			{
				Type = type
			};

			field.Optimize(forceOptimize: false);

			Assert.IsInstanceOfType(field.BytesHandler, byteHandlerType);
			Assert.IsInstanceOfType(field.TypeHandler, typeHandlerType);
		}
		[TestMethod]
		public void Optmize_calls_innerPart_Optmize_method()
		{
			var field = new IsoFieldDefinition
			{
				Type = "fixed(6)",
				InnerParts = new IsoPartDefinition[]
				{
					Substitute.For<IsoPartDefinition>(),
					Substitute.For<IsoPartDefinition>(),
				}
			};

			field.Optimize(forceOptimize: false);

			field.InnerParts[0].Received().Optimize(forceOptimize: false);
			field.InnerParts[1].Received().Optimize(forceOptimize: false);
		}
		#endregion
		#region Parse
		[TestMethod]
		public async Task Parse_reads_bytes_from_context_according_to_bytesHandler()
		{
			var arr = new byte[] { 0x01, 0x02, 0x03, 0x04, 0x5, 0x6 };
			var context = Substitute.For<IsoParsingContext>((ReadOnlyMemory<byte>)arr);
			var protocol = new IsoProtocol();

			context.WhenForAnyArgs(x => x.Read(default)).CallBase();
			context.Message = new IsoMessage();

			var field = new IsoFieldDefinition
			{
				FieldNo = 3,
				Type = "fixed(6)"
			};

			field.Optimize(forceOptimize: false);

			await field.ParseAsync(context, default);

			context.Received().Read(6);
			var binaryIsoPart = context.Message[3] as BinaryIsoPart;
			CollectionAssert.AreEqual(arr, binaryIsoPart.Data.ToArray());
		}
		[TestMethod]
		public async Task Parse_maps_the_content_to_the_MappedName_part()
		{
			var expectedProcessCode = "123456";
			var processCodePartName = "process-code";

			var arr = expectedProcessCode.ToBytes();
			var context = Substitute.For<IsoParsingContext>((ReadOnlyMemory<byte>) arr);
			var protocol = new IsoProtocol();
		
			context.Message = new IsoMessage();
			context.WhenForAnyArgs(x => x.Read(default)).CallBase();

			var field = new IsoFieldDefinition
			{
				FieldNo = 3,
				Type = "fixed(6) text",
				MappedName = processCodePartName
			};
			field.Optimize(forceOptimize: false);

			await field.ParseAsync(context, default);

			context.Received().Read(6);
			var textIsoPart = context.Message["process-code"] as TextIsoPart;
			Assert.AreEqual(expectedProcessCode, textIsoPart.Data);
		}
		[TestMethod]
		public async Task Parse_calls_for_InnerParts_Parse_if_any_exists()
		{
			var arr = new byte[] { 0x01, 0x02, 0x03, 0x04, 0x5, 0x6 };
			var context = new IsoParsingContext(arr)
			{
				Message = new IsoMessage()
			};

			var field = new IsoFieldDefinition
			{
				Type = "fixed(6)",
				FieldNo = 3,
				InnerParts = new IsoPartDefinition[]
				{
					Substitute.For<IsoPartDefinition>(),
					Substitute.For<IsoPartDefinition>(),
				}
			};

			field.Optimize(forceOptimize: false);

			await field.ParseAsync(context, default);

			field.InnerParts[0].ReceivedWithAnyArgs().Parse(default);
			field.InnerParts[1].ReceivedWithAnyArgs().Parse(default);
		}
		[TestMethod]
		public async Task Parse_reads_and_verifies_mac_if_field_is_mac()
		{
			var generatedMac = "0102030405060708".FromHex();

			var message = new IsoMessage(0200);

			var macGeenrator = IMacGenerator.FromFunction((ctx, type) => generatedMac);
			var context = new IsoParsingContext(generatedMac)
			{
				Message = message
			};
			var protocol = new IsoProtocol();

			var field = new IsoFieldDefinition
			{
				Type = "fixed(8) mac des",
				FieldNo = 64
			};

			field.Optimize(forceOptimize: false);

			await field.ParseAsync(context, macGeenrator);

			Assert.IsTrue(message.Has(64));

			var binary = message[64] as BinaryIsoPart;

			CollectionAssert.AreEqual(generatedMac, binary.Data.ToArray());
		}
		[TestMethod]
		[ExpectedException(typeof(MacVerificationFailedException))]
		public async Task Parse_throws_MacVerificationFailedException_if_mac_is_incorrect()
		{
			var generatedMac = "0102030405060708".FromHex();

			var message = new IsoMessage(0200);

			var macGenerator = IMacGenerator.FromFunction((ctx, type) => "0101010101010101".FromHex());

			var context = new IsoParsingContext(generatedMac)
			{
				Message = message
			};
			var protocol = new IsoProtocol();

			var field = new IsoFieldDefinition
			{
				Type = "fixed(8) mac des",
				FieldNo = 64
			};

			field.Optimize(forceOptimize: false);

			await field.ParseAsync(context, macGenerator);
		}
		#endregion
		#region Compose
		[TestMethod]
		public async Task ComposeAsync_writes_corresponding_field_into_context()
		{
			var expectedProcessCode = "123456";

			var field = new IsoFieldDefinition
			{
				Type = "fixed(6) text",
				FieldNo = 3,
			};
			var context = new IsoComposingContext(new IsoMessage(1200));

			field.Optimize(forceOptimize: false);
			
			context.Message[3] = new TextIsoPart(expectedProcessCode);

			await field.ComposeAsync(context, default);

			CollectionAssert.AreEqual(expectedProcessCode.ToBytes(), context.ToArray());
		}
		[TestMethod]
		public async Task ComposeAsync_writes_part_from_MappedName_if_corresponding_field_is_not_present_in_context()
		{
			var expectedProcessCode = "123456";

			var field = new IsoFieldDefinition
			{
				Type = "fixed(6) text",
				FieldNo = 3,
				MappedName = "process-code"
			};
			var context = new IsoComposingContext(new IsoMessage(1200));

			field.Optimize(forceOptimize: false);

			context.Message["process-code"] = new TextIsoPart(expectedProcessCode);

			await field.ComposeAsync(context, default);

			CollectionAssert.AreEqual(expectedProcessCode.ToBytes(), context.ToArray());
		}
		[TestMethod]
		public async Task ComposeAsync_writes_part_from_innerParts_when_neither_field_nor_mappedName_are_present()
		{
			var expectedProcessCode = "123456";

			var field = new IsoFieldDefinition
			{
				Type = "fixed(6) text",
				FieldNo = 3,
				InnerParts = new IsoPartDefinition[]
				{
					new IsoPartDefinition
					{
						Type = "fixed(6) text",
						MappedName = "test-part"
					}
				}
			};

			var context = new IsoComposingContext(new IsoMessage(1200));

			context.Message["test-part"] = expectedProcessCode;

			field.Optimize(forceOptimize: false);

			await field.ComposeAsync(context, default);

			CollectionAssert.AreEqual(expectedProcessCode.ToBytes(), context.ToArray());
		}
		[TestMethod]
		[ExpectedException(typeof(IsoMessageWriteException))]
		public async Task ComposeAsync_throws_IsoMessageWriteException_if_none_of_field_mappedName_or_innerParts_are_present()
		{
			var context = new IsoComposingContext(new IsoMessage(1200));

			var field = new IsoFieldDefinition
			{
				Type = "fixed(6) text",
				FieldNo = 3,
			};
			
			field.Optimize(forceOptimize: false);

			await field.ComposeAsync(context, default);
		}
		[TestMethod]
		public async Task ComposeAsync_generates_and_sets_mac_field()
		{
			var generatedMac = "0102030405060708".FromHex();

			var message = new IsoMessage(0200);
			var macGenerator = IMacGenerator.FromFunction((ctx, type) => generatedMac);
			var context = new IsoComposingContext(message);
			var protocol = new IsoProtocol();

			var field = new IsoFieldDefinition
			{
				Type= "fixed(8) mac des",
				FieldNo = 64
			};

			field.Optimize(forceOptimize: false);

			await field.ComposeAsync(context, macGenerator);

			CollectionAssert.AreEqual(generatedMac, context.ToArray());

			var binary = message[64] as BinaryIsoPart;

			Assert.IsNotNull(binary);
			CollectionAssert.AreEqual(generatedMac, binary.Data.ToArray());
			CollectionAssert.AreEqual(new byte[0], context.ForMacBytes.ToArray());
		}
		#endregion
	}
}
