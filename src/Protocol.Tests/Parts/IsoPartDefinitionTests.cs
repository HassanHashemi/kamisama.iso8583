﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Parts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Text;

namespace KamiSama.Iso8583.Protocol.Tests.Parts
{
	[TestClass]
	public class IsoPartDefinitionTests
	{
		[TestMethod]
		[DataRow(@"[p03] == ""123456""")]
		[DataRow(null)]
		public void Optmize_sets_up_expression_and_calls_parent_method(string condition)
		{
			var part = new IsoPartDefinition
			{
				Condition = condition,
				Type = "fixed(6)"
			};

			part.Optimize();

			Assert.IsNotNull(part.conditionExpression);
			Assert.IsTrue(part.optimized);

			part.optimized = false;

			//try to call it again when expression is set
			part.Optimize();

			//parent optmize is not called
			Assert.IsFalse(part.optimized);
		}
		[TestMethod]
		[DataRow(@"[p03] == ""000000""", true)]
		[DataRow(@"[p03] == ""123456""", false)]
		[DataRow(null, true)]
		[DataRow("true", true)]
		[DataRow("True", true)]
		public void Parse_parses_the_context_if_condition_matches(string condition, bool expectedToParse)
		{
			var arr = new byte[] { 0x01, 0x02, 0x03, 0x04, 0x5, 0x6 };
			var context = Substitute.For<IsoParsingContext>((ReadOnlyMemory<byte>)arr);
			var protocol = new IsoProtocol();

			context.WhenForAnyArgs(x => x.Read(default)).CallBase();
			context.Message = new IsoMessage();
			context.Message[3] = "000000";

			var field = new IsoPartDefinition
			{
				Type = "fixed(6)",
				MappedName = "process-code",
				Condition = condition
			};

			field.Optimize(forceOptimize: true);

			field.Parse(context);

			if (expectedToParse)
			{
				context.Received().Read(6);
				var binaryIsoPart = context.Message["process-code"] as BinaryIsoPart;
				CollectionAssert.AreEqual(arr, binaryIsoPart.Data.ToArray());
			} else
			{
				context.DidNotReceiveWithAnyArgs().Read(default);
			}
		}
		[TestMethod]
		[DataRow(@"[p02] == ""6100000000000000""", true)]
		[DataRow(@"[p02] == ""6100000000000001""", false)]
		[DataRow(null, true)]
		[DataRow("true", true)]
		[DataRow("True", true)]
		public void Compose_composes_into_context_if_condition_matches(string condition, bool expectedToCompose)
		{
			var expectedProcessCode = "123456";
			var field = new IsoPartDefinition
			{
				Type = "fixed(6) text",
				MappedName = "process-code",
				Condition = condition
			};
			var context = new IsoComposingContext(new IsoMessage(1200));

			field.Optimize(forceOptimize: false);

			context.Message[2] = "6100000000000000";
			context.Message["process-code"] = expectedProcessCode;

			field.Compose(context);

			if (expectedToCompose)
			{
				CollectionAssert.AreEqual(expectedProcessCode.ToBytes(), context.ToArray());
			} else
			{
				CollectionAssert.AreEqual(new byte[0], context.ToArray());
			}
		}
	}
}
