﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;

namespace KamiSama.Iso8583.Tests
{
	[TestClass]
	public class TextIsoPartTests
	{
		[TestMethod]
		public void Constrcutor_must_set_Data()
		{
			var str = "123456";
			var part = new TextIsoPart(str);

			Assert.AreEqual(str, part.Data);
		}
		[TestMethod]
		public void Cosntructor_without_parameter_sets_Data_to_empty_string()
		{
			var part = new TextIsoPart();

			Assert.AreEqual(string.Empty, part.Data);
		}
		[TestMethod]
		public void Implicit_cast_must_create_TextIsoPart()
		{
			var str = "123456";
			var part = (TextIsoPart) str;

			Assert.AreEqual(str, part.Data);
		}
		[TestMethod]
		public void ToString_returns_Data()
		{
			var data = "123456";
			var part = new TextIsoPart(data);

			Assert.AreEqual(data, part.ToString());
		}
		[TestMethod]
		public void ToBytes_return_UTF8_Encoding_bytes_as_default()
		{
			var data = "123456";
			var part = new TextIsoPart(data);

			CollectionAssert.AreEqual(Encoding.UTF8.GetBytes(data), part.ToBytes().ToArray());
		}
	}
}
