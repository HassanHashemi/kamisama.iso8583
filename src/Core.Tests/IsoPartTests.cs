﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Reflection.PortableExecutable;

namespace KamiSama.Iso8583.Tests
{
	[TestClass]
	public class IsoPartTests
	{
		[TestMethod]
		public void Implict_string_cast_returns_TextIsoPart()
		{
			var data = "123456";
			IsoPart part = data;

			var textIsoPart = part as TextIsoPart;

			Assert.IsNotNull(textIsoPart);
			Assert.AreEqual(data, textIsoPart.Data);
		}
		[TestMethod]
		public void Implict_byte_Array_cast_returns_BinaryIsoPart()
		{
			var data = new byte[] { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06 };
			IsoPart part = data;

			var binaryIsoPart = part as BinaryIsoPart;

			Assert.IsNotNull(binaryIsoPart);
			CollectionAssert.AreEqual(data, binaryIsoPart.Data.ToArray());
		}
		[TestMethod]
		public void Implict_ReadOnlyMemory_cast_returns_BinaryIsoPart()
		{
			var data = new ReadOnlyMemory<byte>(new byte[] { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06 });
			IsoPart part = data;

			var binaryIsoPart = part as BinaryIsoPart;

			Assert.IsNotNull(binaryIsoPart);
			CollectionAssert.AreEqual(data.ToArray(), binaryIsoPart.Data.ToArray());
		}
		[TestMethod]
		public void Implict_cast_to_string_returns_IsoPart_ToString()
		{
			var part = Substitute.ForPartsOf<IsoPart>();
			_ = (string)part;

			part.Received().ToString();
		}
		[TestMethod]
		public void Implict_cast_to_string_returns_null_if_IsoPart_is_null()
		{
			IsoPart part = null;

			Assert.IsNull((string)part);
		}
		[TestMethod]
		public void Implict_cast_to_ReadOnlyMemory_calls_ToBytes_method()
		{
			var part = Substitute.ForPartsOf<IsoPart>();
			_ = (ReadOnlyMemory<byte>)part;

			part.Received().ToBytes();
		}
		[TestMethod]
		public void Implict_cast_to_ReadOnlyMemory_returns_Empty_if_IsoPart_is_null()
		{
			IsoPart part = null;

			Assert.AreEqual(ReadOnlyMemory<byte>.Empty, (ReadOnlyMemory<byte>)part);
		}
		[TestMethod]
		public void Implict_cast_to_ByteArray_calls_ToBytes_method()
		{
			var part = Substitute.ForPartsOf<IsoPart>();
			_ = (byte[])part;

			part.Received().ToBytes();
		}
		[TestMethod]
		public void Implict_cast_to_ByteArray_returns_null_if_IsoPart_is_null()
		{
			IsoPart part = null;

			Assert.IsNull((byte[])part);
		}
	}
}
