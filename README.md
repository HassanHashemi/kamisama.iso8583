# KamiSama.Iso8583

KamiSama.Iso8583 is a super fast .net standard library for Iso-8583 protocol parsing and composing.
Iso-8583 is a financial protocol between financial switches in PSP and clients.

## 1. A fast guideline to use
Here is the code:
```csharp
using KamiSama.Iso8583.Client;

var protocol = IsoProtocol.LoadFromXmlFile("./protocols/sample.protocol.xml");
var client = new Iso8583Client(protocol);

var requrst = new IsoMessage(0200);

request[2] = "28727276672"; //card pan
request[3] = "000000";      //purchase process code
//...

var macGenerator = IMacGenerator.FromKey("A0A12394897983");//some key hex value for ISO 8583 mac generation

var response = await client.SendAsync(request, "host-address", 10101, CancelationToken.None, Timeout.InfiniteTimeSpan);
```

## 2. Simple Coded Interface
The library provides simple interface for iso message. 
The bitmap (primary and secondary) fields are automatically set,

```csharp
var message = new IsoMessage(100);                  // creates simple iso message with Message Type Indicator (MTI) 0100               
message[2] = "1234123412341234";                    // sample text 
message[48] = new byte[] { 0x01, 0x02, 0x03, 0x04}; // sample byte[]

Console.WriteLine(message.PrimaryBitmap.ToDebugString());
Console.WriteLine(message.ToDebugString(52));       // excludes P52 (pin-block) to be written into the output
```
## 3. Protocols
see [protocols](./docs/protocols.md)
