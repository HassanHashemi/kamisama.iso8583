﻿using KamiSama.Extensions;
using KamiSama.Iso8583;
using KamiSama.Iso8583.Protocol;
using KamiSama.Iso8583.Protocol.Mac;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;

namespace Server;

/// <summary>
/// This is a simple iso server for testing
/// </summary>
public class IsoServer
{
	public event Func<IsoMessage, Task<IsoMessage>> MessageReceived;

	private readonly IsoProtocol protocol;
	private readonly IPEndPoint endPoint;
	private readonly byte[] pinKey;
	private readonly byte[] macKey;
	private readonly IMacGenerator macGenerator;

	private readonly TcpListener tcpListener;
	private readonly ILogger<IsoServer> logger;

	/// <summary>
	/// constructor
	/// </summary>
	/// <param name="protocol">the iso protocol to parse and compose message</param>
	/// <param name="pinKey">pin block key</param>
	/// <param name="macKey">mac generation key</param>
	/// <param name="endPoint">listening ip endpoint</param>
	/// <param name="logger">logger</param>
	public IsoServer(IsoProtocol protocol, byte[] pinKey, byte[] macKey, IPEndPoint endPoint, ILogger<IsoServer> logger)
	{
		this.endPoint = endPoint;
		this.protocol = protocol;
		this.tcpListener = new TcpListener(endPoint);
		this.pinKey = pinKey;
		this.macKey = macKey;
		this.logger = logger;

		this.macGenerator = IMacGenerator.FromKey(this.macKey);
	}
	/// <summary>
	/// constructor
	/// </summary>
	/// <param name="protocol">the iso protocol to parse and compose message</param>
	/// <param name="pinKey">pin block key</param>
	/// <param name="macKey">mac generation key</param>
	/// <param name="endPoint">listening ip endpoint</param>
	/// <param name="logger">logger</param>
	public IsoServer(IsoProtocol protocol, byte[] pinKey, byte[] macKey, IPAddress address, int port, ILogger<IsoServer> logger)
		: this(protocol, pinKey, macKey, new IPEndPoint(address, port), logger)
	{
	}
	/// <summary>
	/// starts the server
	/// </summary>
	/// <param name="cancellationToken"></param>
	/// <returns></returns>
	public async Task StartAsync(CancellationToken cancellationToken)
	{
		tcpListener.Start();
		logger.LogInformation("Listening on {endpoint}...", endPoint);

		while (!cancellationToken.IsCancellationRequested)
		{
			var tcpClient = await tcpListener.AcceptTcpClientAsync();
			
			try
			{
				logger.LogInformation("Connection is received on {clientLocalEndPoint} from {clientRemoteEndPoint}", tcpClient.Client.LocalEndPoint, tcpClient.Client.RemoteEndPoint);
				var stream = tcpClient.GetStream();

				var bytes = await protocol.PickMessageBytesFromStreamAsync(stream, CancellationToken.None, TimeSpan.Zero);

				_ = Task.Run(async () => await HandleConnectionAsync(tcpClient, stream, bytes, cancellationToken));
			}
			catch (Exception exp)
			{
				logger.LogError(exp, "{localEngPoint} Processing message failed.", tcpClient.Client.LocalEndPoint);
				return;
			}
		}
	}
	/// <summary>
	/// main method to handle the request/response asynchronously
	/// </summary>
	/// <param name="tcpClient"></param>
	/// <param name="stream"></param>
	/// <param name="bytes"></param>
	/// <param name="cancellationToken"></param>
	/// <returns></returns>
	private async Task HandleConnectionAsync(TcpClient tcpClient, Stream stream, ReadOnlyMemory<byte> bytes, CancellationToken cancellationToken)
	{
		var localEndPoint = tcpClient.Client.LocalEndPoint!;

		try
		{
			var request = await ParseInputMessageAsync(localEndPoint, bytes);

			// example pin block decryption
			var pan = request[2];
			var pinBlockHex = request[52];
			await VerifyPinBlockAsync(pan, pinBlockHex, pinKey);

			if (MessageReceived != null)
			{
				var response = await MessageReceived(request);

				await SendResponseToClientAsync(stream, localEndPoint, response, cancellationToken);
			}
		}
		catch
		{
			return;
		}
		finally
		{
			try { tcpClient.Close(); } catch { }
		}
	}
	/// <summary>
	/// Parses the bytes read into <see cref="IsoMessage"/>
	/// </summary>
	/// <param name="localEndPoint"></param>
	/// <param name="bytes"></param>
	/// <returns></returns>
	private async Task<IsoMessage> ParseInputMessageAsync(EndPoint localEndPoint, ReadOnlyMemory<byte> bytes)
	{
		logger.LogInformation("{clientLocalEndPoint} bytes received: {bytes}", localEndPoint, bytes.ToArray().ToHex());
		IsoParsingContext parsingContext;

		try
		{
			logger.LogInformation("{clientLocalEndPoint} parsing the request...", localEndPoint);
			parsingContext = await protocol.ParseAsync(bytes, macGenerator);
		}
		catch (Exception exp)
		{
			logger.LogError(exp, "{clientLocalEndPoint} cannot parse request", localEndPoint);

			throw;
		}
		var request = parsingContext.Message;
		logger.LogInformation("{clientLocalEndPoint} parsed request:\r\n{request}", localEndPoint, request.ToDebugStringByFields());

		return request;
	}
	/// <summary>
	/// Verifies the pin block field (P52) based on DES encryption.
	/// if you are using TripleDES encryption change the code ;)
	/// </summary>
	/// <param name="pan"></param>
	/// <param name="pinBlockHex"></param>
	/// <param name="pinKeyData"></param>
	/// <returns></returns>
	/// <exception cref="Exception"></exception>
	public Task VerifyPinBlockAsync(string pan, string pinBlockHex, byte[] pinKeyData)
	{
		if (string.IsNullOrWhiteSpace(pan))
			throw new Exception($"Invalid pan.");

		if (string.IsNullOrWhiteSpace(pinBlockHex) || (pinBlockHex.Length != 16))
			throw new Exception($"Pin block length is invalid.");

		byte[] pinBlock;
		try
		{
			pinBlock = pinBlockHex
				.FromHex()
				.DesDecrypt(pinKeyData, cipherMode: CipherMode.ECB, padding: PaddingMode.None);
		}
		catch (Exception ex)
		{
			throw new Exception($"Pin block is not valid.", ex);
		}

		pan = pan.PadLeft(16, '0');
		pan = pan.Substring(pan.Length - 12);

		var panArr = pan.FromHex(); //6 bytes

		for (int i = 2; i < 8; i++)
			pinBlock[i] ^= panArr[i - 2];

		var pinHex = pinBlock.ToHex().TrimEnd('F');

		if (!int.TryParse(pinHex, out _))
			throw new Exception($"Pin is not valid number; probably wrong key is used.");

		return Task.CompletedTask;
	}
	/// <summary>
	/// Sends the iso message to client
	/// </summary>
	/// <param name="stream"></param>
	/// <param name="localEndPoint"></param>
	/// <param name="response"></param>
	/// <param name="cancellationToken"></param>
	/// <returns></returns>
	private async Task SendResponseToClientAsync(Stream stream, EndPoint localEndPoint, IsoMessage response, CancellationToken cancellationToken)
	{
		try
		{
			logger.LogInformation("{clientLocalEndPoint} composing response...", localEndPoint);

			var composingContext = new IsoComposingContext(response);

			await protocol.ComposeAsync(composingContext, macGenerator);
			logger.LogInformation("{clientLocalEndPoint} response message before sending (without mac field)\r\n{response}", localEndPoint, response);
			var responseBytes = composingContext.ToArray();
			logger.LogInformation("{clientLocalEndPoint} sending response message bytes: {responseBytes}", localEndPoint, responseBytes.ToHex());
			await protocol.PutMessageBytesToStreamAsync(stream, responseBytes, cancellationToken);
		}
		catch (Exception exp)
		{
			logger.LogError(exp, "{clientLocalEndPoint} Composing and sending message failed.", localEndPoint);
			throw;
		}
	}
	
}
