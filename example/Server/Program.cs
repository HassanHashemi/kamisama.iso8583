﻿using KamiSama.Extensions;
using KamiSama.Iso8583;
using KamiSama.Iso8583.Protocol;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Net.Sockets;
using System.Runtime;

namespace Server;

internal class Program
{
	static async Task Main(string[] args)
	{
		var protocol = IsoProtocol.LoadFromXmlFile("./single.protocol.xml");
		var pinKey = "1122334455667788".FromHex();
		var macKey = "1122334455667788".FromHex();

		var loggerFactory = LoggerFactory.Create(config => config.AddSimpleConsole());

		var server = new IsoServer(protocol,
							 pinKey,
							 macKey,
							 IPAddress.Any,
							 12000,
							 loggerFactory.CreateLogger<IsoServer>());

		server.MessageReceived += Server_MessageReceived;

		await server.StartAsync(CancellationToken.None);
	}
	/// <summary>
	/// Creates an example echo message with P39 = 000
	/// </summary>
	/// <param name="request"></param>
	/// <returns></returns>
	private static Task<IsoMessage> Server_MessageReceived(IsoMessage request)
	{
		//creating an echo response message from request
		var response = new IsoMessage(request.Id + 10); //200 -> 210

		for (int i = 3; i < 128; i++)
			if (i != 64) // skipping the mac field
				if (request.Has(i))
					response[i] = request[i];

		response[39] = "000";// success code

		return Task.FromResult(response);
	}
}
